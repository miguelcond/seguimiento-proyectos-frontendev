'use strict';

import ReporteCSV from './csv/csv-general.module';

const Reporte = angular
    .module('app.reporte', [
        ReporteCSV,
    ])
    .name;

export default Reporte;
