'use strict';

import ReporteComponent from './csv-general.component';

const Dashboard = angular
    .module('app.reporte.csv', [])
    .component('appReporteCsv', ReporteComponent)
    .config(($stateProvider) => {
        $stateProvider
            .state('reporte-general', {
                url: '/reporte-general',
                component: 'appReporteCsv'
            });
    })
    .name;

export default Dashboard;
