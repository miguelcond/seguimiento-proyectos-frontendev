/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class ReporteCsvController {
  constructor($q, DataService, Message, $scope, $http, apiUrl, Loading, Storage) {
    'ngInject';

    this.$q = $q;
    this.$scope = $scope;
    this.$http = $http;
    this.apiUrl = apiUrl;
    this.DataService = DataService;
    this.Message = Message;
    this.Loading = Loading;
    this.Storage = Storage;
    this.tiposReporte = [
      {valor:'general', texto:'Reporte general de proyectos'},
      {valor:'porMunicipio', texto:'Reporte de avance de proyectos por municipio'},
      {valor:'porDepartamento', texto:'Reporte de avance de proyectos por departamento'},
      {valor:'porDptoEmpleo', texto:'Proyectos ejecutados por departamento (Empleo)'},
      {valor:'porDptoPPTO', texto:'Proyectos ejecutados por departamento (PPTO)'},
      {valor:'porDptoMetas', texto:'Proyectos ejecutados por departamento (Metas)'},
      {valor:'agrupadoFisicoFinanciero', texto:'Reporte de avance físico financiero'},
      {valor:'agrupadoTiposDpto', texto:'Resumen de proyectos de gobiernos autónomos municipales por departamento'}
    ];
    this.filenames = {
      general: 'reporte_general_de_proyectos.csv',
      porMunicipio: 'reporte_avance_de_proyectos_por_municipio.csv',
      porDepartamento: 'reporte_avance_de_proyectos_por_departamento.csv',
      porDptoEmpleo: 'reporte_empleo_por_departamento.csv',
      porDptoPPTO: 'reporte_ppto_por_departamento.csv',
      porDptoMetas: 'reporte_metas_por_departamento.csv',
      agrupadoFisicoFinanciero: 'reporte_avance_fisico_financiero.csv',
      agrupadoTiposDpto: 'resumen_proyectos_de_gam_por_departamento.csv'
    };
    this.tipoReporte = 'general';
    this.filename = 'sample.csv';
    this.showDpa = false;
  }

  $onInit() {
    this.generandoCSV = true;

    let gestion = new Date();
    this.gestiones = [];
    for (let nombre = 2018; nombre <= 1900 + gestion.getYear(); nombre++) {
      this.gestiones.unshift({nombre});
      this.gestion = this.gestiones[0];
    }
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.tipoReporte', () => {
      this.limpiarDatosReporte();
      this.filename = this.filenames[this.tipoReporte];
      // console.log('-----------filename-----------------');
      // console.log(this.filename);
      // console.log('------------------------------------');
    });
    this.$scope.$watch('$ctrl.gestion.nombre', () => {
      this.limpiarDatosReporte();
    });
    this.$scope.$watch('$ctrl.dpa.departamento.codigo', () => {
      this.limpiarDatosReporte();
    });
    this.$scope.$watch('$ctrl.dpa.municipio.codigo', () => {
      this.limpiarDatosReporte();
    });
  }

  aplicarTipo() {
    // console.log('------tipo--------------------------');
    // console.log(this.tipoReporte);
    // console.log('------------------------------------');
    let requierenDpa = ['porMunicipio', 'porDepartamento', 'agrupadoTiposDpto', 'porDptoEmpleo', 'porDptoPPTO', 'porDptoMetas'];
    // if (this.tipoReporte === 'porMunicipio' || this.tipoReporte === 'porDepartamento' || this.tipoReporte === 'agrupadoTiposDpto' || this.tipoReporte === 'porDptoEmpleo' || this.tipoReporte === 'porDptoPPTO' || this.tipoReporte === 'porDptoMetas') {
    if (requierenDpa.indexOf(this.tipoReporte) > -1) {
      this.showDpa = true;
    } else {
      this.showDpa = false;
    }
    this.dpa = {};
  }

  descargarCSV(format) {
    // console.log('------------dpa---------------------');
    // console.log(this.dpa);
    // console.log('------------gestion-----------------');
    // console.log(this.gestion);
    // console.log('------------------------------------');
    let rutaReporte = `reportes/csv`; // default general
    if (format) {
      rutaReporte += `?format=${format}`;
    }

    if (this.tipoReporte !== 'general') {
      if (this.gestion && this.gestion.nombre) {
        rutaReporte = `reportes/csv/${this.tipoReporte}?gestion=${this.gestion.nombre}`;
        if (this.tipoReporte === 'porDepartamento' || this.tipoReporte === 'agrupadoTiposDpto' || this.tipoReporte === 'porDptoEmpleo' ||
            this.tipoReporte === 'porDptoPPTO' || this.tipoReporte === 'porDptoMetas') {
          if (this.dpa.departamento && this.dpa.departamento.codigo) {
            rutaReporte = rutaReporte + `&dpto=${this.dpa.departamento.codigo}`;
          } else {
            this.Message.warning('Debe seleccionar un departamento');
            return
          }
        } else if (this.tipoReporte === 'porMunicipio') {
          if (this.dpa.municipio && this.dpa.municipio.codigo) {
            rutaReporte = rutaReporte + `&municipio=${this.dpa.municipio.codigo}`;
          } else {
            this.Message.warning('Debe seleccionar un municipio');
            return
          }
        }
        if (format) {
          rutaReporte += `&format=${format}`;
        }
      } else {
        this.Message.warning('Debe seleccionar una gestión');
        return
      }
    }

    // console.log('----------ruta reporte--------------');
    // console.log(rutaReporte);
    // console.log('------------------------------------');

    this.Loading.show(`Se estan obteniendo los datos, espere por favor`, true);

    if (format) {
      this.usuario = this.Storage.getUser();
      rutaReporte += `&rol=${this.usuario.rol.id_rol}`
      this.$http({
        url: `${this.apiUrl}${rutaReporte}`,
        method: 'GET',
        headers: {
          'Content-type': 'application/octet-stream'
        },
        responseType: "arraybuffer"
      }).then((data) => {
        // console.log('resp', resp)
        const ieEDGE = navigator.userAgent.match(/Edge/g);
        const ie = navigator.userAgent.match(/.NET/g); // IE 11+
        const oldIE = navigator.userAgent.match(/MSIE/g);

        const blob = new window.Blob([data.data], { type: 'application/octet-stream' });

        if (ie || oldIE || ieEDGE) {

            const fileName = `${this.filename}.xlsx`;
            window.navigator.msSaveBlob(blob, fileName);
        }
        else {

            const file = new Blob([ data.data ], {
                type : 'application/octet-stream'
            });

            const fileURL = URL.createObjectURL(file);
            const a         = document.createElement('a');
            a.href        = fileURL;
            a.target      = '_blank';
            a.download    = `${this.filename}.xlsx`;
            document.body.appendChild(a);

            a.click();

        }
      }).catch((data, status) => {
        console.error(data, status);
      });
      return;
    }

    let deferred = this.$q.defer();
    this.DataService.get(rutaReporte).then((resp) => {
      if (resp && resp.finalizado) {
        this.Loading.hide();
        // console.log('---------datos----------------------');
        // console.log(resp.datos);
        // console.log('------------------------------------');
        this.Message.success(resp.mensaje);
        this.cabeceras = ['Sin_registros'];
        this.getArray = [];
        if (resp.datos.rows[0]) {
          this.cabeceras = this.$$obtenerCabeceras(resp.datos.rows[0]);
          this.getArray = resp.datos.rows;
        }
        deferred.resolve(this.getArray);
        this.cantidad = resp.datos.count;
        this.generandoCSV = resp.datos.count > 0;
      } else {
        this.Loading.hide();
        deferred.reject();
      }
    });
    return deferred.promise;
  }

  obtenerCabeceras() {
    return this.cabeceras;
  }

  $$obtenerCabeceras(datos) {
    let cabeceras = [];
    for (let i in datos) {
      cabeceras.push(i);
    }
    return cabeceras;
  }

  limpiarDatosReporte () {
    if (this.cantidad > 0) {
      this.cabeceras = ['Sin_registros'];
      this.getArray = [];
      this.cantidad = 0;
    }
  }
}

export default ReporteCsvController;
