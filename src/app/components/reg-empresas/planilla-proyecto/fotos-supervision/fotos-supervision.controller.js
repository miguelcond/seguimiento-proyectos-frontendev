'use strict';

import modalController from './foto-supervision.modal.controller';
import modalTemplate from './foto-supervision.modal.html';

import modalUbicacionController from '../vista-ubicacion/modal.controller';
import modalUbicacionTemplate from '../vista-ubicacion/modal.html';

class FotosSupervisionController {
  constructor(DataService, Storage, SidenavFactory, $state, Modal, Message, $window, Usuarios) {
    'ngInject';
    this.DataService = DataService;
    this.Storage = Storage;
    this.SidenavFactory = SidenavFactory;
    this.$state = $state;
    this.$window = $window;
    this.Modal = Modal;
    this.Message = Message;
    this.Usuarios = Usuarios;

    this.user = Storage.getUser();
  }

  $onInit() {
    for (var i = 0; i < this.fotos.length; i++) {
      this.fotos[i].nro = i + 1;
      this.getFoto(this.fotos[i]);
    }
    this.puedeEliminar = this.Usuarios.esSupervisor() && this.supervision && this.supervision.estado_actual && this.supervision.estado_actual.codigo === 'EN_SUPERVISION_FDI';
  }

  getFoto(foto) {
    this.DataService.get(`fotos/${foto.id_foto_supervision}`).then(response => {
      foto.base64 = response.datos;
      foto.coordenadas = response.coordenadas;
      foto.mostrar = true;
    });
  }

  mostrarPopup2(foto) {
    if (foto.base64) {
      this.Modal.showImageBase64(foto, 'Foto Supervision ' + foto.nro);
    }
  }

  mostrarPopup(foto) {
    this.Modal.show({
      template: modalTemplate,
      controller: modalController,
      data: {
        foto: foto,
        fotos: this.fotos
      },
      size: 'lg'
    });
  }

  mostrarUbicacion(foto) {
    //this.$window.open(`https://www.openstreetmap.org/?mlat=${foto.coordenadas.coordinates[1]}&mlon=${foto.coordenadas.coordinates[0]}#map=18/${foto.coordenadas.coordinates[1]}/${foto.coordenadas.coordinates[0]}`,'_blank');
    this.Modal.show({
      template: modalUbicacionTemplate,
      controller: modalUbicacionController,
      data: {
        foto: foto,
        fotos: this.fotos
      },
      size: 'lg'
    });
  }

  eliminarFoto(foto) {
    let datos = angular.copy(foto);
    datos.path_fotografia = foto.path_fotografia.substring(foto.path_fotografia.indexOf('_') + 1);
    datos.fid_supervision = foto.fid_supervision || this.supervision.id_supervision;
    this.Modal.confirm(`¿Está seguro/a de eliminar la Foto supervisión ${foto.nro}?`, () => {
      this.DataService.delete(`fotos/${foto.id_foto_supervision}?supervision=${datos.fid_supervision}`,datos).then((result) => {
        if (result && result.finalizado) {
          this.Message.success(result.mensaje);
          this.$state.reload();
        }
      });
    });
  }
}

export default FotosSupervisionController;
