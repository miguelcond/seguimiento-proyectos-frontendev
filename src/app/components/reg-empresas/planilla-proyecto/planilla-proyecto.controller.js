/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

import modalComputoTemplate from './computo-metrico.modal.html';
import modalComputoController from './computo-metrico.modal.controller';
import modalAnticipoTemplate from './verificacion-anticipo.modal.html';
import modalAnticipoController from './verificacion-anticipo.modal.controller';
import modalItemsNoCerradosTemplate from './items-no-cerrados.modal.html';
import modalItemsNoCerradosController from './items-no-cerrados.modal.controller';

class PlanillaProyectoController {
  constructor($scope, DataService, Modal, $state, $stateParams, Supervision, _, Storage, Util, $q, Flujos, apiUrl, Message, Usuarios) {
    'ngInject';
    this.$scope = $scope;
    this.DataService = DataService;
    this.Modal = Modal;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.Supervision = Supervision;
    this._ = _;
    this.Storage = Storage;
    this.Util = Util;
    this.$q = $q;
    this.Flujos = Flujos;
    this.apiUrl = apiUrl;
    this.Message = Message;
    this.items_modulo = [];
    this.items_x = [];
    this.filtroItems = 'TODOS';
    this.archivos = {};
    this.Usuarios = Usuarios;
  }

  $onInit() {
    this.idSupervision = this.$stateParams.idSupervision;
    this.getTipoUnidades();
    this.getSupervision();
    
    let rol = this.Storage.get('rol');
    if (rol && typeof rol === Array && rol.length > 0) {
      this.url = `supervisiones/${this.idSupervision}?rol=${rol[0].id_rol}`;
    } else {
      this.url = `supervisiones/${this.idSupervision}?rol=${rol.id_rol}`;
    }

    this.$scope.$on('transicion_accion_ejecutada', () => {
      this.$state.go('mis-proyectos');
    });
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.filtroItems', () => {
      if (!this.supervision) {
        return;
      }
      this.mostrarItems();
    });
  }

  mostrarItems() {
    switch (this.filtroItems) {
      case 'COMPLETADOS':
        this.filtrarItems((item) => {
          return item.completado;
        });
        break;
      case 'INCOMPLETOS':
        this.filtrarItems((item) => {
          return !item.completado;
        });
        break;
      case 'PARA_ENVIAR':
        this.filtrarItems((item) => {
          return item.cantidad_actual_fis !== 0;
        });
        break;
      default:
        this.filtrarItems();
    }
  }

  getSupervision() {
    this.usuario = this.Storage.getUser();
    this.DataService.get(`supervisiones/${this.idSupervision}`)
    .then(response => {
      if (response) {
        this.supervision = this.verificarModificaciones(response.datos);
        this.asignarDatosProyecto();
        this.mostrarItems();
        this.Flujos.getAtributosPermitidos(this.supervision);
        this.Supervision.setUsuario(this.usuario);
        this.Supervision.setSupervision(this.supervision);

        this.esEditable = this.Supervision.esEditable('computos');
        this.esEtapaEdicion = this.supervision.estado_supervision === 'REGISTRO_PLANILLA_FDI';
        if (!this.esEtapaEdicion) {
          this.filtroItems = 'PARA_ENVIAR';
        }
        this.prepararBoletas();
        this.supervision.supervisiones_anteriores.map(supervision => {
          supervision.numero = this.Util.getNumeroOrdinal(supervision.nro_supervision);
        });
        const DESEMBOLSOS = this._.map(this.supervision.supervisiones_anteriores, 'desembolso');
        this.supervision.monto_desembolso_total = this.Util.sumar(DESEMBOLSOS);
        this.verificarDeudas();
        this.verificarItemsNoCerrados();

        let comprobante = 'SupCP_Comprobante';
        if(this.supervision.path_comprobante){
          comprobante = this.supervision.path_comprobante.substring(1, this.supervision.path_comprobante.indexOf(']'));
        }

        this.archivos = {
          urlDocComprobantePago: this.supervision? `proyectos/${this.supervision.fid_proyecto}/doc/${comprobante}` : '',
          urlDocComprobantePagoDescarga: this.supervision? `proyectos/${this.supervision.fid_proyecto}/doc/${comprobante}/descargar` : ''
        };  
      }
    });
  }

  verificarModificaciones(datos) {
    // TODO optimizar
    let cabecera = datos.modulos[0].items[0].item_historico ? datos.modulos[0].items[0].item_historico : [];
    for (let i in datos.modulos) {
      let items = datos.modulos[i].items;
      for (let j in items) {
        let ij = items[j].item_historico ? items[j].item_historico.length : 0;
        for (let k = cabecera.length; k > ij; k--) {
          items[j].item_historico.unshift({
            version: (cabecera[k-1].version - ij)
          });
        }
      }
    }

    if(datos.proyecto.proyecto_historico[0] && !datos.proyecto.proyecto_historico[0].tipo_modificacion_proyecto) {
      datos.proyecto.proyecto_historico[0].tipo_modificacion_proyecto = {
        nombre: 'Contrato Original'
      }
    }
    return datos;
  }

  asignarDatosProyecto() {
    this.proyecto = this.supervision.proyecto;
    if (this.proyecto.doc_especificaciones_tecnicas) {
      this.urlDescEspTecnicas = `${this.apiUrl}proyectos/${this.supervision.fid_proyecto}/doc/ET/descargar`;
    }
    this.urlDescPlanilla = `${this.apiUrl}proyectos/${this.supervision.fid_proyecto}/doc/PA_${this.supervision.fid_adjudicacion}-${this.supervision.nro_supervision}/descargar`;
    if (this.supervision.version_adjudicacion) {
      this.urlDescPlanilla = `${this.apiUrl}proyectos/${this.supervision.fid_proyecto}/doc/PA_${this.supervision.fid_adjudicacion}-${this.supervision.nro_supervision}/descargar`;
      this.urlDescComputosResumen = `${this.apiUrl}proyectos/${this.supervision.fid_proyecto}/doc/CMR_${this.supervision.nro_supervision}/descargar`;
      this.urlDescComputosItem = `${this.apiUrl}proyectos/${this.supervision.fid_proyecto}/doc/CM_`;
    }
    
    this.financiamiento = this.proyecto.formularios && this.proyecto.formularios.length > 0 ? this.proyecto.formularios[0] : {};
    this.monto_contraparte = this.financiamiento.form_model && this.financiamiento.form_model.monto_contraparte ? this.financiamiento.form_model.monto_contraparte : 0;
    this.monto_beneficiarios = this.financiamiento.form_model && this.financiamiento.form_model.monto_beneficiarios ? this.financiamiento.form_model.monto_beneficiarios : 0;
    this.monto_otros = this.financiamiento.form_model && this.financiamiento.form_model.monto_otros ? this.financiamiento.form_model.monto_otros : 0;
  }

  irComputo(item) {
    this.$state.go('computo-proyecto', {idSupervision: this.idSupervision, idItem: item.id_item});
  }

  mostrarComputo(item) {
    this.Modal.show({
      template: modalComputoTemplate,
      controller: modalComputoController,
      size: 'xlg',
      data: {
        supervision: this.supervision,
        item: item,
        modoVista: this.supervision.estado_actual.atributos['computos'] ? false : true
      }
    }).result.then( (modificados) => {
      if (!modificados) {
        return;
      }
      if (this.Supervision.esEditable('computos') && modificados.length > 0) {
        this.getSupervision();
      }
    }).catch( () => {});
  }

  getTipoUnidades() {
    this.DataService.get(`parametricas/UNIDAD`).then(response => {
      if (response) {
        this.unidades = response.datos.rows;
        this.unidades_obj = {};
        this.unidades.forEach( uni => {
          this.unidades_obj[uni.codigo] = uni;
        });
      }
    });
  }

  getMgColor(item) {
    if (item.total_acumulado_fis === item.cantidad) {
      return  'bl-success';
    }
    if (item.total_acumulado_fis === 0) {
      return  'bl-danger';
    }
    if (item.cantidad_actual_fis > 0) {
      return  'bl-secondary';
    }
    return  'bl-warning';
  }

  getTitle(item) {
    if (item.total_acumulado_fis === item.cantidad) {
      return  'Completado';
    }
    if (item.total_acumulado_fis ===  0) {
      return  'Sin avance';
    }
    if (item.cantidad_actual_fis > 0) {
      return  'Con avance';
    }
    return  'Con avance en anterior(es) planilla(s)';
  }

  // TODO obtener valores desde el backend
  calcularFechaAmpliada(fecha) {
    if(fecha && this.proyecto && this.proyecto.plazo_ampliacion) {
      fecha = new Date(fecha);
      fecha.setDate(fecha.getDate()+this.proyecto.plazo_ampliacion.por_volumenes+this.proyecto.plazo_ampliacion.por_compensacion);
    }
    return fecha;
  }

  filtrarItems(condicion = () => { return true }) {
    this.supervision.modulos.forEach( modulo => {
      modulo.items.forEach( item => {
        item.show = condicion(item);
      });
      modulo.show = this._.some(modulo.items, { show: true });
    });
    this.Supervision.setSupervision(this.supervision);
  }

  volverSupervisiones() {
    this.$state.go('avance-proyecto', { idProyecto: this.supervision.fid_proyecto, matricula: this.Storage.get('user').matricula });
  }

  verificarDeudas() {
    if (this.proyecto.anticipo === 0) {
      return;
    }
    const DESCUENTOS = this._.map(this.supervision.supervisiones_anteriores, 'descuento_anticipo');
    const TOTAL_DESCUENTOS = this.Util.sumar(DESCUENTOS);
    this.deudaAnticipo = this.Util.restar(this.proyecto.anticipo, TOTAL_DESCUENTOS);
    if (this.deudaAnticipo === 0) {
      return;
    }
    this.tieneDeudas = true;
    this.desembolso = this.Supervision.calcularDescuento(this.supervision.tipo_descuento_anticipo, this.supervision.monto_total_actual_ejecutado, this.deudaAnticipo, this.proyecto.porcentaje_anticipo);
  }

  verificarItemsNoCerrados() {
    const ITEMS = this.Supervision.getItems();
    this.itemsNoCerrados =  this._.filter(ITEMS, item => {
      return ((item.falta_ejecutar_fis === 0 && item.falta_ejecutar_fin !== 0) ||
      (item.ejecutar_porcentaje_fis === 0 && item.falta_ejecutar_fis > 0)) && item.estado !== 'CERRADO';
    });
  }

  verificarPlanilla() {
    let defer = this.$q.defer();
    if (this.Supervision.esEditable('tipo_descuento_anticipo')) {
      return this.verificarAnticipo(defer);
    }
    if (this.estaEnTecnico(defer)) {
      return this.verificarDatosParaTecnico(defer);
    }
    if (this.requiereInforme(defer)) {
      if (!this.tieneInforme()) {
        this.Message.warning('Debe completar el informe técnico de conclusiones');
        defer.reject();
      } else {
        defer.resolve({confirmar: true});
      }
    } else {
      defer.resolve({confirmar: true});
    }
    return defer.promise;
  }

  verificarAnticipo(defer) {
    if (!this.tieneAvances()) {
      this.Message.warning(`No tiene avances para enviar.`);
      return defer.promise;
    }

    if (this.tieneAnticipo() && this.deudaAnticipo > 0) {
      const modalConfirmacion =  this.Modal.show({
        template: modalAnticipoTemplate,
        controller: modalAnticipoController,
        data: {
          supervision: this.supervision,
          deudaAnticipo: this.deudaAnticipo,
          esPlanillaCierre: this.supervision.monto_total_por_ejecutar === 0
        },
        size: 'lg'
      });
      modalConfirmacion.result.then( () => {
          defer.resolve();
      }, () => { });
    } else {
      defer.resolve();
    }
    return defer.promise;
  }

  tieneInforme() {
    return this.supervision.informe ? true : false;
  }

  verificarDatosParaTecnico(defer) {
    if (this.itemsNoCerrados.length > 0) {
      const modalConfirmacion = this.Modal.show({
        template: modalItemsNoCerradosTemplate,
        controller: modalItemsNoCerradosController,
        data: {
          items: this.itemsNoCerrados
        },
        size: 'lg'
      });
      modalConfirmacion.result.then( (aprobar) => {
        if (aprobar) {
          defer.resolve();
        }
      }, () => { });
    } else if (this.supervision.porcentaje_avance_fis === 100) {
      this.Modal.confirm(`Se le recuerda que ésta es la <strong>Planilla de Cierre</strong>.
        <p>¿Está seguro/a de aprobar la presente planilla, tomando en cuenta que con ella finalizará el registro de los avances del proyecto?</p>
        <p>En caso de estar seguro/a seleccione la opción <b>APROBAR</b>.</p>`, () => {
        defer.resolve();
      }, null,`Confirmación previa al cierre del proyecto`, 'APROBAR');
    } else {
      defer.resolve({confirmar: true});
    }
    return defer.promise;
  }

  tieneAnticipo() {
    return this.supervision.proyecto.porcentaje_anticipo > 0;
  }

  tieneAvances() {
    return this.supervision.monto_total_actual_ejecutado > 0;
  }

  estaEnTecnico() {
    return this.supervision.estado_supervision === 'EN_TECNICO_FDI';
  }

  requiereInforme() {
    return this.supervision.estado_supervision === 'EN_SUPERVISION_FDI';
  }

  irAtras() {
    window.history.back();
  }

  prepararBoletas() {
    this.boletasAnticipo = this.getBoletaPorTipo('TB_CORRECTA_INVERSION');
    this.boletasCcontrato = this.getBoletaPorTipo('TB_CUMPLIMIENTO_CONTRATO');
  }

  getBoletaPorTipo(tipo) {
    return this._.filter(this.supervision.proyecto.boletas, boleta => {
      return boleta.tipo_boleta === tipo && boleta.estado === 'ACTIVO';
    });
  }

  getTipoClass(tipo_modificacion) {
    if (tipo_modificacion == 'TM_CONTRATO_MODIFICATORIO') return 'cmodificatorio';
    if (tipo_modificacion == 'TM_ORDEN_CAMBIO') return 'orden-cambio';
    if (tipo_modificacion == 'TM_ORDEN_TRABAJO') return 'orden-trabajo';
    return '';
  }

  esTecnico() {
    return this.Usuarios.esTecnicoResponsable();
  }

  permitirVerAreaComprobante() {
    return this.supervision && (this.supervision.estado_supervision == 'EN_FISCAL_FDI' || this.supervision.estado_supervision == 'EN_ARCHIVO_FDI');
  }

  permiteModificarComprobante() {
    return this.supervision.estado_supervision == 'EN_FISCAL_FDI' && this.Usuarios.esFiscal();
  }
}

export default PlanillaProyectoController;
