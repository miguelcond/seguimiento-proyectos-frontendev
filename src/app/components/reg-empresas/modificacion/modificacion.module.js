'use strict';

import ModificacionComponent from './modificacion.component';
import ModuloComponent from './nuevomodulo/modulo.component';
import ModificacionService from './modificacion.service';

const Modificacion = angular
    .module('app.modificacion', [])
    .component('modificacion', ModificacionComponent)
    .component('modulo', ModuloComponent)
    .config(($stateProvider) => {
      $stateProvider
          .state('modificacion', {
            url: '/modificacion/{proyecto:int}/{adjudicacion:int}/version/{version:int}',
            component: 'modificacion',
            params: {
              adjudicacion: 0,
              tipoModificacion: 0,
              version: 0,
              edicion: false,
              crear: false
            }
          });
    })
    .service('Modificacion', ModificacionService)
    .name;

export default Modificacion;
