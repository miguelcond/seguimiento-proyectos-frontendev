/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

import modalFechaModificacionTemplate from './modal/fechamodificacion.html';
import modalFechaModificacionController from './modal/fechamodificacion.controller.js';

class ModificacionController {
  constructor($scope, $state, $stateParams, $q, $window, DataService, Modal, Storage, Util, Message, Modificacion, decimales, Usuarios) {
    'ngInject';
    this.$scope = $scope;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$q = $q;
    this.$window = $window;
    this.DataService = DataService;
    this.Modal = Modal;
    this.Storage = Storage;
    this.Util = Util;
    this.Message = Message;
    this.Modificacion = Modificacion;
    this.decimales = decimales;
    this.Usuarios = Usuarios;

    this.usuario = this.Storage.getUser();
    if (['SUPERVISOR_FDI','TECNICO_FDI','FISCAL_FDI','JEFE_REVISION_FDI','RESP_GAM_GAIOC'].indexOf(this.usuario.rol.nombre) < 0) {
      Message.warning('No tiene permisos suficientes para ver la modificación');
      this.$window.history.back();
    }
    this.filtroItems = 'TODOS';

    this.tipoModificacion = $stateParams.tipoModificacion;
    this.adjudicacion = $stateParams.adjudicacion;
    this.tabIndex = 1;
    this.configRestarDias = 0;
  }

  $onInit() {
    if (this.$stateParams.crear) {
      this.$stateParams.crear = false;
      this.crearModificacion();
    } else {
      this.obtenerModificacion();
      this.obtenerConfigCalcFechas();
    }

    // TODO verificar la URL con el backend
    this.url = `proyectos/${this.$stateParams.proyecto}/adjudicaciones/${this.$stateParams.adjudicacion}/procesar?rol=${this.Storage.get('rol').id_rol}`;

    this.mensajeConfirmacion = {
      "Finalizar": `¿Está seguro de finalizar y enviar las modificaciones?<br>
                    Se le informa que estas modificaciones, una vez aprobadas serán incorporadas a la planilla de avance.`,
      "Cancelar": `¿Está seguro/a de cancelar la modificación?<br>La cancelación revertirá los cambios realizados en la presente modificación.`,
      "Guardar": `Se guardarán los cambios realizados.`
    };

    this.modificandoPlazo = this.modificandoItems = this.modoVista = !this.$stateParams.edicion;

    this.$scope.$on('transicion_accion_ejecutada', () => {
      this.$state.go('proyectos-pendientes');
    });

    /* DOCUMENTO */
    this.uploadCallback = (file) => {
      let data = {
        "doc_respaldo_modificacion": angular.copy(file)
      }
      this.DataService.put(`proyectos/${this.$stateParams.proyecto}/adjudicaciones/${this.$stateParams.adjudicacion}/guardar`, data).then(response => {
        if (response && response.finalizado) {
          this.informe_tecnico_registrado = true;
          //this.asignarDatos(file, response.datos.documento);
        }
      });
    }
    this.uploadRespaldo = (file) => {
      if (!file.name) {
        this.Message.warning(`Debe escribir el título del documento.`);
        return;
      }
      let datos = this.proyecto.adjudicacion.documentos.find((d)=>{return d.datos.name==file.name});
      if (datos) datos = datos.datos;
      let data = { documento_tecnico: angular.copy(file) }
      data.documento_tecnico.datos = datos;
      this.DataService.put(`proyectos/${this.$stateParams.proyecto}/adjudicaciones/${this.$stateParams.adjudicacion}/guardar`, data).then(response => {
        if (response && response.finalizado) {
          let temp = response.datos.documentos.find((d)=>{return d.name==datos.name});
          if (temp) {
            datos.archivo = temp.archivo;
            datos.codigo = temp.codigo;
          }
        }
      });
    }
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.filtroItems', () => {
      if (!this.proyecto) {
        return;
      }
      this.mostrarItems();
    });
  }

  cancelar() {
    this.Modal.confirm(`¿Está seguro/a de cancelar la modificación?<br>Se revertirán los cambios realizados en la presente modificación.`,
      () => {

      }, null, 'Cancelación de modificación','Sí, estoy seguro/a');
  }

  finalizar() {
    this.Modal.confirm(`¿Está seguro de finalizar y enviar las modificaciones?<br>
      Se le informa que estas modificaciones de fecha __/__/__ una vez aprobadas serán incorporadas a la planilla de avance.`,
      () => {

      }, null, 'Finalizar Modificación','Sí, estoy seguro/a');
  }

  crearModificacion() {
    let tipo = 'TM';
    switch(this.tipoModificacion) {
      case 1:
        tipo = 'TM_ORDEN_TRABAJO';
        break;
      case 2:
        tipo = 'TM_ORDEN_CAMBIO';
        break;
      case 3:
        tipo = 'TM_CONTRATO_MODIFICATORIO';
        break;
      default:
        tipo = 'TM';
        break;
    }
    // let tipo = this.tipoModificacion === 3 ? 'TM_CONTRATO_MODIFICATORIO' : this.tipoModificacion === 2 ? 'TM_ORDEN_CAMBIO' : this.tipoModificacion === 1 ? 'TM_ORDEN_TRABAJO' : 'TM';
    this.DataService.put(`proyectos/${this.$stateParams.proyecto}/modificaciones/${this.adjudicacion}?tipo=${tipo}`).then((result) => {
      if (result && result.finalizado) {
        //this.proyecto = result.datos;
        //this.agregarCamposModificar(result.datos);
        this.$state.go('modificacion',{
          proyecto: this.$stateParams.proyecto,
          adjudicacion: this.adjudicacion,
          version: result.datos.adjudicacion.version,
          tipoModificacion: tipo,
          edicion: true,
          crear: false
        });
      } else {
        //this.$state.go('mis-proyectos',null);
        this.$window.history.back();
      }
    });
  }

  obtenerModificacion() {
    const storedRol = this.Storage.get('rol');
    let query = {};
    if (typeof storedRol === Array && storedRol.length > 0) {
      query = {
        rol:  storedRol[0].id_rol, // Se envia el id del primer rol en el array (Si existe mas de uno)
      };
    } else if (storedRol.hasOwnProperty('id_rol')) {
      query = {
        rol:  storedRol.id_rol,
      };
    }

    if (this.$stateParams.version) {
      query.version = this.$stateParams.version;
    }
    let url = `proyectos/${this.$stateParams.proyecto}/modificaciones/${this.$stateParams.adjudicacion}?${this.Util.serialize(query)}`;
    this.DataService.get(url).then((result) => {
      if (result && result.finalizado) {

        switch (result.datos.adjudicacion.tipo_modificacion) {
          case 'TM_ORDEN_TRABAJO':
            this.tipoModificacion = 1;
            break;
          case 'TM_ORDEN_CAMBIO':
            this.tipoModificacion = 2;
            break;
          case 'TM_CONTRATO_MODIFICATORIO':
            this.tipoModificacion = 3;
            break;
          default:
            this.tipoModificacion = 0;
            break;
        }

        if (!result.datos.modificaciones || !result.datos.modificaciones.nombre) {
          if (this.tipoModificacion != 0) this.$state.go('proyectos-pendientes',null);
        }
        //this.proyecto = result.datos;
        this.agregarCamposModificar(result.datos);
        this.proyecto.idProyecto = this.$stateParams.proyecto;

        if (this.proyecto.adjudicacion.doc_respaldo_modificacion) {
          let a = this.proyecto.adjudicacion.doc_respaldo_modificacion.indexOf('[') + 1;
          let b = this.proyecto.adjudicacion.doc_respaldo_modificacion.indexOf(']');
          let codigo = this.proyecto.adjudicacion.doc_respaldo_modificacion.substring(a,b);
          this.DataService.get(`proyectos/${this.proyecto.idProyecto}/doc/${codigo}`).then((doc) => {
            if (doc && doc.finalizado) {
              this.informe_tecnico = doc.datos;
              this.informe_tecnico_registrado = true;
            }
          });
        }
        // Documentos adjuntos
        if (this.proyecto.adjudicacion.documentos) {
          let documentos = [];
          for (let i in this.proyecto.adjudicacion.documentos) {
            documentos.push({ datos: this.proyecto.adjudicacion.documentos[i] });
          }
          this.proyecto.adjudicacion.documentos = documentos;
        }
        if (!this.$stateParams.edicion && this.proyecto.adjudicacion.estado_actual) {
          this.$stateParams.edicion = this.proyecto.adjudicacion.estado_actual.codigo === 'MODIFICADO_FDI' && this.Usuarios.esSupervisor();
          this.modoVista = !this.$stateParams.edicion;
        }
      } else {
        this.$state.go('proyectos-pendientes',null);
      }
    });
  }

  agregarCamposModificar(proyecto) {
    if (proyecto) {
      // Historial
      proyecto.modificaciones.total_variacion = 0;    // Monto total de variacion sumando todas los tipos de modificaciones
      proyecto.modificaciones.total_variacion_ot = 0; // Monto total de variacion sumando las modificaciones ORDEN_TRABAJO
      proyecto.modificaciones.total_variacion_oc = 0; // Monto total de variacion sumando las modificaciones ORDEN_CAMBIO
      proyecto.modificaciones.total_variacion_cm = 0; // Monto total de variacion sumando las modificaciones CONTRATO_MODIFICATORIO
      proyecto.modificaciones.total_variacion_porcentaje = 0;
      proyecto.modificaciones.total_variacion_porcentaje_ot = 0;
      proyecto.modificaciones.total_variacion_porcentaje_oc = 0;
      proyecto.modificaciones.total_variacion_porcentaje_cm = 0;
      proyecto.plazo_ampliacion_total = 0;
      for (let i in proyecto.modificaciones.historial) {
        let version = proyecto.modificaciones.historial[i];
        proyecto.modificaciones.total_variacion = this.Util.sumar([version.variacion, proyecto.modificaciones.total_variacion]);
        if (version.tipo_modificacion === 'TM_ORDEN_TRABAJO') {
          proyecto.modificaciones.total_variacion_ot = this.Util.sumar([version.variacion, proyecto.modificaciones.total_variacion_ot]);
          // version.variacion_porcentaje_ot = this.Util.dividir( this.Util.multiplicar([version.variacion, 100]), proyecto.version_inicial.monto);
          // proyecto.modificaciones.total_variacion_porcentaje_ot = this.Util.sumar([proyecto.modificaciones.total_variacion_porcentaje_ot, version.variacion_porcentaje_ot]);
        }
        if (version.tipo_modificacion === 'TM_ORDEN_CAMBIO') {
          proyecto.modificaciones.total_variacion_oc = this.Util.sumar([version.variacion, proyecto.modificaciones.total_variacion_oc]);
          version.variacion_porcentaje_oc = this.Util.dividir( this.Util.multiplicar([version.variacion, 100]), proyecto.version_inicial.monto);
          proyecto.modificaciones.total_variacion_porcentaje_oc = this.Util.sumar([proyecto.modificaciones.total_variacion_porcentaje_oc, version.variacion_porcentaje_oc]);
        }
        if (version.tipo_modificacion === 'TM_CONTRATO_MODIFICATORIO') {
          proyecto.modificaciones.total_variacion_cm = this.Util.sumar([version.variacion, proyecto.modificaciones.total_variacion_cm]);
          version.variacion_porcentaje_cm = this.Util.dividir( this.Util.multiplicar([version.variacion, 100]), proyecto.version_inicial.monto);
          proyecto.modificaciones.total_variacion_porcentaje_cm = this.Util.sumar([proyecto.modificaciones.total_variacion_porcentaje_cm, version.variacion_porcentaje_cm]);
        }
        version.plazo_ampliacion_total = proyecto.plazo_ampliacion_total += version.plazo_ampliacion? (version.plazo_ampliacion.por_volumenes + version.plazo_ampliacion.por_compensacion): 0;
        version.nueva_fecha_provisional = new Date(proyecto.orden_proceder);
        version.nueva_fecha_provisional.setDate(version.nueva_fecha_provisional.getDate() + version.plazo_ampliacion_total + proyecto.plazo_ejecucion - this.configRestarDias);
      }
      proyecto.modificaciones.total_variacion_h = 0;
      if (proyecto.tipo_modificacion === 'TM_ORDEN_TRABAJO') {
        proyecto.modificaciones.total_variacion_h = proyecto.modificaciones.total_variacion_ot;
      }
      if (proyecto.tipo_modificacion === 'TM_ORDEN_CAMBIO') {
        proyecto.modificaciones.total_variacion_h = proyecto.modificaciones.total_variacion_oc;
      }
      if (proyecto.tipo_modificacion === 'TM_CONTRATO_MODIFICATORIO') {
        proyecto.modificaciones.total_variacion_h = proyecto.modificaciones.total_variacion_cm;
      }
      // Modulos
      this.nuevosmodulos = [];
      this.cabecera = angular.copy(proyecto.modulos[0].items[0]);
      if (!proyecto.version_anterior || !proyecto.version_anterior.tipo_modificacion_adjudicacion) {
        this.cabecera.version_anterior = null;
      }
      // TODO verificar backend
      if (proyecto.adjudicacion.version == 2 && !this.cabecera.version_inicial) {
        this.cabecera.version_anterior = {};
      }
      if (proyecto.adjudicacion.version == 1) {
        proyecto.modificaciones.nombre = "Contrato Original";
      }
      let modulos = [];
      for (let i in proyecto.modulos) {
        if (proyecto.modulos[i].nombre.indexOf(proyecto.modificaciones.nombre) >= 0) {
          this.nuevosmodulos.push(proyecto.modulos[i]);
          this.modificandoItems = true;
        } else {
          let modulo = proyecto.modulos[i];
          modulo.isCollapsed = true && !this.modoVista;
          for (let j in modulo.items) {
            if (modulo.items[j].version_anterior && modulo.items[j].cantidad != modulo.items[j].version_anterior.cantidad) {
              this.modificandoItems = true;
              modulo.isCollapsed = false;
            }
            // Verificar avance
            if (!modulo.items[j].avance_actual) {
              modulo.items[j].avance_actual = 0;
            }
            if (modulo.items[j].estado === 'CERRADO') {
              modulo.items[j].avance_actual = modulo.items[j].cantidad;
            }
            // Verificar cabeceras
            if (!modulo.items[j].version_inicial && this.cabecera.version_inicial) {
              modulo.items[j].version_inicial = {};
            }
            modulo.items[j].show = true;
            // Para verificar si se modifico el valor
            modulo.items[j].cantidad_ant = modulo.items[j].cantidad;
          }
          modulos.push(modulo);
        }
      }
      proyecto.modulos = modulos;
      this.proyecto = proyecto;
      this.modificandoPlazo = this.proyecto.plazo_ampliacion && (this.proyecto.plazo_ampliacion.por_volumenes || this.proyecto.plazo_ampliacion.por_compensacion);
      this.recalcularMontos();
      this.recalcularFechas();
      // Valores para verificar se se han cambiado los datos.
      this.proyecto.plazo_ampliacion_ant = {};
    }
  }

  // TODO obtener valores desde el backend
  calcularFechaAmpliada(fecha) {
    if (fecha && this.proyecto && this.proyecto.plazo_ampliacion) {
      fecha = new Date(fecha);
      fecha.setDate(fecha.getDate() + this.proyecto.plazo_ampliacion.por_volumenes + this.proyecto.plazo_ampliacion.por_compensacion);
    }
    return fecha;
  }

  /* MODIFICAR ITEMS */
  modificarCantidadItem(item) {
    if (item && item.cantidad_ant != item.cantidad) {
      this.DataService.put(`items/${item.id_item}`,{cantidad:item.cantidad}).then((result) => {
        if (result && result.finalizado) {
          this.Message.success(result.mensaje);
          item.error = false;
        } else {
          item.error = true;
        }
        item.cantidad_ant = item.cantidad;
      });
      this.recalcularMontos();
    }
  }

  modificarPesoItem(item) {
    if (item && item.id_item > 0) {
      this.DataService.put(`items/${item.id_item}`,{peso_ponderado: item.peso_ponderado}).then((result) => {
        if (result && result.finalizado) {
          this.Message.success(result.mensaje);
        } else {
          this.Message.warning('Sucedió un error al tratar de actualizar el item, intente de nuevo por favor');
        }
      });
    }
  }

  recalcularMontos(item) {
    // TODO para optimizar solo sumar o restar el item modificado. (probar con item eliminado)
    item;
    // Recalcular monto total
    this.cantItems = 0;
    this.proyecto.monto_total_variacion_modificado = 0;
    for (let i in this.proyecto.modulos) {
      for (let j in this.proyecto.modulos[i].items) {
        this.cantItems++;
        let itemLooped = this.proyecto.modulos[i].items[j];
        // if(itemLooped.version_anterior && itemLooped.version_anterior.cantidad != itemLooped.cantidad) {
        //   this.proyecto.monto_total_variacion_modificado += this.Util.redondear(itemLooped.precio_unitario * (itemLooped.cantidad - itemLooped.version_anterior.cantidad));
        // }
        this.proyecto.monto_total_variacion_modificado = this.Util.sumar([this.proyecto.monto_total_variacion_modificado,
          this.Util.multiplicar([itemLooped.precio_unitario, itemLooped.cantidad])
        ]);
      }
    }
    for (let i in this.nuevosmodulos) {
      for (let j in this.nuevosmodulos[i].items) {
        let itemLooped = this.nuevosmodulos[i].items[j];
        itemLooped.nro_item = ++this.cantItems;
        if (itemLooped.precio_unitario && itemLooped.cantidad) {
          //this.proyecto.monto_total_variacion_modificado += this.Util.redondear(itemLooped.precio_unitario * itemLooped.cantidad);
          this.proyecto.monto_total_variacion_modificado = this.Util.sumar([this.proyecto.monto_total_variacion_modificado,
            this.Util.multiplicar([itemLooped.precio_unitario, itemLooped.cantidad])
          ]);
        }
      }
    }
    this.proyecto.monto_total_variacion_modificado = this.Util.restar(this.proyecto.monto_total_variacion_modificado, this.proyecto.version_anterior.monto);
    //this.proyecto.porcentaje_variacion = (this.proyecto.monto_total_variacion_modificado * 100) / this.proyecto.version_inicial.monto;
    // this.proyecto.porcentaje_variacion = this.Util.multiplicar([this.proyecto.monto_total_variacion_modificado, this.Util.dividir(100, this.proyecto.version_inicial.monto)]);
    this.proyecto.porcentaje_variacion = this.Util.dividir(this.Util.multiplicar([this.proyecto.monto_total_variacion_modificado, 100]), this.proyecto.version_inicial.monto);
    this.porcentajeLimite = this.tipoModificacion === 3 ? 0.1 : this.tipoModificacion === 2 ? 0.05 : 0;
    // TODO tambien debe sumarse con las anteriores modificaciones.
    //let monto_variacion = this.Util.redondear(this.proyecto.monto_total_variacion_modificado + this.proyecto.modificaciones.total_variacion);
    let monto_variacion = this.Util.sumar([this.proyecto.monto_total_variacion_modificado, this.proyecto.modificaciones.total_variacion_h]);
    //let monto_limite = this.Util.redondear(this.proyecto.version_inicial.monto * this.porcentajeLimite);
    let monto_limite = this.Util.multiplicar([this.proyecto.version_inicial.monto, this.porcentajeLimite]);
    this.porcentajeExcedido = (monto_variacion) > (monto_limite) || this.Util.restar(0,monto_variacion) > (monto_limite);
  }

  adicionarModulo() {
    if (!this.nuevosmodulos || !this.nuevosmodulos[0]) {
      let datos = {
        fid_proyecto: this.$stateParams.proyecto,
        fid_adjudicacion: this.$stateParams.adjudicacion,
        nombre: `ITEMS NUEVOS (${this.proyecto.modificaciones.nombre})`
      };
      this.DataService.post(`modulos`, datos).then( (result) => {
        if (result && result.finalizado) {
          if (!this.nuevosmodulos) {
            this.nuevosmodulos = [];
          }
          this.Message.success(result.mensaje);
            if (!result.datos.items) {
              result.datos.items = [];
            }
            if (result.datos.items.length <= 0) {
              result.datos.items.push({edicion: true});
            }
          this.nuevosmodulos.push(result.datos);
        }
      });
    }
  }

  modificarItems() {
    this.modificandoItems = !this.modificandoItems;
  }

  /* MODIFICACION DEL PLAZO */
  modificarPlazoAmpliacion(plazo_ampliacion) {
    if (plazo_ampliacion) {
      plazo_ampliacion.por_compensacion = plazo_ampliacion.por_compensacion || 0;
      plazo_ampliacion.por_volumenes = plazo_ampliacion.por_volumenes || 0;
      let datos = {
        plazo_ampliacion: {
          por_compensacion: parseInt(plazo_ampliacion.por_compensacion || 0),
          por_volumenes: parseInt(plazo_ampliacion.por_volumenes || 0)
        }
      }
      if( (this.proyecto.plazo_ampliacion_ant.por_volumenes!=plazo_ampliacion.por_volumenes ||
          this.proyecto.plazo_ampliacion_ant.por_compensacion!=plazo_ampliacion.por_compensacion) ) {
        delete this.proyecto.nueva_fecha_provisional;
        delete this.proyecto.nueva_fecha_definitiva;
        this.DataService.put(`proyectos/${this.$stateParams.proyecto}`, datos).then((result) => {
          if (result && result.finalizado) {
            this.Message.success(result.mensaje);
            this.proyecto.plazo_ampliacion_ant = angular.copy(plazo_ampliacion);
            this.recalcularFechas();
          }
        });
      }
    }
  }

  recalcularFechas() {
    let plazo_ampliacion = this.proyecto.plazo_ampliacion;
    let nro_dias = this.proyecto.plazo_recepcion_definitiva || 90;
    if (plazo_ampliacion && (parseInt(plazo_ampliacion.por_compensacion) || parseInt(plazo_ampliacion.por_volumenes)) ) {
      this.proyecto.nueva_fecha_provisional = new Date(this.proyecto.orden_proceder);
      // this.proyecto.nueva_fecha_provisional.setDate(this.proyecto.nueva_fecha_provisional.getDate() + parseInt(plazo_ampliacion.por_volumenes) + parseInt(plazo_ampliacion.por_compensacion) + this.proyecto.plazo_ejecucion - this.configRestarDias);
      this.proyecto.fecha_recepcion_provisional = this.proyecto.nueva_fecha_provisional.setDate(this.proyecto.nueva_fecha_provisional.getDate() + parseInt(plazo_ampliacion.por_volumenes) + parseInt(plazo_ampliacion.por_compensacion) + this.proyecto.plazo_ejecucion + this.proyecto.plazo_ampliacion_total - this.configRestarDias);
      this.proyecto.nueva_fecha_definitiva = new Date(this.proyecto.nueva_fecha_provisional);
      // this.proyecto.nueva_fecha_definitiva.setDate(this.proyecto.nueva_fecha_provisional.getDate() + nro_dias);
      this.proyecto.fecha_recepcion_definitiva = this.proyecto.nueva_fecha_definitiva.setDate(this.proyecto.nueva_fecha_provisional.getDate() + nro_dias);
    } else {
      delete this.proyecto.nueva_fecha_provisional;
      delete this.proyecto.nueva_fecha_definitiva;
      this.proyecto.fecha_recepcion_provisional = new Date(this.proyecto.orden_proceder);
      this.proyecto.fecha_recepcion_provisional.setDate(this.proyecto.fecha_recepcion_provisional.getDate() + this.proyecto.plazo_ejecucion + this.proyecto.plazo_ampliacion_total - this.configRestarDias);
      this.proyecto.fecha_recepcion_definitiva = new Date(this.proyecto.fecha_recepcion_provisional);
      this.proyecto.fecha_recepcion_definitiva.setDate(this.proyecto.fecha_recepcion_definitiva.getDate() + nro_dias);
    }
  }

  fechaSumDias(fecha, dias) {
    let fechaNueva = new Date(fecha);
    if (!isNaN(fechaNueva.getTime()) && !isNaN(parseInt(dias))) {
      fechaNueva.setDate(fechaNueva.getDate() + parseInt(dias));
      return fechaNueva;
    }
    return fecha;
  }

  modificarPlazo() {
    this.modificandoPlazo = !this.modificandoPlazo;
  }

  /* INFOMRE TECNICO */
  verInformeTecnico() {
    this.Modal.showDocumentBase64(this.informe_tecnico.base64, 'Informe', 'strBase64');
  }
  quitarInformeTecnico() {
    this.informe_tecnico_registrado = false;
    delete this.informe_tecnico;
  }
  /* RESPALDO TECNICO */
  modoEdicionDoc(documento) {
    return (documento.datos.estado.nombre === this.proyecto.adjudicacion.estado_actual.codigo &&
        documento.datos.rol.nombre === this.Usuarios.usuario.rol.nombre);
  }
  verRespaldoTecnico(documento) {
    this.DataService.get(`proyectos/${this.proyecto.idProyecto}/doc/${documento.datos.codigo}?name=${documento.datos.name}`).then((doc) => {
      if (doc && doc.finalizado) {
        documento.file = doc.datos;
        this.Modal.showDocumentBase64(documento.file.base64, 'Informe', 'strBase64');
      }
    });
  }
  cambiarRespaldoTecnico(documento) {
    delete documento.datos.archivo;
  }
  agregarRespaldoTecnico() {
    if (!this.proyecto.adjudicacion.documentos) this.proyecto.adjudicacion.documentos = [];
    this.proyecto.adjudicacion.documentos.push({
      datos: {
        rol: {
          nombre: this.Usuarios.usuario.rol.nombre,
          descripcion: this.Usuarios.usuario.rol.descripcion,
        },
        estado: {
          nombre: this.proyecto.adjudicacion.estado_actual.codigo,
          descripcion: this.proyecto.adjudicacion.estado_actual.nombre,
        }
      }
    });
  }
  eliminarRespaldoTecnico(documento) {
    let eliminado = false;
    let i = 0;
    this.Modal.confirm(`¿Está seguro de eliminar por completo el documento?`,
      () => {
        while (!eliminado && this.proyecto.adjudicacion.documentos[i]) {
          if (this.proyecto.adjudicacion.documentos[i].datos.name == documento.datos.name) {
            this.DataService.put(`proyectos/${this.$stateParams.proyecto}/adjudicaciones/${this.$stateParams.adjudicacion}/respaldo`, documento).then(doc => {
              if (doc && doc.finalizado) {
                this.proyecto.adjudicacion.documentos.splice(i,1);
              }
            });
            eliminado = true;
            break;
          }
          i++;
        }
      }, null, 'Eliminar documento','Sí, estoy seguro/a');
  }

  /* FUNCIONES UTILES */
  mostrarItems() {
    switch (this.filtroItems) {
      case 'COMPLETADOS':
        this.filtrarItems((item) => {
          return item.avance_actual === item.cantidad;
        });
        break;
      case 'INCOMPLETOS':
        this.filtrarItems((item) => {
          return item.avance_actual !== item.cantidad;
        });
        break;
      default:
        this.filtrarItems();
    }
  }

  getMgColor(item) {
    if (item.avance_actual === item.cantidad || item.estado === 'CERRADO') {
      return  'bl-success';
    }
    if (item.avance_actual === 0) {
      return  'bl-primary';
    }
    if (item.avance_actual > 0) {
      return  'bl-info';
    }
    return  'bl-warning';
  }

  getTitle(item) {
    if (item.avance_actual === item.cantidad || item.estado === 'CERRADO') {
      return  'Completado';
    }
    if (item.avance_actual ===  0) {
      return  'Sin avance';
    }
    if (item.avance_actual > 0) {
      return  'Con avance';
    }
    return  'Con avance';
  }

  filtrarItems(condicion = () => { return true }) {
    this.proyecto.modulos.forEach( modulo => {
      modulo.items.forEach( item => {
        item.show = condicion(item);
      });
      //modulo.show = this._.some(modulo.items, { show: true });
    });
  }

  irAtras() {
    window.history.back();
  }

  verificarInformeTecnico() {
    if (!this.informe_tecnico_registrado) {
      this.Message.warning(`Debe cargar el documento con la aprobación de la modificación.`);
    }
    return this.informe_tecnico_registrado;
  }

  verificarNuevosItems() {
    // Verificar documentos adjuntos
    // TODO debe ser configurable
    // if (this.nuevosmodulos && this.nuevosmodulos[0]) {
    //   for (let i in this.nuevosmodulos[0].items) {
    //     let item = this.nuevosmodulos[0].items[i];
    //     if (item.id_item && !item.especificaciones) {
    //       this.Message.warning(`Debe cargar el documento de especificaciones técnicas del item<br>Nro. ${item.nro_item} - <strong>${item.nombre}</strong>`);
    //       return false;
    //     }
    //     if (!this.nuevosmodulos[0].items[i].analisis_precios) {
    //       this.Message.warning(`Debe cargar el documento de análisis de precios unitarios del item<br>Nro. ${item.nro_item} - <strong>${item.nombre}</strong>`);
    //       return false;
    //     }
    //   }
    // }
    return true;
  }

  /* TRANSICION */
  verificarModificacion(accion) {
    let defer = this.$q.defer();
    // TODO Se debe verificar segun tipo modificacion
    // ---
    // Registrar la fecha de modificacion
    if (accion.label === 'Cancelar') {
      this.proyecto.fecha_modificacion = new Date();
      defer.resolve({confirmar: true});
    } else {
      // verificar campos requeridos
      if (!this.verificarInformeTecnico()) {
        defer.reject();
        return defer.promise;
      }
      if (!this.verificarNuevosItems()) {
        defer.reject();
        return defer.promise;
      }
      // Modal para registrar fecha
      const modalFechaModificacion =  this.Modal.show({
        template: modalFechaModificacionTemplate,
        controller: modalFechaModificacionController,
        data: this.proyecto,
        size: 'md'
      });
      modalFechaModificacion.result.then( (proyecto) => {
        this.proyecto = proyecto;
        defer.resolve();
      }, () => { });
    }
    //defer.resolve({confirmar: true});
    return defer.promise;
  }

  obtenerConfigCalcFechas() {
    this.DataService.get(`parametricas/CONFIG`).then((result) => {
      if (result && result.finalizado && result.datos && result.datos.rows) {
        result.datos.rows.forEach(element => {
          if (element.codigo === 'CFG_CALC_FECHA') {
            this.configRestarDias = parseInt(element.descripcion) || 0;
          }
        });
      }
    });
  }
}

export default ModificacionController;
