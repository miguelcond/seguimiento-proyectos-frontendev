'usr strict';

import controller from './avance-proyecto.controller';
import template from './avance-proyecto.html';

const AvanceProyectoComponent = {
  bindings: {},
  controller,
  template
};

export default AvanceProyectoComponent;
