'use strict';

class AvanceProyectoController {
  constructor(DataService, Storage, Modal, $state, $stateParams, DataTable, Proyecto) {
    'ngInject';
    this.DataService = DataService;
    this.Storage = Storage;
    this.Modal = Modal;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.DataTable = DataTable;
    this.Proyecto = Proyecto;
  }

  $onInit() {
    this.idProyecto = this.$stateParams.idProyecto;
    this.matricula = this.$stateParams.matricula;
    this.getProyecto();
    this.cargarTabla();
  }

  getProyecto() {
    this.DataService.get(`proyectos/empresas/${this.matricula}/${this.idProyecto}`)
    .then( response => {
      this.proyecto = response.datos;
      this.Proyecto.setCopia(response.datos);
    });
  }

  nuevaSupervision() {
    var data = {
      fid_proyecto: this.idProyecto,
      computo_metrico: []
    };
    this.DataService.post(`supervisiones`, data).then( response => {
      this.irPlanilla(this.matricula, this.idProyecto, response.datos.id_supervision);
    })
  }

  irPlanilla(matricula, idProyecto, idSupervision) {
      this.$state.go('planilla-proyecto',{ idSupervision });
  }

  // nuevaPlanilla() {
  //   this.$state.go('planilla', {codigo: this.idProyecto});
  //   // this.Modal.show({
  //   //   template: modalPlanillasTemplate,
  //   //   controller: modalAvanceProyectoController,
  //   //   size: 'md'
  //   // }).result.then( result => {
  //   //   this.$state.go('documentacion-proyecto', {codigo: result.id_proyecto});
  //   // });
  // }


  cargarTabla() {
    var colors = {
      NUEVO: 'default',
      ENVIADO: 'primary',
      APROBADO: 'success',
      PENDIENTE_PAGO: 'warning',
      OBSERVADO: 'danger',
    };
    this.planillas = {
      url: `supervisiones`,
      queryExtra: { fid_proyecto: this.idProyecto, origen: 'bandeja' },
      data: {
        formly: [
          {
            key: 'id_supervision',
            type: 'input',
            templateOptions: {
              label: 'ID',
              type: 'number'
            }
          },
          {
            key: 'fid_proyecto',
            type: 'fid_proyecto',
            templateOptions: {
              label: 'ID proyecto',
              type: 'number'
            }
          },
          {
            key: 'nro_supervision',
            type: 'input',
            templateOptions: {
              label: 'Nro',
              type: 'number'
            }
          },
          {
            key: 'fecha_inicio',
            type: 'datepicker',
            templateOptions: {
              label: 'Fecha de creación',
              type: 'date'
            }
          },
          {
            key: 'fecha_fin',
            type: 'datepicker',
            templateOptions: {
              label: 'Fecha de creación',
              type: 'date'
            }
          },
          {
            key: 'data',
            type: 'input',
            templateOptions: {
              label: 'Fid caeb',
              type: 'number'
            }
          },
          {
            key: 'estado_supervision',
            type: 'select',
            templateOptions: {
              label: 'Estado',
              type: 'select',
              options: [
                { value: 'NUEVO', name: 'NUEVO' },
                { value: 'ENVIADO', name: 'ENVIADO' },
                { value: 'APROBADO', name: 'APROBADO' },
                { value: 'OBSERVADO', name: 'OBSERVADO' }
              ]
            }
          },
        ]
      },
      fields: ['id_supervision', 'fid_proyecto', 'nro_supervision', 'fecha_inicio', 'fecha_fin', 'data', 'estado_supervision'],
      hiddenFields: ['id_supervision', 'fid_proyecto','id_producto', 'extra'],
      // fks: ['fid_formulario'],
      permission: {
        create: true,
        update: false,
        delete: false,
        filter: false
      },
      labels: { create: 'NUEVA PLANILLA' },
      eventCreate: ()=>{
        this.nuevaSupervision();
      },
      buttons: [
        {
          icon: 'eye',
          tooltip: 'Ver planilla',
          // if: (ev, item) => { return item.estado==='APROBADO'; },
          onclick: (ev, item) => {

            this.irPlanilla(this.matricula, this.idProyecto, item.id_supervision);
            // this.$log.log(ev, item);
            // this.Modal.pdf(`cert_sello_bolivia/productos_shb/ddjj_sello_bolivia/${item.id_sello_bolivia}`);
          }
        },
        // {
        //   icon: 'file-pdf-o',
        //   tooltip: 'Ver certificacion',
        //   if: (ev, item) => { return item.estado==='APROBADO'; },
        //   onclick: (ev, item) => {
        //     // this.$log.log(ev, item);
        //     // this.Modal.pdf(`certificaciones/documento/${item.id_certificacion}`, 'Certificación');
        //     this.Modal.pdf(`cert_sello_bolivia/productos_shb/ddjj_sello_bolivia/${item.id_sello_bolivia}/previsualizar_certificado`);
        //   }
        // },
        // {
        //   icon: 'eye',
        //   tooltip: 'Ver detalles de solicitud',
        //   if: (ev, item) => {
        //     return item.estado==='ENVIADO';
        //   },
        //   onclick: (ev, item) => {
        //     // this.$log.log(ev, item);
        //     item = angular.extend(item, item.extra);
        //     delete item.extra;
        //     this.Storage.set('selloBolivia', item);
        //     this.$location.path('detalleSolicitudSello');
        //   }
        // }
      ],
      clases: {
        estado: (item) => {
          // this.$log.log(item);
          return ['badge', 'badge-'+colors[item.estado]];
        }
      }
    };
  }

}

export default AvanceProyectoController;
