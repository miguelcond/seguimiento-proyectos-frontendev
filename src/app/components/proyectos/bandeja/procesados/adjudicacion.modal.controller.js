'use strict';

class NuevaAdjudicacionModalController {
  constructor($uibModalInstance, Storage, DataService, Message) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.Storage = Storage;
    this.DataService = DataService;
    this.Message = Message;
  }

  $onInit() {

  }

  adjudicarProyecto() {
    const datos = {
      matricula: this.Storage.getUser().matricula,
      codigo_acceso: this.codigo_acceso
    };
    this.DataService.post(`adjudicar`, datos).then( result => {
      if (result) {
        this.Message.success(result.mensaje);
        this.$uibModalInstance.close(result.datos);
      }
    });
  }

  cerrar() {
    this.$uibModalInstance.dismiss('cancel');
  }
}

export default NuevaAdjudicacionModalController;
