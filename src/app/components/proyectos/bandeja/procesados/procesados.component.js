'use strict';

import controller from './procesados.controller';
import template from './procesados.html';

const BandejaProcesadosComponent = {
  bindings:{},
  controller,
  template
};

export default BandejaProcesadosComponent;
