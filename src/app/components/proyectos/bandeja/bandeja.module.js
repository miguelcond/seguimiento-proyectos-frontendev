'use strict';

import BandejaPendientesComponent from './pendientes/pendientes.component';
import BandejaProcesadosComponent from './procesados/procesados.component';
// import ListaSupervisionesComponent from './supervisiones/supervisiones.component';
// import ListaModificacionesComponent from './modificaciones/button.component';
import ButtonTimelineComponent from '../../admin/timeline/button.component';
import ButtonEstadoComponent from '../../admin/estados/button.component';
// import ButtonParalizarComponent from './paralizar/button.component';

const Bandeja = angular
    .module('app.bandeja', [])
    .component('bandejaPendientes', BandejaPendientesComponent)
    .component('bandejaProcesados', BandejaProcesadosComponent)
    // .component('listaSupervisiones', ListaSupervisionesComponent)
    // .component('listaModificaciones', ListaModificacionesComponent)
    .component('buttonTimeline', ButtonTimelineComponent)
    .component('buttonEstado', ButtonEstadoComponent)
    // .component('buttonParalizar', ButtonParalizarComponent)
    .config(($stateProvider, $urlRouterProvider) => {
        $stateProvider
            .state('pendientes', {
                url: '/pendientes',
                component: 'bandejaPendientes'
            })
            .state('mis-proyectos', {
                url: '/mis-proyectos',
                component: 'bandejaProcesados'
            });
        $urlRouterProvider.otherwise('/mis-proyectos');
    })
    .name;

export default Bandeja;
