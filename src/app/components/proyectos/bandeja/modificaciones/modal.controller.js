'use strict';

class NuevaAdjudicacionModalController {
  constructor($uibModalInstance, $state, data, Modal, DataService, Storage, _) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.$state = $state;
    this.proyecto = data;
    this.Modal = Modal;
    this.DataService = DataService;
    this._ = _;
    this.modificando = true;
    this.user = Storage.getUser();
  }

  $onInit() {
    this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/modificaciones/historial`).then((result)=>{
      if(result && result.finalizado) {
        this.modificaciones = result.datos;
        this.existeSupervisionesEnCurso = this.tieneSupervisionesEnCurso();
        this.puedeModificar = !result.datos.find((version)=>{ return version.estado_proyecto!='APROBADO'; }) &&
        this.user.rol.nombre=='SUPERVISOR' && !this.existeSupervisionesEnCurso && this.proyecto.estado_proyecto !== 'CERRADO';
        this.modificando = (['SUPERVISOR','TECNICO_UPRE','RESP_DEPARTAMENTAL_UPRE','ENCAR_REGIONAL_UPRE','EMPRESA'].indexOf(this.user.rol.nombre)>=0)? true: false;
        let cant = this.modificaciones.length-1;
        if(cant>=0 && this.modificaciones[cant] && !this.modificaciones[cant].nombre) {
          this.modificaciones[cant].nombre = 'Contrato Original';
        }
      }
    });
  }

  tieneSupervisionesEnCurso() {
    return this._.some(this.proyecto.supervisiones, supervision => {
      return supervision.estado_supervision !== 'EN_ARCHIVO' && supervision.estado_supervision !== 'REGISTRO_PLANILLA';
    });
  }

  abrirModificacion(tipoModificacion) {
    let html = `
      1. Para realizar una modificación deberá contar con el respectivo documento de respaldo debidamente aprobado por las instancias pertinentes (Libro de Ordenes e Informes Técnicos para la Orden de Trabajo y Orden de Cambio y el respectivo contrato para el Contrato Modificatorio).
      <br/><br/>2. Una vez que este registrando modificaciones la empresa no podrá procesar una planilla de avance hasta que se finalice y apruebe la solicitud de modificación.
    `;
    this.Modal.confirm( html,
      ()=>{
        this.$uibModalInstance.close(false);
        this.$state.go('modificacion',{
          proyecto: this.proyecto.id_proyecto,
          tipoModificacion,
          edicion: true,
          crear: true
        });
      },null, 'Confirmar inicio de modificación','Confirmar');
  }

  irAModificacion(modificacion, edicion) {
    let tipoModificacion = modificacion.tipo_modificacion=='TM_ORDEN_TRABAJO'?1:modificacion.tipo_modificacion=='TM_ORDEN_CAMBIO'?2:modificacion.tipo_modificacion=='TM_CONTRATO_MODIFICATORIO'?3:0;
    this.$uibModalInstance.close(false);
    this.$state.go('modificacion',{
      proyecto: this.proyecto.id_proyecto,
      version: modificacion.version,
      tipoModificacion,
      edicion,
      crear: false
    });
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

  verModificar(modificacion) {
    return modificacion.estado_proyecto=='MODIFICADO' && this.user.rol.nombre=='SUPERVISOR';
  }
  verUltimaModificacion(modificacion) {
    return ['REVISION_MODIFICACION','MODIFICADO','CERRADO'].indexOf(modificacion.estado_proyecto)>=0 &&
           ['SUPERVISOR','TECNICO_UPRE','RESP_DEPARTAMENTAL_UPRE','ENCAR_REGIONAL_UPRE','EMPRESA'].indexOf(this.user.rol.nombre)>=0 &&
           !this.verModificar(modificacion);
  }
}

export default NuevaAdjudicacionModalController;
