'use strict';

class DesembolsoController {
  constructor(Proyecto, Modal, Util, Datetime) {
    'ngInject';

    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this.Util = Util;
    this.Datetime = Datetime;
  }

  $onInit() {
    this.archivo = {
      urlDocCertificacion: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/CIP` : '',
      urlDocCertificacionDescarga: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/CIP/descargar` : ''
    };
    let fechaConvenio = this.Datetime.getFormatDate(this.proyecto.fecha_suscripcion_convenio);
    this.validacion = {
      fecha_minima: this.Datetime.addDaysToDate(angular.copy(fechaConvenio), 1),
      mensaje_fecminima: `La fecha debe ser mayor a la fecha del convenio (${this.Datetime.parseDate(fechaConvenio, '/')})`
    };
  }

}

export default DesembolsoController;
