'use strict';

class RegistroFiscalSupervisorController {
  constructor($uibModalInstance ,DataService, $stateParams, data, _, Message) {
    'ngInject';

    this.DataService = DataService;
    this.$stateParams = $stateParams;
    this.data = data;
    this.$uibModalInstance = $uibModalInstance;
    this._ = _;
    this.Message = Message;
    this.miembro = null;
    this.equipo = this.data.equipo;
    this.cargos = angular.copy(this.data.cargos);
    this.miembro = this.data.miembro || {};
    this.modoEdicion = false;
    this.modoVista = this.data.modoVista;
  }

  $onInit() {
    this.miembro.tipo_equipo = 'TE_BENEFICIARIO';
    if (this.miembro.id_equipo_tecnico) {
      this.getDatosMiembroEquipo();
    } else {
    this.quitarCargosOcupados();
    this.modoEdicion = true;
    }
  }

  quitarCargosOcupados(cargoIgnorado) {
    this._.remove(this.cargos, (cargo) => {
      const CARGO_EXISTENTE = this._.find(this.equipo, {cargo: cargo.codigo, estado: 'ACTIVO'});
      if (CARGO_EXISTENTE && CARGO_EXISTENTE.cargo === cargoIgnorado) {
        return false;
      }
      return CARGO_EXISTENTE && CARGO_EXISTENTE.estado === 'ACTIVO';
    });
  }

  getDatosMiembroEquipo() {
    this.DataService.get(`proyectos/${this.miembro.fid_proyecto}/equipos_tecnicos/${this.miembro.id_equipo_tecnico}`).then( result => {
      this.miembro = result.datos;
      angular.extend(this.miembro, result.datos.usuario.persona);
      delete this.miembro.usuario;
      this.datosIniciales = angular.copy(this.miembro);
      this.quitarCargosOcupados(this.miembro.cargo);
      if (this.miembro.tipo_equipo !== 'TE_UPRE') {
        this.archivo = {
          url: `proyectos/${this.miembro.fid_proyecto}/doc/${this.miembro.doc_designacion.substring(1, this.miembro.doc_designacion.indexOf(']'))}`,
          urlDescarga: `proyectos/${this.miembro.fid_proyecto}/doc/${this.miembro.doc_designacion.substring(1, this.miembro.doc_designacion.indexOf(']'))}/descargar`,
        };
      }
    });
  }

  registrar() {
    if (!this.miembro.doc_designacion) {
      this.Message.warning('Debe seleccionar el archivo de designación.');
      return;
    }
    if (!this.data.registrar) {
      return this.$uibModalInstance.close(this.miembro);
    }
    let url = `proyectos/${this.$stateParams.codigo}/equipos_tecnicos`;
    let servicio = 'post';
    let miembroEquipo = angular.copy(this.miembro);
    if (this.miembro.id_equipo_tecnico) {
      url += `/${this.miembro.id_equipo_tecnico}`;
      servicio = 'put';
      if (typeof this.miembro.doc_designacion === 'string') {
        delete miembroEquipo.doc_designacion;
      }
    }
    this.DataService[servicio](url, miembroEquipo).then( result => {
      if (result) {
        this.Message.success(result.mensaje);
        this.$uibModalInstance.close(this.miembro);
      }
    });
  }

  esSupervisor() {
    return this.miembro.cargo === 'CG_SUPERVISOR';
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

}

export default RegistroFiscalSupervisorController;
