'use strict';

class ArchivoModalController {
    constructor($uibModalInstance, data, title, icon, DataService, Message, labelOk, apiUrl) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.data = data;
        this.title = title;
        this.icon = icon;
        this.DataService = DataService;
        this.Message = Message;
        this.labelOk = labelOk;
        this.apiUrl = apiUrl;
    }

    $onInit() {
      this.descargas = {
        manual: {
          ruta: `${this.apiUrl}items/csv/manuales?archivo=manual_ods`,
          nombre: 'Manual ods'
        },
        modelo: {
          ruta: `${this.apiUrl}items/csv/manuales?archivo=items&tipo=ods`,
          nombre: 'Modelo items'
        }
      }
    }

    registrar() {
      this.$uibModalInstance.close(this.archivo);
    }

    cerrar() {
      this.$uibModalInstance.dismiss('cancel');
    }
}

export default ArchivoModalController;
