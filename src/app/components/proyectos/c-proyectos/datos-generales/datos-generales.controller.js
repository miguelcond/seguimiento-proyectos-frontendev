'use strict';

class DatosGenerales {
  constructor(DataService, Util, Proyecto, Modal, _) {
    'ngInject';
    this.DataService = DataService;
    this.Util = Util;
    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this._ = _;
  }

  $onInit() {

  }

  verDocumento(tipo, nombreDocumento) {
    this.Modal.showDocumentBase64(`proyectos/${this.proyecto.id_proyecto}/documentos/${tipo}`, nombreDocumento);
  }

}

export default DatosGenerales;
