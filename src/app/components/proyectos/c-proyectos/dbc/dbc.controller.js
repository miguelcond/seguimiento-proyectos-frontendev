'use strict';

class DbcController {
  constructor(Util, Proyecto, Modal) {
    'ngInject';
    this.Util = Util;
    this.Proyecto = Proyecto;
    this.Modal = Modal
  }

  $onInit() {

  }

  verDocumento() {
    this.Modal.showDocumentBase64(`proyectos/${this.proyecto.id_proyecto}/doc/DBC`,
    `Documento Base de Contratación`);
  }

  estaRegistrado() {
    return typeof this.proyecto.doc_base_contratacion === 'string';
  }

}

export default DbcController;
