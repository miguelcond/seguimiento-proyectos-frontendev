'use strict';

import controller from './dbc.controller';
import template from './dbc.html';

const dbcComponent = {
  bindings: {
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default dbcComponent;
