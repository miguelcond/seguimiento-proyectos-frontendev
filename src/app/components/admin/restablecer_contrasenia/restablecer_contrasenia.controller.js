/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class LoginController {

    constructor($rootScope, $location, $scope, Storage, Message, timeSessionExpired, Auth, authUrl, authEmpresasUrl, DataService, apiUrlPublic, $stateParams, $log) {
        'ngInject';

        this.$rootScope = $rootScope;
        
        this.$scope = $scope;
        this.Storage = Storage;
        this.Message = Message;
        this.Auth = Auth;
        this.authEmpresasUrl = authEmpresasUrl;
        this.apiUrlPublic = apiUrlPublic;
        this.timeSessionExpired = timeSessionExpired;
        this.DataService = DataService;
                    
        this.$rootScope.auth = false;

        this.$location = $location;
        this.$stateParams = $stateParams;
        this.authUrl = authUrl;
        this.$log = $log;
    }

    $onInit() {
        this.$rootScope.auth = false;
        this.codigo = this.$stateParams.codigo == "" ? null : this.$stateParams.codigo;
        this.pathLogin = 'login';

        console.log('------------------------------------');
        console.log('reached restablecer');
        console.log('------------------------------------');
        console.log(this.codigo);
        console.log('------------------------------------');

        if (!this.codigo) {
          console.log('------------------------------------');
          console.log('no se ha especificado un codigo');
          console.log('------------------------------------');
          this.$location.path(this.pathLogin);
        }

        this.verificarCodigo();

        /*
        if (this.Storage.exist('expired')) {
            this.Message.warning('Su sesión ha sido cerrada automáticamente después de ' + this.timeSessionExpired + ' minutos de inactividad.', null, 0);
            this.Storage.destroy('expired');
        }
        if (this.Storage.exist('menu') && this.Storage.existUser() && this.Storage.exist('token') && this.Storage.getUser().estado == 'ACTIVO') {
            this.$rootScope.auth = true;
            this.$location.path(this.Storage.getUser().pathInicio);
        }
        this.$scope.$watch('$ctrl.password', () => {
          if (this.password) {
            let el = document.getElementById('password');
            if (el) {
              angular.element(el).find('input').removeClass('not-required');
            }
          }
        }, true);
        this.$scope.$watch('$ctrl.username', () => {
          if (this.username) {
            let el = document.getElementById('username');
            if (el) {
              angular.element(el).find('input').removeClass('not-required');
            }
          }
        }, true);
        */
    }

    verificarCodigo() {
      let datos = {
        codigo: this.codigo
      }

      this.DataService.post(`${this.authUrl}/verificarcodigo`, datos).then((resp) => {
        console.log('---------resp verificacion----------');
        console.log(resp);
        console.log('------------------------------------');
        if (resp && resp.finalizado) {
          console.log('------------------------------------');
          console.log('codigo correcto');
          console.log('------------------------------------');
        } else {
          console.log('------------------------------------');
          console.log('el codigo no esta en DB');
          console.log('------------------------------------');
          this.$location.path(this.pathLogin);
        }
      }).catch(error => {
        this.$log.log(error);
        this.$location.path(this.pathLogin);
      });
    }

    inicioDeSesion() {
      this.$location.path(this.pathLogin);
    }

    cambiarContrasena() {
      console.log('--------------------nueva-----------');
      console.log(this.nueva);
      console.log('--------confirmado------------------');
      console.log(this.confirmada);
      console.log('------------------------------------');
      let datos = {
        codigo: this.codigo,
        nueva: this.nueva,
        confirmada: this.confirmada
      }
      this.DataService.post(`${this.authUrl}/cambiarcontrasenia`, datos).then((resp) => {
        if (resp && resp.finalizado) {
          this.Message.success(resp.mensaje, null, 0);
          this.$location.path(this.pathLogin);
        }
      });
    }

    /*
    signin() {
        this.Auth.signin({
            username: this.username,
            password: this.password,
            // path: 'pendientes',
            pathLogin: 'login',
            menu: true,
            sidenav: true
        });
    }

    signinEmpresa() {
      this.Auth.signin({
        nit: this.nit,
        usuario: this.username,
        clave: this.password,
        path: 'matriculas',
        pathLogin: 'login',
        menu: true,
        sidenav: true
      }, {url: this.authEmpresasUrl});
    }

    recovery() {
        this.flip = true;
        this.username = '';
        this.password = '';
    }

    goLogin() {
        this.flip = false;
        this.correo = '';
    }

    iniciarSesion() {
      if (!this.empresa) {
        this.signin();
      } else {
        this.signinEmpresa();
      }
    }

    recuperarContrasenia() {
      if (!this.correo) {
        this.Message.warning('Debe escribir su dirección de correo electrónico', null, 0);
      } else {
        let datos = {
          email: this.correo
        }
        this.DataService.post(`${this.authUrl}/restablecercontrasenia`, datos).then((resp) => {
          if (resp && resp.finalizado) {
            this.Message.success(resp.mensaje, null, 0);
          }
        });
      }
    }
    */
}

export default LoginController;
