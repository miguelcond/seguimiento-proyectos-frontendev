'use strict';

import RestablecerContraseniaComponent from './restablecer_contrasenia.component';
import RestablecerContraseniaService from './restablecer_contrasenia.service';
import './restablecer_contrasenia.css';

const RestablecerContrasenia = angular
    .module('app.restablecerContrasenia', [])
    .service('RestablecerContrasenia', RestablecerContraseniaService)
    .component('appAdminRestablecerContrasenia', RestablecerContraseniaComponent)
    .config(($stateProvider) => {
        $stateProvider
            .state('restablecerContrasenia', {
                url: '/restablecer-contrasenia/:codigo',
                component: 'appAdminRestablecerContrasenia'
            })
    })
    .name;

export default RestablecerContrasenia;
