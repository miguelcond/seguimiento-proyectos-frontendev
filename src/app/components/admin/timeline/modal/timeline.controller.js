'use strict';

class ReasignarUsuarioController {
  constructor($uibModalInstance, data, DataService, Message) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.DataService = DataService;
    this.Message = Message;
    this.proyecto = data;
  }

  $onInit() {
    this.eventos = [];

    this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/logs`).then((resp)=>{
      if (resp && resp.finalizado) {
        for (let i in resp.datos) {
          let persona = resp.datos[i].usuario.persona;
          this.eventos.push({
            side: 'right',
            badgeClass: 'warning',
            badgeIconClass: 'info-circle',
            estado: resp.datos[i].estado.nombre,
            fecha: resp.datos[i]._fecha_creacion,
          });
          this.eventos.push({
            side: 'left',
            badgeClass: 'info',
            badgeIconClass: resp.datos[i].observacion? 'eye': resp.datos[i].objeto_tipo==='proyecto'? 'check-circle': 'edit',
            nombre: `${persona.nombres||''} ${persona.primer_apellido||''} ${persona.segundo_apellido||''}`,
            rol: resp.datos[i].usuario_rol.descripcion,
            contenido: resp.datos[i].observacion,
            fecha: resp.datos[i]._fecha_creacion,
          });
        }
        this.Message.success(resp.mensaje);
      }
    });
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

}

export default ReasignarUsuarioController;
