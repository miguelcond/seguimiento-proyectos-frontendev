'use strict';

class UsuariosService {
  constructor(Storage) {
    'ngInject';
    this.Storage = Storage;
    this.usuario = this.Storage.getUser(); // Solo ejecuta la captura de usuario una vez
  }

  getRolesResponsableGAM() {
    return ['RESP_GAM_GAIOC'];
  }

  getRolesSupervisor() {
    return ['SUPERVISOR_FDI'];
  }

  getRolesResponsable() {
    return ['TECNICO_FDI','RESP_GAM_GAIOC','MAE_FDI','JEFE_REVISION_FDI'];
  }

  getRolesTecnico() {
    return ['TECNICO_FDI'];
  }

  getRolesFiscal() {
    return ['FISCAL_FDI'];
  }

  getRolesJefeRevision() {
    return ['JEFE_REVISION_FDI'];
  }

  getRolesRecepcion() {
    return ['RESP_RECEPCION_FDI'];
  }

  getRolesFinanciero() {
    return ['FINANCIERO_FDI'];
  }

  esTecnicoResponsable() {
    this.obtenerUsuario();
    return this.usuario && this.usuario.rol && this.getRolesTecnico().indexOf(this.usuario.rol.nombre) !== -1;
  }

  esResponsableProyecto() {
    this.obtenerUsuario();
    return this.usuario && this.usuario.rol && this.getRolesResponsable().indexOf(this.usuario.rol.nombre) !== -1;
  }

  esResponsableGAM() {
    this.obtenerUsuario();
    return this.usuario && this.usuario.rol && this.getRolesResponsableGAM().indexOf(this.usuario.rol.nombre) !== -1;
  }

  esSupervisor() {
    this.obtenerUsuario();
    return this.usuario && this.usuario.rol && this.getRolesSupervisor().indexOf(this.usuario.rol.nombre) !== -1;
  }

  esFiscal() {
    this.obtenerUsuario();
    return this.usuario && this.usuario.rol && this.getRolesFiscal().indexOf(this.usuario.rol.nombre) !== -1;
  }

  esJefeRevision() {
    this.obtenerUsuario();
    return this.usuario && this.usuario.rol && this.getRolesJefeRevision().indexOf(this.usuario.rol.nombre) !== -1;
  }

  esRecepcion() {
    this.obtenerUsuario();
    return this.usuario && this.usuario.rol && this.getRolesRecepcion().indexOf(this.usuario.rol.nombre) !== -1;
  }

  esFinanciero() {
    this.obtenerUsuario();
    return this.usuario && this.usuario.rol && this.getRolesFinanciero().indexOf(this.usuario.rol.nombre) !== -1;
  }

  obtenerUsuario () {
    this.usuario = this.Storage.getUser();
  }
}

export default UsuariosService;
