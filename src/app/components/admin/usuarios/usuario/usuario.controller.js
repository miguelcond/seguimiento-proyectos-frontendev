'use strict';

import modalController from '../reenviar-confirmacion/reconfirmacion.modal.controller';
import modalTemplate from '../reenviar-confirmacion/reconfirmacion.modal.html';


class UsuarioController {
  constructor(DataService, Storage, Modal, $state, $stateParams, DataTable, Proyecto, $log, Util, Message) {
    'ngInject';
    this.DataService = DataService;
    this.Storage = Storage;
    this.Modal = Modal;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.DataTable = DataTable;
    this.Proyecto = Proyecto;
    this.$log = $log;
    this.Util = Util;
    this.Message = Message;
  }

  $onInit() {
    this.idUsuario = this.$stateParams.idUsuario==""? null: this.$stateParams.idUsuario;
    // this.$log.log(this);

    this.usuario = {
      estado: 'ACTIVO',
      // tipo_departamento: 'UNICO',
      persona: {
        // tipo_documento: 'TD_CI',
      },
      roles: []
    };
    this.estados = [
      { value: 'ACTIVO', text: 'ACTIVO' },
      { value: 'INACTIVO', text: 'INACTIVO' },
    ];

    // this.tipos_departamento = [
    //   { value: 'UNICO', text: 'Único' },
    //   { value: 'REG_OCC', text: 'Regional occidental' },
    //   { value: 'REG_CEN', text: 'Regional central' },
    //   { value: 'REG_ORI', text: 'Regional oriental' },
    //   { value: 'TODOS', text: 'Todos' },
    // ];

    this.departamentos = [];

    this.roles = [];
    this.getUsuario();
  }

  getUsuario() {
    this.DataService.get(`departamentos`)
    .then( response => {
      this.departamentos = response;
      this.departamentos_obj = {};
      this.departamentos.forEach( dep => {
        if (!this.departamentos_obj[dep.codigo]) {
          this.departamentos_obj[dep.codigo] = dep;
        }
      });
      return this.DataService.get(`rol`);
    })
    .then( response => {
      this.roles = response.datos.rows;
      this.roles_obj = {};
      this.roles.forEach( rol => {
        if (!this.roles_obj[rol.id_rol]) {
          this.roles_obj[rol.id_rol] = rol;
        }
      });
      if (this.idUsuario) {
        return this.DataService.get(`usuario/${this.idUsuario}`)
      }
    })
    .then( response => {
      if (response) {
        this.usuario = response.datos;
        // angular.extend(this.usuario, response.datos);
        this.usuario.usuarios_roles.forEach( item => {
          this.roles_obj[item.fid_rol].valor = true;
        })
        this.usuario.fcod_departamento.forEach( item => {
          this.departamentos_obj[item].valor = true;
        })
      }
    });
  }

  guardarDatos(form) {
    // this.$log.log(form);
    if (form.$valid) {
      this.usuario.roles = [];
      this.roles.forEach( rol => {
        if (rol.valor) {
          this.usuario.roles.push(rol.id_rol);
        }
      })

      this.usuario.fcod_departamento = [];
      this.departamentos.forEach( dep => {
        if (dep.valor) {
          this.usuario.fcod_departamento.push(dep.codigo);
        }
      });

      if (this.idUsuario) {
        this.usuario.id = this.idUsuario;
      }
      // this.$log.log('form valido', angular.copy(this.usuario));
      if (this.usuario.persona && this.usuario.persona.numero_documento && this.usuario.persona.numero_documento.indexOf('-')>=0)
      { this.usuario.persona.numero_documento = this.usuario.persona.numero_documento.split('-')[0]; }
      this.DataService.save(`usuario`, this.usuario)
      .then( response => {
        if (response) {
          this.Message.warning(response.mensaje);
          if (!this.idUsuario) {
            this.$state.go('usuarios');
          }
          // this.$log.log('exacto', response);
        }
      })

    } else {
      this.Util.setTouchedForm(form);
    }
  }

  reenviarContrasenia() {
    const USUARIO = angular.copy(this.usuario);
    const REENVIO = this.Modal.show({
      template: modalTemplate,
      controller: modalController,
      data: {
        usuario: this.usuario,
      }
    });
    REENVIO.result.then((correoReenviado) => {
      if (!correoReenviado) {
        this.usuario = USUARIO;
      }
    }).catch(() => {});
  }
}

export default UsuarioController;
