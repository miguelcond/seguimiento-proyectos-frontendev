'usr strict';

import controller from './usuario.controller';
import template from './usuario.html';

const UsuarioComponent = {
  bindings: {},
  controller,
  template
};

export default UsuarioComponent;
