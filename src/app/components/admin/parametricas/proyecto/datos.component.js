'usr strict';

import controller from './datos.controller';
import template from './datos.html';

const ProyectoComponent = {
  bindings: {},
  controller,
  template
};

export default ProyectoComponent;
