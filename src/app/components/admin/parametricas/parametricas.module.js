'use strict';

import adminParametricasComponent from './parametricas.component';

const adminproyectos = angular
    .module('app.parametricas', [])
    .component('adminParametricas', adminParametricasComponent)
    .config(($stateProvider) => {
      $stateProvider
          .state('admin-parametricas', {
            url: '/admin-parametricas',
            component: 'adminParametricas'
          });
    })
    .name;

export default adminproyectos;
