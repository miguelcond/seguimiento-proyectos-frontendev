'use strict';

class OrgInstitucionalesController {
  constructor($state, DataService, Modal, Message) {
    'ngInject';

    this.$state = $state;
    this.DataService = DataService;
    this.Modal = Modal;
    this.Message = Message;
  }

  $onInit() {
    this.$$obtenerorg_instituciones();
    this.obtenerConfigParams();
  }

  $$obtenerorg_instituciones() {
    this.DataService.get(`parametricas/ORG_INSTITUCION`).then((resp) => {
      if (resp && resp.finalizado) {
        this.org_instituciones = [];
        for (let i in resp.datos.rows) {
          resp.datos.rows[i].codigo_ant = resp.datos.rows[i].codigo;
          resp.datos.rows[i].nombre_ant = resp.datos.rows[i].nombre;
          resp.datos.rows[i].descripcion_ant = resp.datos.rows[i].descripcion;
          this.org_instituciones.push(resp.datos.rows[i]);
        }
      }
    });
  }

  obtenerConfigParams() {
    this.DataService.get(`parametricas/CONFIG`).then((resp) => {
      if (resp && resp.finalizado) {
        this.configParams = [];
        for (let i in resp.datos.rows) {
          resp.datos.rows[i].codigo_ant = resp.datos.rows[i].codigo;
          resp.datos.rows[i].nombre_ant = resp.datos.rows[i].nombre;
          resp.datos.rows[i].descripcion_ant = resp.datos.rows[i].descripcion;
          this.configParams.push(resp.datos.rows[i]);
        }
      }
    });
  }

  obtenerIncompletos() {
    return this.org_instituciones.find((institucion) => {
      return !institucion.codigo || !institucion.nombre || !institucion.descripcion
    });
  }

  obtenerModificados() {
    return this.org_instituciones.find((institucion) => {
      return institucion.codigo != institucion.codigo_ant || institucion.nombre != institucion.nombre_ant || institucion.descripcion != institucion.descripcion_ant
    })
  }

  nuevo() {
    if (!this.obtenerIncompletos() && !this.obtenerModificados()) {
      this.org_instituciones.push({});
    } else {
      this.Message.warning('Debe guardar los cambios antes de agregar un nuevo parametro.');
    }
  }

  actualizar(param, type) {
    if (param.codigo && param.nombre && param.descripcion &&
        (param.codigo != param.codigo_ant || param.nombre != param.nombre_ant || param.descripcion != param.descripcion_ant)) {
      if (!param.codigo_ant) {
        this.Modal.confirm(`¿Está seguro/a de agregar el nuevo código <b>${param.codigo}</b> del parametro? No podra modificar el código mas adelante.`, () => {
          this.DataService.post(`parametricas/${type}`, param).then((resp) => {
            if (resp && resp.finalizado) {
              param.codigo_ant = param.codigo;
              param.nombre_ant = param.nombre;
              param.descripcion_ant = param.descripcion;
            }
          });
        }, null, null, 'Si estoy seguro/a', '');
      } else {
        this.DataService.put(`parametricas/${type}`, param).then((resp) => {
          if (resp && resp.finalizado) {
            param.nombre_ant = param.nombre;
            param.descripcion_ant = param.descripcion;
          }
        });
      }
    }
  }
}

export default OrgInstitucionalesController;
