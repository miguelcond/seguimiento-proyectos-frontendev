'use strict';

class ReasignarUsuarioController {
  constructor($uibModalInstance, data, DataService, Message) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.transicion = data.transicion;
    this.DataService = DataService;
    this.Message = Message;
  }

  $onInit() {
    this.datos = {
      nro_usuario: this.transicion.usuario_asignado.nro,
      nro_rol: this.transicion.usuario_asignado_rol.nro,
    };
    let roles = [];
    roles.push(this.transicion.usuario_asignado_rol.nro);
    this.DataService.get(`proyectos/${this.transicion.objeto}/usuarios?roles=[${roles.join(',')}]`)
    .then(response => {
      if (response) {
        this.usuariosParaAsignar = response.datos
      }
    });
  }

  reasignarUsuario() {
    let datos = angular.copy(this.datos);
    if(datos.seleccionado) {
      datos.nuevo_nro_rol = datos.seleccionado.id_rol || null;
      datos.nuevo_nro_usuario = datos.seleccionado.id_usuario || null;
      delete datos.seleccionado;
    }
    this.DataService.put(`adminproyectos/${this.transicion.objeto}/usuarios`,datos).then((resp)=>{
      if (resp && resp.finalizado) {
        this.Message.success(resp.mensaje);
        this.$uibModalInstance.close(true);
      }
    });
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

}

export default ReasignarUsuarioController;
