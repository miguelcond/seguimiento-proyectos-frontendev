'use strict';

import UserComponent from './user.component';

const User = angular
    .module('app.user', [])
    .component('appAdminUser', UserComponent)
    .config(($stateProvider) => {
        $stateProvider
            .state('user', {
                url: '/user',
                component: 'appAdminUser'
            });
    })
    .name;

export default User;
