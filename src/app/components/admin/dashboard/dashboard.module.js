'use strict';

import DashboardComponent from './dashboard.component';

const Dashboard = angular
    .module('app.admin.dashboard', [])
    .component('appDashboard', DashboardComponent)
    .config(($stateProvider) => {
        $stateProvider
            .state('dashboard', {
                url: '/',
                component: 'appDashboard'
            });
    })
    .name;

export default Dashboard;
