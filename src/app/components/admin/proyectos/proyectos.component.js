'usr strict';

import controller from './proyectos.controller';
import template from './proyectos.html';

const ProyectosComponent = {
  bindings: {},
  controller,
  template
};

export default ProyectosComponent;
