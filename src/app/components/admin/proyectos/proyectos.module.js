'use strict';

import adminProyectosComponent from './proyectos.component';
import adminProyectoComponent from './proyecto/datos.component';
// import reconfirmacionUsuarioComponent from './reenviar-confirmacion/reconfirmacion.component';
// import UsuariosService from './adminproyectos.service';

const adminproyectos = angular
    .module('app.adminproyectos', [])
    .component('adminproyectos', adminProyectosComponent)
    .component('adminproyecto', adminProyectoComponent)
    .config(($stateProvider) => {
      $stateProvider
          .state('adminproyectos', {
            url: '/admin-proyectos',
            component: 'adminproyectos'
          })
          .state('adminproyecto', {
            url: '/admin-proyecto/{codigo:int}',
            component: 'adminproyecto'
          });
    })
    .name;

export default adminproyectos;
