'usr strict';

import controller from './techos_presupuestarios.controller';
import template from './techos_presupuestarios.html';

const TechosPresupuestariosComponent = {
  bindings: {},
  controller,
  template
};

export default TechosPresupuestariosComponent;
