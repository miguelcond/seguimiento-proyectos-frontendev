/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class TechoPresupuestarioController {
  constructor($scope, DataService, Storage, Modal, $state, $stateParams, DataTable, Proyecto, $log, Util, Message) {
    'ngInject';
    this.$scope = $scope;
    this.DataService = DataService;
    this.Storage = Storage;
    this.Modal = Modal;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.DataTable = DataTable;
    this.Proyecto = Proyecto;
    this.$log = $log;
    this.Util = Util;
    this.Message = Message;
  }

  $onInit() {
    if (!this.$stateParams.gestion) {
      this.$state.go(`admin-techos-presupuestarios`);
    }
    this.codigo = this.$stateParams.codigo == "" ? null : this.$stateParams.codigo;
    // this.$log.log(this);

    this.techo_presupuestario = {
      estado: 'ACTIVO',
    };
    this.estados = [
      { value: 'ACTIVO', text: 'ACTIVO' },
      { value: 'INACTIVO', text: 'INACTIVO' },
    ];

    this.getTechoPresupuestario();

    this.$scope.$watch('$ctrl.techo_presupuestario.techo_presupuestal', () => {
      this.techo_presupuestario.monto_disponible = this.techo_presupuestario.techo_presupuestal - this.techo_presupuestario.monto_usado;
      if (this.techo_presupuestario.monto_disponible < 0) {
        this.techo_presupuestario.monto_disponible = 0;
      }
    });
  }

  getTechoPresupuestario() {
    this.DataService.get(`techo_presupuestario/${this.codigo}?gestion=${this.$stateParams.gestion}`)
    .then(response => {
      if (response) {
        this.techo_presupuestario = response.datos;
      }
    });
  }

  guardarDatos(form) {
    // this.$log.log(form);
    if (form.$valid) {
      if (this.codigo) {
        this.techo_presupuestario.codigo = this.codigo;
      }
      // this.$log.log('form valido', angular.copy(this.usuario));
      // this.DataService.save(`techo_presupuestario`, this.techo_presupuestario)
      // .then( response => {
      //   if (response) {
      //     this.Message.warning(response.mensaje);
      //     if (!this.codigo) {
      //       this.$state.go('admin-techos-presupuestarios');
      //     }
      //     // this.$log.log('exacto', response);
      //   }
      // });
      this.DataService.put(`techo_presupuestario/${this.codigo}?gestion=${this.$stateParams.gestion}`, this.techo_presupuestario)
      .then((resp) => {
        if (resp && resp.finalizado) {
          this.Message.success(resp.mensaje);
        } else {
          this.Message.warning(resp.mensaje);
        }
      });
    } else {
      this.Util.setTouchedForm(form);
    }
  }
}

export default TechoPresupuestarioController;
