'use strict';

import modalConvenioController from './modal/modal.controller';
import modalConvenioTemplate from './modal/modal.html';

class CodigoConvenioController {
  constructor(Modal) {
    'ngInject';
    this.Modal = Modal;
  }

  $onInit() {
    
  }

  getIcon(){
    return this.proyecto.estado == 'INACTIVO'?'fa-eye':'fa-eye-slash';
  }

  getTooltip(){
    return this.proyecto.estado == 'INACTIVO'?'ACTIVAR':'INACTIVAR';
  }
  verRegistro() {
    this.Modal.show({
      template: modalConvenioTemplate,
      controller: modalConvenioController,
      data: this.proyecto,
      size: 'lg'
    });
  }

}

export default CodigoConvenioController;
