'use strict';

import controller from './button.controller';
import template from './button.html';

const ButtonEstadoComponent = {
  bindings:{
    proyecto: '<'
  },
  controller,
  template
};

export default ButtonEstadoComponent;
