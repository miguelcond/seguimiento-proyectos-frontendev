/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class EstadoEditarController {
  constructor($scope, DataService, Storage, Modal, $state, $stateParams, DataTable, Proyecto, $log, Util, Message) {
    'ngInject';
    this.$scope = $scope;
    this.DataService = DataService;
    this.Storage = Storage;
    this.Modal = Modal;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.DataTable = DataTable;
    this.Proyecto = Proyecto;
    this.$log = $log;
    this.Util = Util;
    this.Message = Message;
  }

  $onInit() {
    this.codigo = this.$stateParams.codigo == "" ? null : this.$stateParams.codigo;
    // this.$log.log(this);

    this.jsoneditorOptions = {
      "mode": 'code'
    };

    this.getEstado();

    // this.$scope.$watch('$ctrl.estado.areas', () => {
    //   if (!this.estado.areas || this.estado.areas !== '') {
    //     this.$log.log('El campo esta vacío!!!');
    //   }
    // });
  }

  getEstado() {
    this.DataService.get(`estados/editar/${this.codigo}`)
    .then(response => {
      if (response) {
        this.estado = response.datos;
      }
    });
  }

  guardarDatos() {
    if (this.estado) {
      if (!this.estado.nombre || this.estado.nombre === '') {
        this.Message.warning('Debe especificar un nombre');
      }

      // if (typeof this.estado.areas !== 'object' && typeof this.estado.areas !== 'array') {
      //   this.Message.warning('El campo ´areas debe ser un objeto o un array');
      // }

      // this.DataService.save(`estados`, this.estado)
      // .then( response => {
      //   if (response) {
      //     this.Message.warning(response.mensaje);
      //     if (!this.codigo) {
      //       this.$state.go('admin-estados');
      //     }
      //   }
      // });

      this.DataService.put(`estados/${this.codigo}`, this.estado)
      .then((resp) => {
        if (resp && resp.finalizado) {
          this.Message.success(resp.mensaje);
        } else {
          this.Message.warning(resp.mensaje);
        }
      });
    } else {
      this.Message.warning('No existen datos pendientes para guardar');
    }
  }

  // alertarError() {
  //   this.$log.log('json invalido');
  // }
}

export default EstadoEditarController;
