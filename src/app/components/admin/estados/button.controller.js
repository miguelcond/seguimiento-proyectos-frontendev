'use strict';

class EstadoController {
  constructor(Modal,DataService,Message) {
    'ngInject';
    this.Modal = Modal;
    this.DataService = DataService;
    this.Message = Message;
  }

  $onInit() {

  }

  getIcon(){
    return this.proyecto.estado == 'INACTIVO'?'fa-eye':'fa-eye-slash';
  }

  getTooltip(){
    return this.proyecto.estado == 'INACTIVO'?'ACTIVAR':'INACTIVAR';
  }
  puedeCambiarEstado(){
    return ["RECHAZADO_FDI"].indexOf(this.proyecto.estado_proyecto)!==-1;
  }

  cambiarEstado() {
    let nuevoEstado = this.proyecto.estado == 'INACTIVO'?'ACTIVO':'INACTIVO';
    this.Modal.confirm('¿Esta seguro/a de modificar el estado? No estara disponible para los demás usuarios.', ()=>{
      let datos = {
        id_proyecto: this.proyecto.id_proyecto,
        estado: `${nuevoEstado}`
      }
      this.DataService.put(`proyectos/${this.proyecto.id_proyecto}`, datos).then((resp)=>{
        if (resp && resp.finalizado) {
          this.proyecto.estado = nuevoEstado;
          this.Message.success(resp.mensaje);                    
          this.$state.reload();
        }
      });
    }, null, 'Modificar datos', 'Si, estoy seguro/a');
  }

}

export default EstadoController;
