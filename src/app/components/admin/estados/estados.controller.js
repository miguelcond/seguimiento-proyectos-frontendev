/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class EstadosController {
  constructor($state, DataService, Modal, Message) {
    'ngInject';

    this.$state = $state;
    this.DataService = DataService;
    this.Modal = Modal;
    this.Message = Message;
  }

  $onInit() {
    this.cargarTabla();
  }

  cargarTabla() {
    var colors = {
      INACTIVO: 'warning',
      ACTIVO: 'success',
      ELIMINADO: 'danger'
    };
    this.estados = {
      url: `estados`,
      // queryExtra: { fid_proyecto: this.idProyecto, origen: 'bandeja' },
      data: {
        formly: [
          {
            key: 'codigo',
            type: 'input',
            templateOptions: {
              label: 'Código',
              type: 'text'
            }
          },
          {
            key: 'codigo_proceso',
            type: 'input',
            templateOptions: {
              label: 'Código de proceso',
              type: 'text'
            }
          },
          {
            key: 'nombre',
            type: 'input',
            templateOptions: {
              label: 'Estado',
              type: 'text'
            }
          },
          {
            key: 'tipo',
            type: 'input',
            templateOptions: {
              label: 'Tipo',
              type: 'text'
            }
          },
          {
            key: 'estado',
            type: 'select',
            templateOptions: {
              label: 'Estado',
              type: 'select',
              options: [
                { value: 'ACTIVO', name: 'ACTIVO' },
                { value: 'INACTIVO', name: 'INACTIVO' },
                { value: 'ELIMINADO', name: 'ELIMINADO' }
              ]
            }
          }
        ]
      },
      fields: ['codigo', 'codigo_proceso', 'nombre', 'tipo', 'estado'],
      permission: {
        create: false,
        update: true,
        delete: false,
        filter: false
      },
      labels: { update: 'Recargar tabla' },
      eventEdit: (estado) => { this.$state.go('estados/editar',{ codigo: estado.codigo }); },
      clases: {
        'estado': item => {
          return ['badge', `badge-${colors[item.estado]}`];
        }
      }
    };
  }

  // reestablecerValores() {
  //   this.Modal.confirm(`¿Está seguro/a de reestablecer los valores de los montos usados en cada municipio? Este cambio no podrá ser revertido.`, () => {
  //     this.DataService.put(`techo_presupuestario/reestablecer`, {reestablecer: true}).then((resp) => {
  //       if (resp && resp.finalizado) {
  //         this.Message.success('Se han reestablecido los valores por defecto correctamente');
  //         this.$state.reload();
  //       }
  //     });
  //   }, null, null, 'Si estoy seguro/a', '');
  // }
}

export default EstadosController;
