'use strict';

import ProyectoComponent from './proyecto.component';

const Proyecto = angular
    .module('app.rproyecto', [])
    .component('regProyecto', ProyectoComponent)
    .config(($stateProvider) => {
        $stateProvider
            .state('reg-proyecto', {
                url: '/reg-proyecto/{codigo: int}',
                component: 'regProyecto',
                params: {
                  urlProcesado: ''
                }
            });
    })
    .name;

export default Proyecto;
