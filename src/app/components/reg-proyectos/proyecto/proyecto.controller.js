/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class ProyectoController {
  constructor($stateParams, Storage, Proyecto, $state, $q, Message, _, Modal, DataService) {
    'ngInject';
    this.$stateParams = $stateParams;
    this.Proyecto = Proyecto;
    this.Storage = Storage;
    this.$q = $q;
    this.$state = $state;
    this.Message = Message;
    this._ = _;
    this.Modal = Modal;
    this.DataService = DataService;
  }

  $onInit() {
    this.idProyecto = this.$stateParams.codigo;
    this.Proyecto.setEstadoActual(null);
    this.getProyecto();
    this.url = `proyectos/${this.idProyecto}?rol=${this.Storage.get('rol').id_rol}`;
  }

  getProyecto() {
    this.Proyecto.get(this.idProyecto, this.$stateParams.urlProcesado).then(response => {
      if (response) {
        this.Proyecto.setEstadoActual(response.datos.estado_actual);
        this.proyecto = response.datos;
        this.estadoActual = response.datos.estado_actual;
      } else {
        this.$state.go('pendientes');
      }
    });
  }

  verificarSegunEstado() {
    let tieneError = false;
    let defer = this.$q.defer();

    if (this.Proyecto.esEditable('beneficiarios') && !this.sonDatosCompletosProductivo()) {
      tieneError = true;
    }
    if (!tieneError && this.Proyecto.esEditable('autoridad_beneficiaria') && !this.sonDatosCompletosAutoridad()) {
      tieneError = true;
    }
    if (!tieneError && this.Proyecto.esEditable('modulos') && (!this.sonDatosCompletosModulos() || !this.montosItemValidos())) {
      tieneError = true;
    }
    if (!tieneError && this.Proyecto.esEditable('financiamiento') && !this.montoFinanciamientoValido()) {
      tieneError = true;
    }
    if (!tieneError && this.Proyecto.esEditable('evaluacion')) {
      if (!tieneError && this.Proyecto.tratarComoProductivo(this.proyecto) && !this.sonDatosCompletosEvaluacion()) {
        tieneError = true;
      }
      if (!tieneError && !this.Proyecto.tratarComoProductivo(this.proyecto) && !this.tieneEvaluacionExterna()) {
        tieneError = true;
      }
      if (!tieneError && !this.sonDatosCompletosInforme()) {
        tieneError = true;
      }
    }
    if (!tieneError && this.Proyecto.esEditable('equipo_tecnico') && !this.tieneFiscalEquipoTecnico()) {
      // if (!this.tieneEquipoTecnico()) {
      // this.Modal.alert(`Debe registrar al Supervisor y Fiscal del proyecto, en el apartado Equipo Técnico.`, null, 'Información incompleta', null, 'md');
      tieneError = true;
    }

    if (tieneError) {
      defer.reject();
    } else {
      // Ejecutar acciones adicionales
      // if (this.proyecto.hasOwnProperty('montoFinanciamientoExcedido') && this.proyecto.montoFinanciamientoExcedido === false) {
      //   this.actualizarTechoDisponible();
      // }
      defer.resolve({confirmar: true});
    }

    return defer.promise;
  }

  tieneEquipoTecnico() {
    const TIENE_SUPERVISOR = this._.find(this.proyecto.equipo_tecnico, {cargo: 'CG_SUPERVISOR'});
    const TIENE_FISCAL = this._.find(this.proyecto.equipo_tecnico, {cargo: 'CG_FISCAL'});
    // return TIENE_SUPERVISOR && TIENE_FISCAL ? true : false;
    let mensaje = [];
    if (!TIENE_SUPERVISOR || !TIENE_FISCAL) {
      mensaje.push({area: 'Módulos/Equipo técnico', mensaje: 'Debe registrar al Supervisor y Fiscal del proyecto, en el apartado Equipo Técnico.'});
    }
    if (mensaje.length > 0) {
      let titulo = 'Equipo técnico';
      this.mostrarErroresModal(mensaje, titulo);
    } else {
      return true;
    }
  }

  tieneFiscalEquipoTecnico() {
    const TIENE_FISCAL = this._.find(this.proyecto.equipo_tecnico, {cargo: 'CG_FISCAL'});
    let mensaje = [];
    if (!TIENE_FISCAL) {
      mensaje.push({area: 'Módulos/Equipo técnico', mensaje: 'Debe registrar al Fiscal del proyecto, en el apartado Equipo Técnico.'});
    }
    if (mensaje.length > 0) {
      let titulo = 'Registrar Fiscal';
      this.mostrarErroresModal(mensaje, titulo);
    } else {
      return true;
    }
  }

  sonDatosCompletosAutoridad() {
    let mensaje = [];
    if (!this.proyecto.autoridad_beneficiaria || !this.proyecto.autoridad_beneficiaria.nombres || !this.proyecto.autoridad_beneficiaria.telefono || !this.proyecto.cargo_autoridad_beneficiaria) {
      mensaje.push({area: 'Contactos', mensaje: 'Datos de los contactos responsables.'});
    }
    if (mensaje.length > 0) {
      let titulo = 'Contactos';
      // let texto = '<p>Debe completar la información de los siguientes apartados:</p><ul>';
      // mensaje.map(obs => {
      //   texto += `<li><strong>${obs.area}</strong>: ${obs.mensaje}</li>`;
      // });
      // texto +='</ul>';
      // this.Modal.alert(texto, () => { return false; }, titulo, null, 'md');
      this.mostrarErroresModal(mensaje, titulo);
    } else {
      return true;
    }
  }

  mostrarErroresModal(mensaje, titulo, texto = null) {
    let text = texto ? texto : '<p>Debe completar la información de los siguientes apartados:</p><ul>';
    mensaje.map(obs => {
      text += `<li><strong>${obs.area}</strong>: ${obs.mensaje}</li>`;
    });
    text +='</ul>';
    this.Modal.alert(text, () => { return false; }, titulo, null, 'md');
  }

  sonDatosCompletosModulos() {
    let mensaje = [];
    if (!this.modulos || this.modulos.length < 1) {
      mensaje.push({area: 'Módulos/Items', mensaje: 'Registre al menos un módulo'});
    } else {
      const INCOMPLETOS = this.getModulosIncompletos();
      if (INCOMPLETOS > 0) {
        mensaje.push({area: 'Módulos/Items', mensaje: INCOMPLETOS > 1 ? `Algunos módulos no tienen items registrados, o no fueron guardados.` : `El módulo no tiene items registrados, o no fue guardado.`});
      }
    }
    // if (!this.proyecto.doc_especificaciones_tecnicas) {
    //   mensaje.push({area: 'Módulos/Items', mensaje: 'Registre el documento de especificaciones técnicas'});
    // }
    if (this.Proyecto.esEditable('doc_base_contratacion') && !this.proyecto.doc_base_contratacion) {
      mensaje.push({area: 'DBC', mensaje: 'Registre el Documento Base de Contratación'});
    }
    if (mensaje.length > 0) {
      let titulo = 'Información incompleta';
      this.mostrarErroresModal(mensaje, titulo);
    } else {
      return true;
    }
  }

  montosItemValidos() {
    let mensaje = [];
    if (this.proyecto.montoItemsExcedido) {
      mensaje.push({area: 'Módulos/Items', mensaje: 'Corrija el monto excedente para guardar la lista de items'});
    }
    if (mensaje.length > 0) {
      let titulo = 'Monto excedente';
      this.mostrarErroresModal(mensaje, titulo);
    } else {
      return true;
    }
  }

  montoFinanciamientoValido() {
    let mensaje = [];
    if (this.proyecto.montoFinanciamientoExcedido) {
      mensaje.push({area: 'Módulos/Financiamiento', mensaje: 'El monto total del proyecto excede el monto disponible según techo presupuestario'});
    }
    if (mensaje.length > 0) {
      let titulo = 'Monto excedente';
      this.mostrarErroresModal(mensaje, titulo);
    } else {
      return true;
    }
  }

  sonDatosCompletosEvaluacion() {
    if (!this.proyecto.evaluacion) {
      let texto = '<p>Debe completar la información de los siguientes apartados:</p><ul><li><strong>Evaluación</strong>: La tabla de criterios de evaluación esta incompleta</li></ul>';
      let titulo = 'Evaluación';
      this.mostrarErroresModal([], titulo, texto);
    } else {
      if (this.Proyecto.tratarComoProductivo(this.proyecto)) {
        let count = 0;
        let mensaje = [];
        for (let i in this.proyecto.evaluacion.datos) {
          let fila = this.proyecto.evaluacion.datos[i];
          for (let j in fila.cols) {
            if (fila.cols[j].contenido && fila.cols[j].contenido.name == 'NO_CUMPLE' && fila.cols[j].contenido.value) {
              if (mensaje.length < 3) {
                mensaje.push({area: '1. Criterios de evaluación', mensaje: `${fila.cols[j-2].contenido}`});
              } else {
                count++;
              }
            }
          }
        }
        for (let i in this.proyecto.evaluacion.datos2) {
          let fila = this.proyecto.evaluacion.datos2[i];
          for (let j in fila.cols) {
            if (fila.cols[j].contenido && fila.cols[j].contenido.name=='NO_CUMPLE' && fila.cols[j].contenido.value) {
              if (mensaje.length < 3) {
                mensaje.push({area: '2. Criterios generales de elegibilidad', mensaje: `${fila.cols[j-2].contenido}`});
              } else {
                count++;
              }
            }
          }
        }

        for (let i in this.proyecto.evaluacion.datos3) {
          let fila = this.proyecto.evaluacion.datos3[i];
          let fila_requerida = false;

          if (this.Proyecto.tratarComoProductivo(this.proyecto)) fila_requerida = false;

          if (this.proyecto.beneficiarios.productivo) {
            if (this.proyecto.beneficiarios.productivo.hasOwnProperty(fila.key) && this.proyecto.beneficiarios.productivo[fila.key]) fila_requerida = true;
            /*if(fila.key=='agricola' && this.proyecto.beneficiarios.productivo.agricola) fila_requerida = true;
            if(fila.key=='pecuario' && this.proyecto.beneficiarios.productivo.pecuario) fila_requerida = true;
            if(fila.key=='pesca' && this.proyecto.beneficiarios.productivo.pesca) fila_requerida = true;
            if(fila.key=='piscicultura' && this.proyecto.beneficiarios.productivo.piscicultura) fila_requerida = true;
            if(fila.key=='aprovecha_biodiversidad' && this.proyecto.beneficiarios.productivo.aprovecha_biodiversidad) fila_requerida = true;
            if(fila.key=='integral_sustentable_bosque' && this.proyecto.beneficiarios.productivo.integral_sustentable_bosque) fila_requerida = true;
            if(fila.key=='maquinaria' && this.proyecto.beneficiarios.productivo.maquinaria) fila_requerida = true;
            if(fila.key=='prod_agroeco_organica' && this.proyecto.beneficiarios.productivo.prod_agroeco_organica) fila_requerida = true;
            if(fila.key=='sis_agroforestales' && this.proyecto.beneficiarios.productivo.sis_agroforestales) fila_requerida = true;
            if(fila.key=='infraestructura' && this.proyecto.beneficiarios.productivo.infraestructura) fila_requerida = true;
            if(fila.key=='transformacion' && this.proyecto.beneficiarios.productivo.transformacion) fila_requerida = true;*/
          }

          for (let j in fila.cols) {
            if (fila_requerida) {
              if (fila.cols[j].contenido && fila.cols[j].contenido.name=='NO_CUMPLE' && fila.cols[j].contenido.value) {
                if (mensaje.length < 3) {
                  mensaje.push({area: '3. Criterios específicos de elegibilidad', mensaje: `${fila.cols[j-2].contenido}`});
                } else {
                  count++;
                }
              }
            } else {
              if (fila.cols[j].contenido && fila.cols[j].contenido.name=='CUMPLE') {
                fila.cols[j].contenido.value = false;
              }
              if (fila.cols[j].contenido && fila.cols[j].contenido.name=='NO_CUMPLE') {
                fila.cols[j].contenido.value = false;
              }
              if (fila.cols[j].contenido && fila.cols[j].contenido.name=='NO_APLICA') {
                fila.cols[j].contenido.value = true;
              }
            }
          }
        }

        for (let i in this.proyecto.evaluacion.datos4) {
          let fila = this.proyecto.evaluacion.datos4[i];
          for (let j in fila.cols) {
            //verificacion fila 16
            if (!(this.proyecto.beneficiarios && this.proyecto.beneficiarios.tamanio_proyecto == 'OP_TP_MENORES' && fila.key == 'seccion_4_1')) {
              if (fila.cols[j].contenido && fila.cols[j].contenido.name == 'NO_CUMPLE' && fila.cols[j].contenido.value) {
                if (mensaje.length < 3) {
                  mensaje.push({area: '4. Factibilidad del proyecto', mensaje: `${fila.cols[j-2].contenido}`});
                } else {
                  count++;
                }
              }
            }
          }
        }

        if (mensaje.length > 0) {
          if (count > 0) {
            mensaje.push({area: '...', mensaje: `Y ${count} criterio/s mas...`});
          }
          let texto = '<p>El proyecto tiene parámetros o criterios de evaluación que "no cumplen" para su aprobación:</p><ul>';
          let titulo = 'Información incompleta';
          this.mostrarErroresModal(mensaje, titulo, texto);
        } else {
          return true;
        }
      } else {
        return true;
      }
    }
  }

  sonDatosCompletosInforme() {
    if (!this.proyecto.informe) {
      let texto = '<p>Debe completar la información de los siguientes apartados:</p><ul><li><strong>Informe de Evaluación</strong>: No existe ningún informe.</li></ul>';
      let titulo = 'Informe';
      this.mostrarErroresModal([], titulo, texto);
    } else {
      let count = 0;
      let mensaje = [];

      if (this.proyecto.informe) {
        if (!this.proyecto.informe.director) {
          if (mensaje.length < 3) {
            mensaje.push({area: 'Informe', mensaje: `Nombre del director`});
          } else {
            count++;
          }
        }
      }
      if (this.proyecto.informe) {
        if (!this.proyecto.informe.codigo_documento) {
          if (mensaje.length < 3) {
            mensaje.push({area: 'Informe', mensaje: `El informe no se ha generado`});
          } else {
            count++;
          }
        }
      }
      if (this.Proyecto.tratarComoProductivo(this.proyecto)) {
        let estado_evaluacion = this.proyecto.evaluacion.datos5[0].cols.find((col) => { return (col.contenido && col.contenido.value); });
        if (!estado_evaluacion || !estado_evaluacion.contenido || estado_evaluacion.contenido.name!='APROBADO') {
          mensaje.push({area: 'Informe', mensaje: `De acuerdo a la revisión realizada el proyecto no se encuentra aprobado.`});
        }
      }
      if (mensaje.length > 0) {
        if (count > 0) {
          mensaje.push({area: '...', mensaje: `Y ${count} criterio/s mas...`});
        }
        let texto = '<p>El proyecto tiene criterios que deben ser cumplidos:</p><ul>';
        let titulo = 'Información incompleta';
        this.mostrarErroresModal(mensaje, titulo, texto);
      } else {
        return true;
      }
    }
  }

  sonDatosCompletosProductivo() {
    if (this.proyecto.beneficiarios && this.Proyecto.tratarComoProductivo(this.proyecto)) {
      if (!this.proyecto.beneficiarios.productivo) {
        let texto = '<p>Debe completar la información:</p><ul><li><strong>Proyecto Productivo</strong>: Marcar una o mas subclasificaciones.</li></ul>';
        let titulo = 'Informe';
        this.mostrarErroresModal([], titulo, texto);
        return false;
      } else {
        let count = 0;

        for (let i in this.proyecto.beneficiarios.productivo) {
          if (this.proyecto.beneficiarios.productivo[i]) {
            count++;
          }
        }
        if (count == 0) {
          let texto = '<p>Debe completar la información:</p><ul><li><strong>Proyecto Productivo</strong>: Marcar una o mas subclasificaciones.</li></ul>';
          let titulo = 'Informe';
          this.mostrarErroresModal([], titulo, texto);
          return false;
        }
      }
    }
    return true;
  }

  getModulosIncompletos() {
    let incompletos = 0;
    this.modulos.map(modulo => {
      if (!modulo.items || modulo.items.length < 1) {
        incompletos ++;
      }
      for (let i in modulo.items) {
        if (!modulo.items[i].id_item || modulo.items[i].edicion) {
          incompletos ++;
        }
      }
    });
    return incompletos;
  }

  existeOrdenDeProceder() {
    if (!this.proyecto.adj || !this.proyecto.adj.orden_proceder) {
      let texto = '<p>Debe completar la información de los siguientes apartados:</p><ul><li><strong>Actvidades e items</strong>: No ha especificado la fecha de orden de proceder de la adjudicación seleccionada.</li></ul>';
      let titulo = 'Orden de proceder de adjudicación';
      this.mostrarErroresModal([], titulo, texto);
    } else if (!this.proyecto.orden_proceder) {
      let texto = '<p>Debe completar la información de los siguientes apartados:</p><ul><li><strong>Datos Generales</strong>: No ha especificado la fecha de orden de proceder.</li></ul>';
      let titulo = 'Orden de proceder';
      this.mostrarErroresModal([], titulo, texto);
    } else {
      return true;
    }
  }

  existeDocEspTecnicas() {
    if (!this.proyecto.adj || !this.proyecto.adj.doc_especificaciones_tecnicas) {
      let texto = '<p>Debe completar la información en los siguientes apartados:<ul><li><strong>Actividades e items</strong>: No ha cargado el documento de especificaciones técnicas de la adjudicación deleccionada.</li></ul>';
      let titulo = 'Documento de especificaciones técnicas';
      this.mostrarErroresModal([], titulo, texto);
    } else {
      return true;
    }
  }

  verificarEnParalelo(accion) {
    if (accion.label === 'Guardar') {
      return false;
    }
    if (this.Proyecto.esEditable('beneficiarios') && !this.sonDatosCompletosProductivo()) {
      return true;
    }
    if (this.Proyecto.esEditable('autoridad_beneficiaria') && !this.sonDatosCompletosAutoridad()) {
      return true;
    }
    if (this.Proyecto.esEditable('modulos') && !this.sonDatosCompletosModulos()) {
      return true;
    }
    if (this.Proyecto.esEditable('evaluacion') && !this.sonDatosCompletosEvaluacion()) {
      return true;
    }
    if (this.Proyecto.esEditable('evaluacion') && !this.sonDatosCompletosInforme()) {
      return true;
    }
    if (this.Proyecto.esEditable('doc_especificaciones_tecnicas') && !this.existeDocEspTecnicas()) {
      return true;
    }
    if (this.Proyecto.esEditable('orden_proceder') && !this.existeOrdenDeProceder()) {
      return true;
    }

    return false;
  }

  // actualizarTechoDisponible() {
  //   const data = {
  //     idProyecto: this.proyecto.id_proyecto,
  //     montoActualizar: this.proyecto.financiamiento.monto_fdi ? this.proyecto.financiamiento.monto_fdi : 0,
  //     fechaValidos: this.proyecto._fecha_creacion
  //   };
  //   const codigo = this.proyecto.dpa.municipio.codigo;
  //   this.DataService.put(`techo_presupuestario/${codigo}/recalcular`, data).then((resp) => {
  //     if (resp && resp.finalizado) {
  //       console.log('------------------------------------');
  //       console.log(resp);
  //       console.log('------------------------------------');
  //     }
  //   });
  // }

  tieneEvaluacionExterna() {
    let mensaje = [];
    console.log("EVALUACION EXTERNA");
    console.log(this.proyecto.doc_ev_externa);
    if (!this.proyecto.doc_ev_externa || !this.proyecto.doc_ev_externa.base64 || !this.proyecto.doc_ev_externa.nombre || !this.proyecto.doc_ev_externa.path || !this.proyecto.doc_ev_externa.size || !this.proyecto.doc_ev_externa.tipo) {
      if (typeof this.proyecto.doc_ev_externa === 'string' || this.proyecto.doc_ev_externa instanceof String) {
        if (this.proyecto.doc_ev_externa == "") {
          mensaje.push({area: 'Módulos/Evaluación', mensaje: 'Debe cargar el documento de evaluación externa en el apartado Evaluación.'});
        }
      } else {
        mensaje.push({area: 'Módulos/Evaluación', mensaje: 'Debe cargar el documento de evaluación externa en el apartado Evaluación.'});
      }
    }
    if (mensaje.length > 0) {
      let titulo = 'Documento de Evaluación externa';
      this.mostrarErroresModal(mensaje, titulo);
    } else {
      return true;
    }
  }
}

export default ProyectoController;
