/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';
import modalRegistroController from './registro.modal.controller';
import modalRegistroTemplate from './registro.modal.html';

class EquipoTecnicoController {
  constructor(Modal, DataService, _, Usuarios, Message, Storage) {
    'ngInject';
    this.Modal = Modal;
    this.Message = Message;
    this.DataService = DataService;
    this.user = Storage.getUser();
    this._ = _;
    this.Usuarios = Usuarios;
    this.url = '';
    this.esEquipoCompleto = true;
  }

  $onInit() {
    this.listaEquipo = [];
    if (!this.adjudicaciones) {
      this.adjudicaciones = {};
    }
    if (!this.adjudicaciones.empresas) {
      this.adjudicaciones.empresas = [];
    }
    this.adjudicaciones.nro_adjudicaciones = this.adjudicaciones.empresas.length;

    this.url = `proyectos/${this.proyecto.id_proyecto}/equipos_tecnicos`;
    this.getCargos();
    this.getListaEquipo();
    this.urlReconfirmacion = `proyectos/${this.proyecto.id_proyecto}/equipos_tecnicos/`;
    this.esResponsableProy = this.Usuarios.esResponsableProyecto();
    this.esResponsableGAM = this.Usuarios.esResponsableGAM();

    this.monto_total = this.proyecto.financiamiento.monto_total_proyecto > this.proyecto.monto_total ? this.proyecto.financiamiento.monto_total_proyecto : this.proyecto.monto_total;
    this.monto_restante = this.proyecto.monto_total;

    this.obtenerAdjudicaciones();
  }

  getListaEquipo() {
    this.DataService.get(`${this.url}?rol=${this.user.rol.id_rol}`).then(result => {
      if (result && result.finalizado) {
        this.listaEquipo = result.datos.rows;
        this.verificarEquipoCompleto();
        this.proyecto.equipo_tecnico = this.listaEquipo;
      }
    });
  }

  getCargos() {
    this.DataService.get(`parametricas/CARGO`).then( response => {
      if (response) {
        this.cargos = response.datos.rows;
        this.verificarEquipoCompleto();
      }
    });
  }

  adicionar(callbackOk, callbackCancel) {
    let adicionMiembro = this.Modal.show({
      template: modalRegistroTemplate,
      controller: modalRegistroController,
      data: {equipo: this.listaEquipo, cargos: this.cargos, registrar: !callbackOk},
      size: 'lg'
    });
    adicionMiembro.result.then( (result) => {
      if (result) {
        if (typeof(callbackOk) === 'function') {
          callbackOk(result);
        }
        this.getListaEquipo();
      } else {
        if (typeof(callbackCancel) === 'function') {
          callbackCancel();
        }
      }
    });
  }

  getListaEquipoAdjudicacion(index) {
    let urlEquipoTecnico = `proyectos/${this.proyecto.id_proyecto}/equipos_tecnicos/adjudicaciones/${this.adjudicaciones.empresas[index].id_adjudicacion}`;
    let servicioEquipoTecnico = 'get';
    this.DataService[servicioEquipoTecnico](urlEquipoTecnico).then(resultEquipoTecnico => {
      if (resultEquipoTecnico) {
        this.adjudicaciones.empresas[index].equipo = resultEquipoTecnico.datos.rows;
      }
    });
  }

  cargoSupervisor() {
    let supervisor = [];
    for (let i = 0; i < this.cargos.length; i++) {
      if (this.cargos[i].codigo == 'CG_SUPERVISOR') {
        supervisor.push(this.cargos[i]);
      }
    }
    return supervisor;
  }

  adicionarSupervisorAdjudicacion(index,callbackOk, callbackCancel) {
    let adicionMiembro = this.Modal.show({
      template: modalRegistroTemplate,
      controller: modalRegistroController,
      data: {
        id_adjudicacion: this.adjudicaciones.empresas[index].id_adjudicacion,
        referencia: this.adjudicaciones.empresas[index].referencia,
        equipo: this.adjudicaciones.empresas[index].equipo,
        cargos: this.cargoSupervisor(),
        registrar: !callbackOk
      },
      size: 'lg'
    });
    adicionMiembro.result.then( (result) => {
      if (result) {
        if (typeof(callbackOk) === 'function') {
          callbackOk(result);
        }
        this.getListaEquipoAdjudicacion(index);
      } else {
        if (typeof(callbackCancel) === 'function') {
          callbackCancel();
        }
      }
    });
  }

  verificarEquipoCompleto() {
    var nroMiembros = 0;
    this.cargos.map(cargo => {
      const MIEMBRO = this._.find(this.listaEquipo, (miembro) => {
        return miembro.cargo === cargo.codigo;
      });
      if (MIEMBRO) {
        nroMiembros ++;
      }
    });
    this.esEquipoCompleto = this.cargos.length === nroMiembros;
  }

  esSupervisorOFiscal(usuario) {
    return usuario.estado == 'ACTIVO' && ['CG_SUPERVISOR', 'CG_FISCAL'].indexOf(usuario.cargo) >= 0;
  }

  verDatos(miembro) {
    const EDICION = this.Modal.show({
      template: modalRegistroTemplate,
      controller: modalRegistroController,
      data: {equipo: this.listaEquipo, cargos: this.cargos, miembro: miembro, modoVista: this.modoVista},
      size: 'lg'
    });

    EDICION.result.then( (result) => {
      if (result) {
        this.getListaEquipo();
      }
    });
  }

  inactivarUsuarioAdjudicacion(usuario,index) {
    console.log("index usuario=>"+usuario);
    let datos_usuario = this.adjudicaciones.empresas[index].equipo[usuario];
    console.log(datos_usuario);
    let mensaje = `Esta seguro/a de inhabilitar al ${datos_usuario.cargo_usuario.nombre}?<br><br>Este usuario ya no podra ser habilitado nuevamente con el mismo cargo.<br><br>* Deberá agregar un nuevo usuario.`;
    this.Modal.confirm(mensaje, () => {
      console.log("inactivar");
      this.adjudicaciones.empresas[index].equipo[usuario].estado = 'INACTIVO';
      this.adicionarSupervisorAdjudicacion(index,(persona) => {
        this.DataService.put(`${this.url}/${datos_usuario.id_equipo_tecnico}/reemplazar`, persona).then((resp) => {
          if (resp && resp.finalizado) {
            this.getListaEquipoAdjudicacion(index);
          } else {
            this.Message.warning('Sucedió un problema al realizar la inhabilitación.');
          }
        });
      }, () => {
        this.adjudicaciones.empresas[index].equipo[usuario].estado = 'ACTIVO';
      });
    }, null, 'Inhabilitar', 'Si estoy seguro/a', null, 'ban');
  }

  inactivarUsuario(usuario) {
    let mensaje = `Esta seguro/a de inhabilitar al ${usuario.cargo_usuario.nombre}?<br><br>Este usuario ya no podra ser habilitado nuevamente con el mismo cargo.<br><br>* Deberá agregar un nuevo usuario.`;
    this.Modal.confirm(mensaje, () => {
      usuario.estado = 'INACTIVO';
      this.adicionar((persona) => {
        this.DataService.put(`${this.url}/${usuario.id_equipo_tecnico}/reemplazar`, persona).then((resp) => {
          if (resp && resp.finalizado) {
            this.getListaEquipo();
          } else {
            this.Message.warning('Sucedió un problema al realizar la inhabilitación.');
          }
        });
      }, () => {
        usuario.estado = 'ACTIVO';
      });
    }, null, 'Inhabilitar', 'Si estoy seguro/a', null, 'ban');
  }

  guardarAdjudicacion(index) {
    let url = `proyectos/${this.proyecto.id_proyecto}/adjudicaciones`;
    let servicio = 'post';
    if (this.adjudicaciones.empresas[index].id_adjudicacion > 0) {
      url = `proyectos/${this.proyecto.id_proyecto}/adjudicaciones/${this.adjudicaciones.empresas[index].id_adjudicacion}`;
      servicio = 'put';
    }

    let adjudicacion = angular.copy(this.adjudicaciones.empresas[index]);
    if (adjudicacion.doc_adjudicacion === null) {
      delete adjudicacion.doc_adjudicacion;
    }
    // if (adjudicacion.monto <= this.monto_restante) {
      this.DataService[servicio](url, adjudicacion).then(result => {
        if (result) {
          this.Message.success(result.mensaje);
          this.adjudicaciones.empresas[index].id_adjudicacion = result.datos.id_adjudicacion;
          this.adjudicaciones.empresas[index].doc_adjudicacion = result.datos.doc_adjudicacion;
          this.adjudicaciones.empresas[index].archivo = {};
          let docAdjudicacion = this.adjudicaciones.empresas[index].doc_adjudicacion.substring(1, this.adjudicaciones.empresas[index].doc_adjudicacion.indexOf(']'));
          this.adjudicaciones.empresas[index].archivo.url = `proyectos/${this.proyecto.id_proyecto}/doc/${docAdjudicacion}`;
          this.adjudicaciones.empresas[index].archivo.urlDescarga = `proyectos/${this.proyecto.id_proyecto}/doc/${docAdjudicacion}/descargar`;
          this.monto_restante = this.calcularMontoRestante();
        }
      });
    // } else {
    //   this.Message.warning('El monto asignado excede el monto disponible restante para adjudicaciones');
    // }
  }

  agregarAdjudicacion() {
    if (!this.adjudicaciones) {
      this.adjudicaciones = {};
    }
    if (!this.adjudicaciones.empresas) {
      this.adjudicaciones.empresas = [];
    }
    let empresa = {
      id_adjudicacion: 0,
      nro: this.adjudicaciones.nro_adjudicaciones? this.adjudicaciones.nro_adjudicaciones + 1: 1,
      monto: 0,
      referencia: '',
      doc_adjudicacion: null
    };
    this.adjudicaciones.empresas.push(empresa);
    this.adjudicaciones.nro_adjudicaciones = this.adjudicaciones.empresas.length;
  }

  quitarAdjudicacion(index) {
    if (this.adjudicaciones.empresas[index].id_adjudicacion > 0) {
      this.Modal.confirm(`Esta seguro/a de Eliminar la Adjudicacion ${this.adjudicaciones.empresas[index].referencia}`, () => {
        let adjudicacion = this.adjudicaciones.empresas[index];
        adjudicacion.estado = 'INACTIVO';
        let url = `proyectos/${this.proyecto.id_proyecto}/adjudicaciones/${adjudicacion.id_adjudicacion}/eliminar`;
        let servicio = 'put';
        this.DataService[servicio](url, adjudicacion).then(result => {
          if (result) {
            this.Message.success(result.mensaje);
            this.adjudicaciones.empresas.splice(index, 1);
            this.adjudicaciones.nro_adjudicaciones = this.adjudicaciones.empresas.length;
            this.monto_restante = this.calcularMontoRestante();
          }
        });
      }, null, 'Inhabilitar', 'Si estoy seguro/a', null, 'ban');
    } else {
      if (this.adjudicaciones.empresas && this.adjudicaciones.empresas.length > 0) {
        this.adjudicaciones.empresas.splice(index, 1);
        this.adjudicaciones.nro_adjudicaciones = this.adjudicaciones.empresas.length;
        this.monto_restante = this.calcularMontoRestante();
      }
    }
  }

  tieneFiscal() {
    for (let i = 0; i < this.listaEquipo.length; i++) {
      if (this.listaEquipo[i].cargo == 'CG_FISCAL') {
        return true;
      }
    }
    return false;
  }

  tieneSupervisor(adjudicacion) {
    if (adjudicacion && adjudicacion.equipo && adjudicacion.equipo.length > 0) {
      for (let i = 0; i < adjudicacion.equipo.length; i++) {
        if (adjudicacion.equipo[i].cargo === 'CG_SUPERVISOR') {
          return true;
        }
      }
    }
    return false;
  }

  tieneSupervisiones(index) {
    if (this.adjudicaciones.empresas && this.adjudicaciones.empresas[index].id_adjudicacion > 0) {
      const adjudicacionActual = this.adjudicaciones.empresas[index].id_adjudicacion;
      if (this.proyecto && this.proyecto.supervisiones && this.proyecto.supervisiones.length > 0) {
        for (let i in this.proyecto.supervisiones) {
          if (this.proyecto.supervisiones[i].fid_adjudicacion === adjudicacionActual) {
            return true;
          }
        }
      }
    }
    return false;
  }

  puedeVerAdjudicaciones() {
    return this.proyecto && ['ASIGNACION_EQUIPO_TECNICO_FDI', 'APROBADO_FDI', 'EN_EJECUCION_FDI'].indexOf(this.proyecto.estado_proyecto) >= 0 && this.tieneFiscal();
  }

  obtenerAdjudicaciones() {
    let url = `proyectos/${this.proyecto.id_proyecto}/adjudicaciones`;
    let servicio = 'get';
    this.DataService[servicio](url).then(result => {
      if (result) {
        for (let i = 0; i < result.datos.rows.length; i++) {
          let adjudicacion = result.datos.rows[i];
          adjudicacion.archivo = {};
          let docAdjudicacion = adjudicacion.doc_adjudicacion.substring(1, adjudicacion.doc_adjudicacion.indexOf(']'));
          adjudicacion.archivo.url = `proyectos/${this.proyecto.id_proyecto}/doc/${docAdjudicacion}`;
          adjudicacion.archivo.urlDescarga = `proyectos/${this.proyecto.id_proyecto}/doc/${docAdjudicacion}/descargar`;
          this.adjudicaciones.empresas.push(adjudicacion);
          let urlEquipoTecnico = `proyectos/${this.proyecto.id_proyecto}/equipos_tecnicos/adjudicaciones/${adjudicacion.id_adjudicacion}`;
          let servicioEquipoTecnico = 'get';
          this.DataService[servicioEquipoTecnico](urlEquipoTecnico).then(resultEquipoTecnico => {
            if (resultEquipoTecnico) {
              adjudicacion.equipo = resultEquipoTecnico.datos.rows;
            }
          });
        }
        this.adjudicaciones.nro_adjudicaciones = this.adjudicaciones.empresas.length;
        if (this.adjudicaciones.nro_adjudicaciones > 0) {
          this.monto_restante = this.calcularMontoRestante();
        }
      }
    });
  }

  calcularMontoRestante() {
    let monto_restante = this.monto_total;
    for (let i in this.adjudicaciones.empresas) {
      let adjudicacion = this.adjudicaciones.empresas[i];
      if (adjudicacion.estado !== 'INACTIVO') {
        monto_restante -= adjudicacion.monto ? adjudicacion.monto : 0;
      }
    }
    return monto_restante;
  }
}

export default EquipoTecnicoController;
