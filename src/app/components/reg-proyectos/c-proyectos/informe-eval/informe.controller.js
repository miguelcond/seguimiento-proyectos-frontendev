/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

import fixFechaController from '../evaluacion/fix-fecha.controller';
import fixFechaTemplate from '../evaluacion/fix-fecha.html';

class InformeController {
  constructor($scope, $log, $timeout, DataService, Message, Modal, Loading, apiUrl, Util, Storage, Usuarios, Proyecto) {
    'ngInject';

    this.$scope = $scope;
    this.apiUrl = apiUrl;
    // this.$log = $log;
    this.$timeout = $timeout;
    this.DataService = DataService;
    this.Modal = Modal;
    this.Message = Message;
    this.Loading = Loading;
    this.Util = Util;
    this.Proyecto = Proyecto;
    this.user = Storage.getUser();
    this.esTecEval = [12,13].indexOf(this.user.rol.id_rol) >= 0;
    this.Usuarios = Usuarios;
  }

  $onInit() {
    if (!this.proyecto.informe) {
      this.proyecto.informe = {};
      // Obtener plantilla de datos requeridos.
      this.$obtenerFormInforme();

      if (!this.proyecto.informe.director || this.proyecto.informe.director == undefined || this.proyecto.informe.director.trim() == "") {
        this.proyecto.informe.director = this.proyecto.director_gral.nombre || '';
      }

      if (!this.proyecto.informe.indicador) this.proyecto.informe.indicador = {};
      if (!this.proyecto.informe.indicador.proceso || this.proyecto.informe.indicador.proceso == '') {
        this.proyecto.informe.indicador.proceso = this.proyecto.beneficiarios.indicadores_impacto[0].combinado || '0';
      }
    }

    // Parámetros de descarga
    this.descarga = {
      urlDescarga: `${this.apiUrl}proyectos/${this.proyecto.id_proyecto}/doc/RIE/descargar`,
      nombreDescarga: `Informe resumen`
    };

    if (this.proyecto.informe.valoracion.length < 9) {
      this.agregarOpcionFps();
    }

    this.$scope.$watch('$ctrl.proyecto.beneficiarios.indicadores_impacto', () => {
      this.actualizarIndicadoresImpacto();
    });

    this.$scope.$watch('$ctrl.proyecto.informe.indicador.linea_base', () => {
      // this.$log.log('Nuevo indicador de impacto', this);
      this.actualizarIndicadoresImpacto();
    });
  }

  actualizarIndicadoresImpacto() {
    this.proyecto.informe.indicador.proceso = this.proyecto.beneficiarios.indicadores_impacto[0].combinado;
    let _linea_base = parseFloat(this.proyecto.informe.indicador.linea_base);
    _linea_base = isNaN(_linea_base) ? 0 : _linea_base;

    let _indicador_impacto = this.proyecto.informe.indicador.proceso;
    let _indicador_impacto_numero = _indicador_impacto ? parseFloat(_indicador_impacto.split(' ')[0]) : 0;
    let _indicador_impacto_unidad = _indicador_impacto ? _indicador_impacto.split(' ')[1] : '';
    _indicador_impacto_numero = isNaN(_indicador_impacto_numero) ? 0 : _indicador_impacto_numero;
    this.proyecto.informe.indicador.impacto =  `${this.Util.redondear((_linea_base + _indicador_impacto_numero))} ${_indicador_impacto_unidad}`;
  }

  radioChange(cols, index) {
    if (!this.modoVista) {
      for (let i in cols) {
        if (cols[i].contenido && cols[i].contenido.type === 'radio') {
          cols[i].contenido.value = (i == index);
        }
      }
    }
  }

  radioColor(contenido) {
    if (contenido.value && contenido.name == 'CUMPLE') return 'btn-success';
    if (contenido.value && contenido.name == 'NO_CUMPLE') return 'btn-danger';
    return 'btn-secondary';
  }

  generarInformePdf(form) {
    if (!this.proyecto.evaluacion) {
      this.Message.warning('Debe completar la pestaña de evaluación para poder continuar!');
      return;
    }
    // Obtener estado de la evaluacion que tenga valor true
    let estado_evaluacion = 'APROBAR';
    if (this.Proyecto.tratarComoProductivo(this.proyecto)) {
      estado_evaluacion = this.proyecto.evaluacion.datos5[0].cols.find((col) => { return (col.contenido && col.contenido.value); });
      if (!estado_evaluacion || !estado_evaluacion.contenido || !(estado_evaluacion.contenido.name === 'APROBADO' || estado_evaluacion.contenido.name === 'RECHAZO')) {
        this.Message.warning('La evaluación debe estar en estado APROBADO ó RECHAZADO!');
        return;
      }
      estado_evaluacion = (estado_evaluacion.contenido.name == 'RECHAZO') ? 'RECHAZAR': 'APROBAR';
    }
    if (!this.$$datosCompletos(form)) {
      this.Message.warning(`Debe completar los campos marcados con *`);
      return;
    }
    this.Modal.confirm('¿Esta seguro/a de generar el informe?.', () => {
      // Parsear datos a enviar
      let _datos = angular.copy(this.proyecto.informe);
      _datos.id_proyecto = this.proyecto.id_proyecto;
      _datos.$_plantilla = 102;
      _datos.$_nombre = 'informe';
      _datos.codigo = this.proyecto.codigo;
      _datos.estado_evaluacion = estado_evaluacion;
      _datos.revisor = this.proyecto.evaluacion.revisor;
      _datos.jefe_area = this.proyecto.evaluacion.jefe_area;
      _datos.jefe_depto = this.proyecto.evaluacion.jefe_depto;
      this.DataService.post(`proyectos/${this.proyecto.id_proyecto}/informe-evaluacion`, _datos).then((resp) => {
        if (resp && resp.finalizado) {
          this.Message.success(resp.mensaje);
          this.documentos.push(resp.datos);
          this.proyecto.informe.codigo_documento = resp.datos.codigo_documento;
        }
      });
    }, null, null, 'Si estoy seguro/a');
  }

  verInforme(doc) {
    this.Modal.showFormularioBase64(`proyectos/${this.proyecto.id_proyecto}/doc/INF?codigo=${doc.codigo_documento}`,`Informe de evaluación`);
  }

  guardarDatos() {
    this.Loading.show('Registrando proyecto', true);
    this.Message.success("Informe de evaluación creado.");
    this.cerrar();
    this.Loading.hide();
  }

  initPlantilla() {
    this.$timeout(() => {
      if (this.proyecto) {
        this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/informes`).then((resp) => {
          if (resp && resp.finalizado) {
            this.documentos = resp.datos;
          }
        });
      }
    },500);
  }

  agregarOpcionFps() {
    this.proyecto.informe.valoracion.push({
      "cols": [
        {"contenido": "Trabajo conjunto FPS"},
        {"contenido": "La evaluación de este proyecto fue realizada en colaboración con el FPS"},
        {"colspan": 2, "contenido": {"type": "select", "options":[{"codigo":"0","nombre":"No"}, {"codigo":"1","nombre":"Si"}], "label":"evaluación realizada por el FPS", "value": "No"}}
      ]
    });
  }

  $obtenerFormInforme() {
    this.proyecto.informe.valoracion = [];
    this.DataService.get(`contenidos/informe_evaluacion/1`).then(response => {
      if (response) {
        if (response.datos && response.datos.estructura) {
          if (response.datos.estructura.length > 6) {
            let row5 = response.datos.estructura[5];
            let row6 = response.datos.estructura[6];
            // Se cargan las condiciones necesarias
            if (row5.cols && row5.cols.length > 0) {
              row5.cols[0].contenido = `Valor ${!this.Proyecto.tratarComoProductivo(this.proyecto) ? 'VANS' : 'VANE'}`;
            }
            if (row6.cols && row6.cols.length > 0) {
              row6.cols[0].contenido = `Valor ${!this.Proyecto.tratarComoProductivo(this.proyecto) ? 'VANP' : 'VANF'}`;
            }

            response.datos.estructura[5] = row5;
            response.datos.estructura[6] = row6;

            this.proyecto.informe.valoracion = response.datos.estructura;
          }
        }
      }
    });

    // this.proyecto.informe.valoracion = [
    //   {
    //     cols: [
    //       { rowspan: 2, contenido: 'Sostenibilidad técnica' },
    //       { contenido: 'Requerimiento de componentes necesarios para la operación y/o funcionamiento del proyecto.' },
    //       { contenido: { type: 'radio', value: false, name: 'CUMPLE' } },
    //       { contenido: { type: 'radio', value: true, name: 'NO_CUMPLE' } },
    //     ]
    //   },
    //   {
    //     cols: [
    //       { contenido: 'Distribución porcentual de los costos de inversión fija, capital de trabajo e inversión diferida.' },
    //       { contenido: { type: 'radio', value: false, name: 'CUMPLE' } },
    //       { contenido: { type: 'radio', value: true, name: 'NO_CUMPLE' } },
    //     ]
    //   },
    //   {
    //     cols: [
    //       { contenido: 'Sostenibilidad social' },
    //       { contenido: 'Actas de consenso y socialización' },
    //       { contenido: { type: 'radio', value: false, name: 'CUMPLE' } },
    //       { contenido: { type: 'radio', value: true, name: 'NO_CUMPLE' } },
    //     ]
    //   },
    //   {
    //     cols: [
    //       { contenido: 'Sostenibilidad organizativo institucional' },
    //       { contenido: 'Actores y roles definidos para la operación, mantenimiento y administración del proyecto' },
    //       { contenido: { type: 'radio', value: false, name: 'CUMPLE' } },
    //       { contenido: { type: 'radio', value: true, name: 'NO_CUMPLE' } },
    //     ]
    //   },
    //   {
    //     cols: [
    //       { rowspan: 3, contenido: 'Sostenibilidad económica' },
    //       { contenido: 'Tiempo de operación del proyecto' },
    //       { contenido: { type: 'radio', value: false, name: 'CUMPLE' } },
    //       { contenido: { type: 'radio', value: true, name: 'NO_CUMPLE' } },
    //     ]
    //   },
    //   {
    //     cols: [
    //       { contenido: `Valor ${(this.proyecto.beneficiarios.tipo!=='OP_TP_PRODUCTIVO')? 'VACP': 'VANE'}` },
    //       { colspan: 2, contenido: { type: 'inputMoney', value: 0, name: 'valor_vane' } },
    //     ]
    //   },
    //   {
    //     cols: [
    //       { contenido: `Valor ${(this.proyecto.beneficiarios.tipo!=='OP_TP_PRODUCTIVO')? 'VACS': 'VANF'}` },
    //       { colspan: 2, contenido: { type: 'inputMoney', value: 0, name: 'valor_vanf' } },
    //     ]
    //   },
    //   {
    //     cols: [
    //       { contenido: 'Sostenibilidad ambiental' },
    //       { contenido: 'Dispensación o Evaluación del Impacto Ambiental' },
    //       { colspan: 2, contenido: { type: 'textarea', value: '* El proyecto cuenta con certificado de dispensación ambiental\n* Se encuentra en proceso de tramite' } }
    //     ]
    //   }
    // ];

    //     this.proyecto.informe.criterios = [
    //       //*******
    //       {
    //         cols: [
    //           { contenido: '1' },
    //           { contenido: { type: 'textarea', value: 'El proyecto ha sido priorizado de manera consensuada por las organizaciones indígenas originario campesinas.' } },
    //           { contenido: { type: 'textarea', value: 'Acta de consenso de priorización del proyecto, firmada por las Organizaciones Indígenas Originario Campesinas.' } }
    //         ]
    //       },
    //       {
    //         cols: [
    //           { contenido: '2' },
    //           { contenido: { type: 'textarea', value: 'El EDTP corresponde a uno de los tipos de programas y/o proyectos establecidos por el FDI.' } },
    //           { contenido: { type: 'textarea', value: 'Documento del EDTP.' } }
    //         ]
    //       },
    //       {
    //         cols: [
    //           { contenido: '3' },
    //           { contenido: { type: 'textarea', value: `${(this.proyecto.beneficiarios.tipo!=='OP_TP_PRODUCTIVO')?'El EDTP está elaborado en el formato del Viceministerio de Recursos Hídricos y Riego, dependiente del Ministerio de Medio Ambiente y Agua.\n':''}
    // El EDTP está elaborado en el formato correspondiente de acuerdo a lo establecido por el FDI.` } },
    //           { contenido: { type: 'textarea', value: 'Documento del EDTP' } }
    //         ]
    //       },
    //       {
    //         cols: [
    //           { contenido: '4' },
    //           { contenido: { type: 'textarea', value: 'La población beneficiaria del proyecto está constituida por familias de pequeños productores de los pueblos indígena originario campesinos, comunidades interculturales y/o afrobolivianas.' } },
    //           { contenido: { type: 'textarea', value: 'Lista de beneficiarios con N° de Cédula de Identidad y firma.' } }
    //         ]
    //       },
    //       {
    //         cols: [
    //           { contenido: '5' },
    //           { contenido: { type: 'textarea', value: 'El proyecto no sobrepasa los techos presupuestarios definidos por el FDI.' } },
    //           { contenido: { type: 'textarea', value: 'Estructura de financiamiento del proyecto.' } }
    //         ]
    //       }
    //       //*******
    //     ];
  }

  corregirFecha(doc) {
    this.Modal.show({
      title: 'Modificar fecha!',
      controller: fixFechaController,
      template: fixFechaTemplate,
      data: doc
    }).result.then((resp) => {
      let datos = {
        codigo_documento: resp.codigo_documento,
        fecha_documento: resp.fecha_documento,
      }
      this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/formulario/informe/${resp.id_formulario}`, datos).then((resp) => {
        if (resp && resp.finalizado) {
          doc.fecha_documento = resp.datos.fecha_documento;
          this.Message.success(resp.mensaje);
        }
      });
    }).catch(() => {});
  }

  estado(doc) {
    this.Modal.confirm('¿Esta seguro/a de cambiar el estado?.', () => {
      let datos = {
        codigo_documento: doc.codigo_documento,
        estado: doc.estado === 'INACTIVO' ? 'ELABORADO' : 'INACTIVO',
      }
      this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/formulario/informe/${doc.id_formulario}`, datos).then((resp) => {
        if (resp && resp.finalizado) {
          doc.estado = resp.datos.estado;
          this.Message.success(resp.mensaje);
        }
      });
    }, null, null, 'Si estoy seguro/a');
  }

  // Metodos privados
  $$datosCompletos(form) {
    return form.$valid;
  }
}

export default InformeController;
