'use strict';

class ModuloController {
    constructor($uibModalInstance, data, title, icon, DataService, Message) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.data = data;
        this.title = title;
        this.icon = icon;
        this.DataService = DataService;
        this.Message = Message;
    }
    $onInit() {
      this.modulos = [];
      this.mensajes = this.data;
    }

    guardar() {
      this.DataService.post(`modulos`, this.modulo).then( resp => {
        if (resp) {
          this.Message.success("Se registró el módulo correctamente.");
          this.$uibModalInstance.close(resp);
        }
      });
    }

    actualizar() {
      this.DataService.put(`modulos/${this.modulo.id_modulo}`, this.modulo).then( resp => {
        if (resp) {
          this.Message.success("Se actualizó el módulo correctamente.");
          this.$uibModalInstance.close(resp);
        }
      });
    }

    cerrar() {
      this.$uibModalInstance.dismiss('cancel');
    }
}

export default ModuloController;
