/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

import modalModuloController from './modulo.modal.controller';
import modalModuloTemplate from './modulo.modal.html';
import modalArchivoController from './archivo.modal.controller';
import modalArchivoTemplate from './archivo.modal.html';
import modalValidacionController from './modal/validacion.controller';
import modalValidacionTemplate from './modal/validacion.html';

class ItemsController {
  constructor($scope, $window, DataService, Util,  Modal, NgTableParams, _, Message, Proyecto, Storage, Usuarios, Datetime) {
    'ngInject';
    this.$scope = $scope;
    this.$window = $window;
    this.DataService = DataService;
    this.Util = Util;
    this.Modal = Modal;
    this.NgTableParams = NgTableParams;
    this._ = _;
    this.Message = Message;
    this.Proyecto = Proyecto;
    this.Usuarios = Usuarios;
    this.Datetime = Datetime;
    this.user = Storage.getUser();
  }

  $onInit() {
    this.modulos = [];
    this.getTipoUnidades();
    // Lista general para tecnico y responsable
    console.log(this.user);

    if (this.Usuarios.esResponsableProyecto()) {
      this.getListaModulos();
    } else {
      this.obtenerAdjudicaciones();
      // this.getEquipoTecnico();
    }

    this.$scope.$watch('$ctrl.adjudicacionSeleccionada', (newValue, oldValue) => {
      if (angular.toJson(newValue) != angular.toJson(oldValue)) {
        this.getAdjudicacion(newValue.id_adjudicacion);
      }
    })
  }

  getListaModulos(modulo) {
    this.DataService.get(`modulos/proyectos/${this.proyecto.id_proyecto}`).then(response => {
      if (response && response.finalizado) {
        this.modulos = response.datos.rows;
        if (modulo && this.modulos.length) {
          for (let i in this.modulos) {
            if (this.modulos[i].id_modulo == modulo.id_modulo) {
              this.modulos[i].show = true;
            }
          }
        }
        // this.copiarItems();
        this.calcularTotal();
      }
    });
  }

  getListaModulosPorAdjudicacion(modulo) {
    this.DataService.get(`modulos/proyectos/${this.proyecto.id_proyecto}/${this.adjudicacion.id_adjudicacion}`).then(response => {
      if (response && response.finalizado) {
        this.modulos = response.datos.rows;
        if (modulo && this.modulos.length) {
          for (let i in this.modulos) {
            if (this.modulos[i].id_modulo == modulo.id_modulo) {
              this.modulos[i].show = true;
            }
          }
        }
        // this.copiarItems();
        this.calcularTotal();
      }
    });
  }

  getItems (modulo) {
    if (!modulo.show) {
      if (!modulo.items) {
        this.DataService.get(`items/modulo/${modulo.id_modulo}`).then(response => {
          if (response && response.finalizado) {
            modulo.items = response.datos.rows;
            this.calcularTotal();
          }
        });
      }
    }
    modulo.show = !modulo.show;
  }

  getTipoUnidades() {
    this.DataService.get(`parametricas/UNIDAD`).then(response => {
      if (response) {
        this.unidades = response.datos.rows;
      }
    });
  }

  // getEquipoTecnico() {
  //   this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/equipos_tecnicos/supervisor`).then(response => {
  //     if (response) {
  //       this.equipos_tecnicos = response.datos;
  //       this.obtenerAdjudicaciones();
  //     }
  //   });
  // }

  obtenerAdjudicaciones() {
    this.opcionesAdjudicaciones = [];
    // for (let eqTecnico in this.equipos_tecnicos) {
    // const equipo_tecnico = this.equipos_tecnicos[eqTecnico];
    // if (equipo_tecnico.fid_adjudicacion) {
    this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/adjudicaciones/supervision`).then(response => {
      if (response) {
        this.opcionesAdjudicaciones = response.datos;
        // this.adjudicacion = response.datos;
        // this.getListaModulosPorAdjudicacion();
      }
    });
    // }
    // }
  }

  getAdjudicacion(id_adjudicacion) {
    // if (this.equipo_tecnico && this.equipo_tecnico.fid_adjudicacion) {
    delete this.proyecto.adj;
    this.DataService.get(`proyectos/adjudicaciones/${id_adjudicacion}`).then(response => {
      if (response) {
        this.adjudicacion = response.datos;
        this.getListaModulosPorAdjudicacion();
        this.prepararValidacionesFecha();
        this.proyecto.adj = {
          id_adjudicacion: this.adjudicacion.id_adjudicacion,
          monto_supervision: this.adjudicacion.monto_supervision,
          orden_proceder: this.adjudicacion.orden_proceder,
          doc_especificaciones_tecnicas: this.adjudicacion.doc_especificaciones_tecnicas
        };
      }
    });
    // }
  }

  adicionarModulo() {
    if (this.adjudicacion) {
      const modulo = {
        fid_proyecto: this.proyecto.id_proyecto,
        referencia: this.adjudicacion.referencia,
        fid_adjudicacion: this.adjudicacion.id_adjudicacion
      };
      const modalModulo = this.mostrarModal('Nuevo módulo', 'fa-plus', modulo, modalModuloTemplate, modalModuloController);
      modalModulo.result.then(result => {
        if (result && result.datos) {
          this.modulos.push(result.datos);
        }
      });
    } else {
      this.Message.error(`Debe seleccionar una adjudicación antes de agregar un módulo`);
    }
  }

  editarModulo(modulo) {
    modulo.referencia = this.adjudicacion.referencia;
    modulo.componente = modulo.nombre.split('|')[0];
    modulo.modulo = modulo.nombre.split('|')[1];
    this.mostrarModal('Editar módulo', 'fa-edit', modulo, modalModuloTemplate, modalModuloController);
  }

  eliminarModulo(modulo) {
    let msg = `¿Está seguro/a de eliminar el módulo <strong>${modulo.nombre.split('|')[1]}</strong>${(modulo && modulo.items && modulo.items.length) ? (modulo.items.length === 1 ? ' y su único item?' : ' y sus ' + modulo.items.length + ' items?') : '?'}`;
    this.Modal.confirm(msg, () => {
      this.DataService.delete(`modulos/${modulo.id_modulo}`).then(result => {
        if (result) {
          this.modulos = this.eliminarElementoDeLista(this.modulos, modulo, 'id_modulo');
          this.Message.success(`El módulo "${modulo.nombre}" ha sido eliminado.`);
          this.$window.setTimeout(()=>{
            this.refrescarLista();
          }, 500);
        }
      });
    }, null, null, 'Si, estoy seguro/a');
  }

  calcularItem(item, index, idxModulo) {
    this.modulos[idxModulo].items[index].subtotal = this.Util.multiplicar([this.modulos[idxModulo].items[index].precio_unitario, this.modulos[idxModulo].items[index].cantidad]);
  }

  adicionarItem(modulo) {
    if (!modulo.items) {
      modulo.items = [];
    }
    for(let i in this.modulos) {
      for (let j in this.modulos[i].items) {
        if (!this.modulos[i].items[j].id_item || this.modulos[i].items[j].edicion) {
          this.Message.warning(`Debe guardar el item "${this.modulos[i].items[j].nombre||'Sin nombre'}"`);
          return;
        }
      }
    }
    modulo.items.push({edicion: true});
  }

  guardarItem(item, index, idxModulo, modulo) {
    if (!this.sonDatosValidosItem(modulo, index)) {
      return;
    }
    item.fid_modulo = modulo.id_modulo;
    this.DataService.post(`items`, item).then(response => {
      if (response) {
        this.Message.success(`Item "${item.nombre}" registrado correctamente.`);
        this.modulos[idxModulo].items[index] = response.datos;
        // this.refrescarLista(modulo);
        this.calcularTotal();
      }
    });
  }

  actualizarItem(item, index, modulo) {
    if (!this.sonDatosValidosItem(modulo, index)) {
      return;
    }
    this.DataService.put(`items/${item.id_item}`, item).then(response => {
      if (response) {
        this.Message.success(`Item "${item.nombre}" actualizado correctamente.`);
        item.edicion = false;
        // this.refrescarLista(modulo);
        this.calcularTotal();
      }
    });
  }

  eliminarItem(item, $index, modulo) {
    if (!item.id_item) {
      modulo.items.splice($index, 1);
      return;
    }
    this.DataService.delete(`items/${item.id_item}`).then(result => {
      if (result) {
        this.Message.success(`Item "${item.nombre}" eliminado correctamente.`);
        modulo.items = this.eliminarElementoDeLista(modulo.items, item, 'id_item');
        this.$window.setTimeout(() => {
          // this.refrescarLista(modulo);
          this.calcularTotal();
        }, 500);
      }
    });
  }

  eliminarElementoDeLista(lista, elemento, atributo) {
    return this._.reject(lista, function(item) {
      return item[atributo] === elemento[atributo];
    });
  }

  mostrarModal(titulo, icon, data, template, controller, labelOk, size) {
    return  this.Modal.show({
      template: template,
      controller: controller,
      data: data || {},
      title: titulo,
      icon: icon,
      size: size?size:'md',
      labelOk: labelOk
    });
  }

  guardarDatosModulo(modulo) {
    this.DataService.put(`modulos/${modulo.id_modulo}`, modulo).then(response => {
      if (response) {
        //modulo.items = response.datos.items;
        this.Message.success(`Los datos del módulo "${modulo.nombre}" se guardaron correctamente.`);
        this.refrescarLista(modulo);
      }
    });
  }

  sonDatosValidosItem(modulo, posicion) {
    const nombreForm = `form_${modulo.id_modulo}`;
    const FORM = angular.element(document.querySelector(`[name=${nombreForm}]`)).scope()[nombreForm];
    let esValido = (
      FORM['item_nombre_'+posicion].$valid &&
      FORM['item_unidad_'+posicion].$valid &&
      FORM['item_cantidad_'+posicion].$valid
    );

    if (!esValido) {
      FORM['item_nombre_'+posicion].$setTouched();
      FORM['item_unidad_'+posicion].$setTouched();
      FORM['item_cantidad_'+posicion].$setTouched();

      this.Message.warning('Verifique, corrija y/o llene los campos resaltados.');
      return false;
    }
    return true;
  }

  importarItems() {
    if (this.adjudicacion) {
      const confArchivo = {
        formatos: 'csv, xlsx, ods', //types
        tipos: '.csv,.xlsx,.ods', //mime-types
        cargarItems: true,
        mensajeInformacion: 'Puede importar items desde un <strong>archivo en formato .ods</strong>.'
      };
      this.mostrarModal('Importar Items', 'fa-upload', confArchivo, modalArchivoTemplate, modalArchivoController, 'Importar')
      .result.then(documento => {
        const items = {
          id_proyecto: this.proyecto.id_proyecto,
          id_adjudicacion: this.adjudicacion.id_adjudicacion,
          base64: documento.base64
        };
        this.DataService.post(`items/file`, items).then( result => {
          if (result.finalizado) {
            this.Message.success('Se importó correctamente los items.');
            // this.modulos = result.datos;
            // this.calcularTotal();
            this.$window.setTimeout(() => {
              this.getListaModulosPorAdjudicacion();
            }, 500);
          }
          if (result.erroresValidacion) {
            const modalModulo = this.mostrarModal('Observaciones en el archivo CSV', 'fa-check', result.erroresValidacion, modalValidacionTemplate, modalValidacionController, null, 'lg');
            modalModulo.result.then(result => { result; });
          }
        });
      });
    } else {
      this.Message.error(`Debe seleccionar una adjudicación antes de cargar items`);
    }
  }

  cargarEspecificacionesTecnicas() {
    if (this.adjudicacion) {
      const confArchivo = {
        formatos: 'pdf',
        tipos: '.pdf',
        mensajeInformacion: 'El archivo representará a las especificaciones técnicas de los items contenidos en la presente adjudicación.'
      };
      this.mostrarModal(`Especificaciones Técnicas`, 'fa-upload', confArchivo, modalArchivoTemplate, modalArchivoController, 'Cargar documento')
      .result.then( docEspecificaciones => {
        if (angular.isUndefined(docEspecificaciones)) {
          return;
        }
        let datos = {
          doc_especificaciones_tecnicas: docEspecificaciones,
          rol: this.user.rol.id_rol
        };
        this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/adjudicaciones/${this.adjudicacion.id_adjudicacion}/esp_tecnicas`, datos)
        .then(result => {
          if (result) {
            this.Message.success(`Se registró el documento de especificaciones técnicas correctamente.`);
            this.adjudicacion.doc_especificaciones_tecnicas = result.datos.doc_especificaciones_tecnicas;
            this.proyecto.adj.doc_especificaciones_tecnicas = result.datos.doc_especificaciones_tecnicas;
          }
        });
        // this.DataService.put(`proyectos/${this.proyecto.id_proyecto}`, {doc_especificaciones_tecnicas: docEspecificaciones, rol: this.user.rol.id_rol}).then(result => {
        //   if (result) {
        //     this.Message.success(`Se registró el documento de especificaciones técnicas correctamente.`);
        //     this.proyecto.doc_especificaciones_tecnicas = result.datos.doc_especificaciones_tecnicas;
        //   }
        // });
      });
    } else {
      this.Message.error('Debe seleccionar una adjudicación antes de cargar las especificaciones técnicas');
    }
  }

  verEspecificacionesTecnicas() {
    // this.Modal.showDocumentBase64(`proyectos/${this.proyecto.id_proyecto}/doc/ET`,
    // `Especificaciones Técnicas`);
    this.Modal.showDocumentBase64(`proyectos/${this.proyecto.id_proyecto}/doc/${this.adjudicacion.doc_especificaciones_tecnicas.substring(1, this.adjudicacion.doc_especificaciones_tecnicas.indexOf(']'))}`,`Especificaciones Técnicas`);
  }

  cargarEspecificacionesItem(item) {
    const confArchivo = {
      formatos: 'pdf',
      tipos: '.pdf'
    };
    this.mostrarModal(`Especificaciones Técnicas - "${item.nombre}"`, 'fa-upload', confArchivo, modalArchivoTemplate, modalArchivoController, 'Cargar documento')
    .result.then( docEspecificaciones => {
      if (angular.isUndefined(docEspecificaciones)) {
        return;
      }
      this.DataService.put(`items/${item.id_item}`, {especificaciones: docEspecificaciones}).then(result => {
        if (result && result.finalizado) {
          this.Message.success(`Se registró el documento de especificaciones técnicas del item "${item.nombre}" correctamente.`);
          item.especificaciones = docEspecificaciones;
        }
      });
    });
  }

  verEspecificacionesItem(item) {
    this.Modal.showDocumentBase64(`proyectos/${this.proyecto.id_proyecto}/doc/ET_${item.id_item}`, `Especificaciones Técnicas - "${item.nombre}"`);
  }

  refrescarLista(modulo) {
    this.getListaModulosPorAdjudicacion(modulo);
  }

  /* Registrar precios unitarios */
  // registrarPrecioItem(item) {
  //   if (!this.esPrecioActualizado(item) || angular.isUndefined(item.precio_unitario) || angular.isUndefined(item.id_item)) {
  //     return;
  //   }
  //   this.DataService.put(`items/${item.id_item}`, {precio_unitario: item.precio_unitario})
  //   .then( result => {
  //     if (result) {
  //       this.actualizarCopiaItems(result.datos);
  //       this.calcularTotal(item);
  //     }
  //   });
  // }

  copiarItems() {
    this.cModulos = angular.copy(this.modulos);
  }

  esPrecioActualizado(item) {
    // const cItem = this.cItems[this._.findIndex(this.cItems, {id_item: item.id_item})];
    // return cItem.precio_unitario !== item.precio_unitario;
    return item;
  }

  actualizarCopiaItems(item) {
    // this.cItems[this._.findIndex(this.cItems, {id_item: item.id_item})] = item;
    return item;
  }

  calcularTotal(item) {
    this.precioTotal = 0;
    /*this.modulos.map(modulo => {
      modulo.items.map(item => {
        if (item.cantidad && item.precio_unitario) {
          item.subtotal = this.Util.multiplicar([item.cantidad, item.precio_unitario]);
          this.precioTotal = this.Util.sumar([this.precioTotal, this.Util.multiplicar([item.cantidad, item.precio_unitario])])
        }
      });
    });  */

    for (let idxModulo in this.modulos) {
      let modulo = this.modulos[idxModulo];
      let totalModulo = 0;
      for (let j in modulo.items) {
        let item = modulo.items[j];
        item.subtotal = this.Util.multiplicar([item.precio_unitario, item.cantidad]);
        totalModulo = this.Util.sumar([item.subtotal, totalModulo]);
      }
      modulo.subtotal = modulo.items && modulo.items.length > 0 ? this.Util.redondear(totalModulo) : modulo.precio;
      // this.precioTotal = this.Util.sumar([this.precioTotal, totalModulo]);
      this.precioTotal = this.Util.sumar([this.precioTotal, modulo.subtotal]);
    }
    /*for(let idxModulo in this.modulos) {
      let totalModulo = 0;
      for (let index in this.modulos[idxModulo].items) {
        this.modulos[idxModulo].items[index].subtotal = this.Util.multiplicar([this.modulos[idxModulo].items[index].precio_unitario, this.modulos[idxModulo].items[index].cantidad]);
        totalModulo = this.Util.sumar(this.modulos[idxModulo].items[index].subtotal, totalModulo);
      }
      this.modulos[idxModulo].subtotal = this.Util.redondear(totalModulo);
      this.precioTotal = this.Util.sumar(this.precioTotal, totalModulo);
    }*/

    this.precioTotal = this.Util.redondear(this.precioTotal);
    if (this.esPresupuestoValido()) {
      // this.proyecto.monto_total = this.precioTotal;
      this.proyecto.montoItemsExcedido = false;
      if (this.proyecto.adj) {
        this.proyecto.adj.monto_supervision = this.precioTotal;
      }
    } else {
      // this.proyecto.monto_total = null;
      this.proyecto.montoItemsExcedido = true;
      this.Message.error('El precio total de los items no debe ser mayor al monto total adjudicado.');
      if (item) item.precio_unitario = '';
    }
  }

  esPresupuestoValido() {
    if (this.adjudicacion) {
      return this.precioTotal <= this.adjudicacion.monto;
    } else {
      return this.precioTotal <= this.proyecto.financiamiento.monto_total_proyecto;
    }
  }

  tieneSupervisiones(modulo) {
    if (this.proyecto && this.proyecto.supervisiones && this.proyecto.supervisiones.length > 0) {
      for (let i in this.proyecto.supervisiones) {
        if ((modulo && this.proyecto.supervisiones[i].fid_adjudicacion === modulo.fid_adjudicacion) || (this.adjudicacion && this.proyecto.supervisiones[i].fid_adjudicacion === this.adjudicacion.id_adjudicacion)) {
          return true;
        }
      }
    }
    return false;
  }

  prepararValidacionesFecha() {
    let fechaDesde, mensajeError = 'La fecha debe ser mayor a la fecha';
    // if (this.proyecto.orden_proceder) {
    //   let desde = this.proyecto.orden_proceder;
    //   desde = this.Datetime.getFormatDate(desde);
    //   desde = this.Datetime.subtractDays(desde, 1);
    //   desde = new Date(desde);
    //   let mes = desde.getMonth() + 1;
    //   fechaDesde = desde.getDate() + "-" + (mes <= 9 ? '0'+mes : mes) + "-" + desde.getFullYear();
    //   mensajeError = `La fecha debe ser mayor o igual a la fecha de orden de proceder del proyecto`;
    //   fechaDesde = this.Datetime.getFormatDate(fechaDesde);
    //   this.validacion = {
    //     fecha_minima: this.Datetime.addDaysToDate(angular.copy(fechaDesde), 1),
    //     mensaje_fecminima: `${mensajeError} (${this.Datetime.parseDate(this.Datetime.addDaysToDate(angular.copy(fechaDesde), 1), '/')})`
    //   };
    // } else
    {
      if (this.tieneBoletas()) {
        fechaDesde = this.fechaMasAntiguaDeBoletas();
        mensajeError = `${mensajeError} de inicio de la boleta de anticipo más antigua`;
      } else if (this.proyecto.tipo === 'TP_CIF' || this.proyecto.fecha_desembolso) {
        fechaDesde = this.proyecto.fecha_desembolso;
        mensajeError = `${mensajeError} del desembolso`;
      } else {
        fechaDesde = this.proyecto.convenio.fecha_suscripcion;
        mensajeError = `${mensajeError} del convenio`;
      }
      fechaDesde = this.Datetime.getFormatDate(fechaDesde);
      this.validacion = {
        fecha_minima: this.Datetime.addDaysToDate(angular.copy(fechaDesde), 1),
        mensaje_fecminima: `${mensajeError} (${this.Datetime.parseDate(fechaDesde, '/')})`
      };
    }
  }

  tieneBoletas() {
    return this.proyecto.boletas.length > 0;
  }

  fechaMasAntiguaDeBoletas() {
    return this._.sortBy(this.proyecto.boletas, (boleta) => {
      return boleta.fecha_inicio_validez;
    })[0].fecha_inicio_validez;
  }

  actualizarPesoItem(item) {
    if (!item.peso_ponderado) {
      item.peso_ponderado = 0;
    }
    if (item && item.id_item > 0 && !item.edicion) {
      this.DataService.put(`items/${item.id_item}`,{peso_ponderado: item.peso_ponderado}).then((result) => {
        if (result && result.finalizado) {
          this.Message.success(result.mensaje);
        } else {
          this.Message.warning('Sucedió un error al tratar de actualizar el item, intente de nuevo por favor');
        }
      });
    }
  }
}

export default ItemsController;
