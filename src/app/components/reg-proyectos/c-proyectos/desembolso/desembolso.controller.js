'use strict';

class DesembolsoController {
  constructor($timeout, Proyecto, Modal, Util, Datetime) {
    'ngInject';

    this.$timeout = $timeout;
    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this.Util = Util;
    this.Datetime = Datetime;
    this.monto_3 = 0;
    this.porcentaje_3 = 0;
  }

  $onInit() {
    this.archivo = {
      urlDocCertificacion: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/CIP` : '',
      urlDocCertificacionDescarga: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/CIP/descargar` : ''
    };
    let fechaConvenio = this.Datetime.getFormatDate(this.proyecto.convenio.fecha_suscripcion);
    this.validacion = {
      fecha_minima: this.Datetime.addDaysToDate(angular.copy(fechaConvenio), 0),
      mensaje_fecminima: `La fecha debe ser igual o mayor a la fecha del convenio (${this.Datetime.parseDate(fechaConvenio, '/')})`,
      fec_min_inscripcion: this.Datetime.getFormatDate(this.proyecto.convenio.fecha_suscripcion),
      mensaje_fecminima_desembolso1: `La fecha debe ser igual o mayor a la fecha de inscripción presupuestaria.`,
      mensaje_fecminima_desembolso2: `La fecha debe ser igual o mayor a la fecha del desembolso anterior.`,
    };

    if (!this.proyecto.cronograma_desembolsos) {
      if (this.proyecto.beneficiarios.productivo && this.proyecto.beneficiarios.productivo.maquinaria) {
        this.proyecto.cronograma_desembolsos = {
          porcentaje_1: 40, //50,
          porcentaje_2: 40, //30,
          porcentaje_3: 20, //20,
        }
      } else {
        this.proyecto.cronograma_desembolsos = {
          porcentaje_1: 40, //20,
          porcentaje_2: 40, //40,
          porcentaje_3: 20, //40,
        }
      }
    }
    if (this.proyecto.financiamiento.monto_total_proyecto) {
      this.proyecto.cronograma_desembolsos.monto_1 = this.Util.multiplicar([this.proyecto.financiamiento.monto_fdi,this.proyecto.cronograma_desembolsos.porcentaje_1/100]);
      this.proyecto.cronograma_desembolsos.monto_2 = this.Util.multiplicar([this.proyecto.financiamiento.monto_fdi,this.proyecto.cronograma_desembolsos.porcentaje_2/100]);
      this.proyecto.cronograma_desembolsos.monto_3 = this.Util.multiplicar([this.proyecto.financiamiento.monto_fdi,this.proyecto.cronograma_desembolsos.porcentaje_3/100]);
      this.monto_3 = this.proyecto.cronograma_desembolsos.monto_3;
      this.porcentaje_3 = this.proyecto.cronograma_desembolsos.porcentaje_3;
    }
  }

  recalcularPorcentaje() {
    this.proyecto.cronograma_desembolsos.porcentaje_3 = (this.proyecto.cronograma_desembolsos.monto_3 / this.proyecto.financiamiento.monto_fdi) * 100;
    this.proyecto.cronograma_desembolsos.porcentaje_4 = this.porcentaje_3 - this.proyecto.cronograma_desembolsos.porcentaje_3;
    this.proyecto.cronograma_desembolsos.monto_4 = this.monto_3 - this.proyecto.cronograma_desembolsos.monto_3;
  }

  selFechaDesembolso() {
    this.$timeout(() => {
      this.proyecto.fecha_desembolso = this.proyecto.cronograma_desembolsos.fecha_1;
    }, 5);
  }
}

export default DesembolsoController;
