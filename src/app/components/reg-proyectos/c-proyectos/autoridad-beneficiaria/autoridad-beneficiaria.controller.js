/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';
import modalRegistroController from './registro-gam.modal.controller';
import modalRegistroTemplate from './registro-gam.modal.html';

class AutoridadBeneficiariaController {
  constructor(Modal, DataService, Util, Proyecto, Message, Usuarios, $scope) {
    'ngInject';
    this.Modal = Modal;
    this.DataService = DataService;
    this.Util = Util;
    this.Proyecto = Proyecto;
    this.Message = Message;
    this.Usuarios = Usuarios;
    this.$scope = $scope;
  }

  $onInit() {
    if (this.Util.isEmpty(this.proyecto.autoridad_beneficiaria)) {
      this.proyecto.autoridad_beneficiaria = {};
      this.proyecto.cargo_autoridad_beneficiaria = 'Responsable GAM/GAIOC Aprobación Proyectos';
    }
    if (this.proyecto.autoridad_beneficiaria.estado === 'INACTIVO') {
      this.proyecto.autoridad_beneficiaria.noVerificar = true;
    }
    if (!this.proyecto.contactos) this.proyecto.contactos = {};
    if (!this.proyecto.contactos.datos) this.proyecto.contactos.datos = [];
    if (!this.proyecto.contactos.datos.length) {
      this.proyecto.contactos.datos.push({
        cargo: 'Técnico Encargado de la Elaboración del EDTP',
        persona: {}
      });
    } else {
      this.proyecto.contactos.datos[0].cargo = 'Técnico Encargado de la Elaboración del EDTP';
    }
    this.esTecnicoResponsable = this.Usuarios.esTecnicoResponsable();
    if (this.proyecto.fid_autoridad_beneficiaria && this.esTecnicoResponsable) {
      this.obtenerUsuarioGam();
    }

    this.$scope.$watch('$ctrl.usuarioGam.persona.correo', () => {
      if (this.usuarioGam && this.usuarioGam.persona && this.proyecto.autoridad_beneficiaria.correo !== this.usuarioGam.persona.correo) {
        this.proyecto.autoridad_beneficiaria.correo = this.usuarioGam.persona.correo;
      }
    });
  }

  agregarContacto() {
    if (!this.proyecto.contactos.datos) {
      this.proyecto.contactos.datos = [];
    }

    this.proyecto.contactos.datos.push({
      persona: {}
    });
  }

  quitarContacto(index) {
    if(this.proyecto.contactos.datos.length>1) {
      this.proyecto.contactos.datos.splice(index,1);
    }
  }

  verificar(data, index) {
    let persona = this.proyecto.autoridad_beneficiaria;
    if (persona && persona.nombres == data.nombres
        && persona.primer_apellido == data.primer_apellido
        && persona.segundo_apellido == data.segundo_apellido
        && persona.numero_documento == data.numero_documento
        && persona.fecha_nacimiento == data.fecha_nacimiento) {
      this.Message.warning('Ya existe una persona de contacto con los mismos nombres y número de documento.'+index);
      this.proyecto.contactos.datos[index] = {
        persona: {}
      };
      return;
    }
    for (let i in this.proyecto.contactos.datos) {
      persona = this.proyecto.contactos.datos[i].persona;
      if (i!=index && persona.nombres == data.nombres
          && persona.primer_apellido == data.primer_apellido
          && persona.segundo_apellido == data.segundo_apellido
          && persona.numero_documento == data.numero_documento
          && persona.complemento == data.complemento) {
        this.Message.warning('Ya existe una persona de contacto con los mismos nombres y número de documento.'+index);
        this.proyecto.contactos.datos[index] = {
          persona: {}
        };
        return;
      }
    }
  }

  puedeReenviarConfirmacion() {
    return this.esTecnicoResponsable && this.usuarioGam && this.proyecto.estado_proyecto !== 'CERRADO_FDI';
  }

  adicionarResponsableModal(callbackOk, callbackCancel) {
    let adicionMiembro = this.Modal.show({
      template: modalRegistroTemplate,
      controller: modalRegistroController,
      data: {
        registrar: !callbackOk
      },
      size: 'lg'
    });
    adicionMiembro.result.then( (result) => {
      if (result) {
        if (typeof(callbackOk) === 'function') {
          callbackOk(result);
        }
      } else {
        if (typeof(callbackCancel) === 'function') {
          callbackCancel();
        }
      }
    });
  }

  inactivarUsuarioResponsableGam(usuario) {
    this.Modal.confirm(`Esta seguro/a de inhabilitar al Responsable GAM/GAIOC?<br><br>Este usuario ya no podra ser habilitado nuevamente con el mismo cargo.<br><br>* Deberá agregar un nuevo usuario.`,
      () => {
        usuario.estado = 'INACTIVO';
        this.adicionarResponsableModal((persona) => {
          this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/contactos/${usuario.id_usuario}/reemplazar`, persona).then((resp) => {
            if (resp && resp.finalizado) {
              if (resp.datos && resp.datos.autoridad_beneficiaria) {
                const aux = resp.datos.autoridad_beneficiaria;
                this.proyecto.autoridad_beneficiaria = {
                  correo: aux.correo,
                  fecha_nacimiento: aux.fecha_nacimiento,
                  nombres: aux.nombres,
                  numero_documento: aux.numero_documento,
                  primer_apellido: aux.primer_apellido,
                  segundo_apellido: aux.segundo_apellido,
                  telefono: aux.telefono,
                  tipo_documento: aux.tipo_documento
                }
              }
              this.Message.success(resp.mensaje);
            } else {
              this.Message.warning('No se pudo realizar la inhabilitación.');
            }
          });
        }, () => {
          usuario.estado = 'ACTIVO';
        });
      }, null, 'Inhabilitar', 'Si estoy seguro/a', null, 'ban');
  }

  obtenerUsuarioGam() {
    this.DataService.get(`usuario/persona/${this.proyecto.fid_autoridad_beneficiaria}`).then( response => {
      if (response) {
        this.usuarioGam = response.datos;
        this.urlReconfirmacion = `proyectos/${this.proyecto.id_proyecto}/contactos/${this.usuarioGam.id_usuario}/notificar`;
      }
    });
  }
}

export default AutoridadBeneficiariaController;
