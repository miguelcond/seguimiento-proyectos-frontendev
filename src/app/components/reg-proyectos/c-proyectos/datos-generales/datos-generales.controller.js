'use strict';

class DatosGenerales {
  constructor(DataService, apiUrl, Util, Proyecto, Modal, Message, $state, Storage) {
    'ngInject';
    this.DataService = DataService;
    this.Util = Util;
    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this.Message = Message;
    this.user = Storage.getUser();
    this.apiUrl = apiUrl;
    this.$state = $state;
  }

  $onInit() {
    // Parámetros de descarga
    this.descarga = {
      urlDescarga: `${this.apiUrl}proyectos/${this.proyecto.id_proyecto}/doc/RIR/descargar`,
      nombreDescarga: `Informe Rechazo`
    };
    this.proyecto.cite_nro = this.proyecto.nro_convenio.substr(-9);
    this.proyecto.cite_codigo = this.proyecto.nro_convenio.replace(this.proyecto.cite_nro, '');
  }

  verDocumento(tipo, nombreDocumento) {
    this.Modal.showDocumentBase64(`proyectos/${this.proyecto.id_proyecto}/documentos/${tipo}`, nombreDocumento);
  }

  // Funcion temporal para corregir codigo y fecha de solicitud
  corregirDatosGenerales() {
    this.Modal.confirm('¿Esta seguro/a de modificar los datos? Deberá regenerar los documentos de evaluacion y/o informes.', ()=>{
      let datos = {
        id_proyecto: this.proyecto.id_proyecto,
        nro_convenio: `${this.proyecto.cite_codigo}${this.proyecto.cite_nro}`,
        fecha_suscripcion_convenio: this.proyecto.fecha_suscripcion_convenio
      }
      this.DataService.put(`proyectos/${this.proyecto.id_proyecto}`, datos).then((resp)=>{
        if (resp && resp.finalizado) {
          this.Message.success(resp.mensaje);
          this.$state.reload();
        }
      });
    }, null, 'Modificar datos', 'Si, estoy seguro/a');
  }

  modificarUbicacion() {
    let ubicacion_editable = this.Proyecto.esEditable('fcod_municipio')
    if(this.proyecto.estado_proyecto == 'REGISTRO_PROYECTO_FDI' && [13,18].indexOf(this.user.rol.id_rol)>=0){
      ubicacion_editable = true;
    }
    return this.modoVista || !ubicacion_editable;
  }

  validarCite() {
    let formato = new RegExp('^[0-9]{4}-[0-9]{4}$');
    return !formato.test(this.proyecto.cite_nro) || this.proyecto.cite_nro.substr(-4) < 2017;
  }

  puedeModificarCodigo(campo){
    return (this.proyecto.estado_proyecto == 'RECHAZADO_FDI' || this.Proyecto.esEditable(campo)) && this.proyecto.id_proyecto;
  }

}

export default DatosGenerales;
