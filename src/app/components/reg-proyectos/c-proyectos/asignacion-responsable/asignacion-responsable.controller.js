'use strict';

class AsignacionResponsable {
  constructor(DataService, Util, Proyecto) {
    'ngInject';

    this.DataService = DataService;
    this.Util = Util;
    this.Proyecto = Proyecto;
  }

  $onInit() {
    this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/usuarios`)
    .then(response => {
      if (response) {
        this.usuariosParaAsignar = [];
        for(let i in response.datos) {
          response.datos[i].nombre = `${response.datos[i].nombre}` + (response.datos[i].rol_nombre? ` (${response.datos[i].rol_nombre})`:``);
          this.usuariosParaAsignar.push(response.datos[i]);
          if (response.datos[i].id_usuario==this.proyecto.fid_usuario_asignado && response.datos[i].id_rol==this.proyecto.fid_usuario_asignado_rol) {
            this.usuario_asignado = this.usuariosParaAsignar[i];
          }
        }
      }
    });
  }

  seleccionado() {
    this.proyecto.fid_usuario_asignado = this.usuario_asignado.id_usuario || null;
    this.proyecto.fid_usuario_asignado_rol = this.usuario_asignado.id_rol || null;
  }

}

export default AsignacionResponsable;
