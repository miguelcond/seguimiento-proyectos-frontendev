/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class ArchivoAdjunto {
  constructor($scope, Util, Usuarios, Proyecto, Modal, DataService, Message) {
    'ngInject';
    this.$scope = $scope;
    this.Util = Util;
    this.Usuarios = Usuarios;
    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this.DataService = DataService;
    this.Message = Message;
    this.archivos = null;
  }

  $onInit() {
    this.uploadArchivoAdjunto = (file) => {
      if (!file.name) {
        this.Message.warning(`Debe escribir el título del archivo.`);
        return;
      }
      let datos = this.proyecto.archivo_adjunto.find((d)=>{return d.datos.titulo==file.name});
      if (!datos.file) {
        this.Message.warning(`No se puede crear subir un documento con titulo '${file.name}'. Puede ser que ya exista uno similar.`);
        return;
      }
      if (datos.datos.id_archivo_adjunto) {
        datos.p = 'change';
      } else {
        datos.p = 'save';
      }
      this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/archivo_adjunto`, datos).then(response => {
        if (response && response.finalizado) {
          datos.datos.id_archivo_adjunto = response.datos.id_archivo_adjunto;
          datos.datos.ruta = response.datos.ruta;
          datos.datos.nombre_original = response.datos.nombre_original;
        }
      });
    }

  }

  $onChanges(a) {
    if (!a.proyecto.previousValue && a.proyecto.currentValue.id_proyecto) {
      this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/archivo_adjunto`).then((resp)=>{
        if (resp.finalizado) {
          // Documentos adjuntos
          let documentos = [];
          for (let i in resp.datos.rows) {
            documentos.push({ datos: resp.datos.rows[i] });
          }
          this.proyecto.archivo_adjunto = documentos;
        }
      });
    }
  }

  /* RESPALDO TECNICO */
  modoEdicionDoc(archivo) {
    return archivo.datos.codigo_estado===this.proyecto.estado_actual.codigo;
  }

  verArchivoAdjunto(archivo) {
    if (archivo.file && archivo.file.base64) {
      this.Modal.showDocumentBase64(archivo.file.base64, 'Informe', 'strBase64');
      return;
    }
    this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/archivo_adjunto/${archivo.datos.id_archivo_adjunto}`).then((doc) => {
      if (doc && doc.finalizado) {
        archivo.file = doc.datos.file;
        this.Modal.showDocumentBase64(archivo.file.base64, 'Informe', 'strBase64');
      }
    });
  }

  cambiarArchivoAdjunto(archivo) {
    delete archivo.datos.ruta;
  }

  agregarArchivoAdjunto() {
    console.log('agregar', this.proyecto);
    if (!this.proyecto || !this.proyecto.id_proyecto) return;
    if (!this.proyecto.archivo_adjunto) this.proyecto.archivo_adjunto = [];
    this.proyecto.archivo_adjunto.push({
      datos: {
        titulo: null,
        descripcion: null,
        rol: {
          nombre: this.Usuarios.usuario.rol.nombre,
          descripcion: this.Usuarios.usuario.rol.descripcion,
        },
        codigo_estado: this.proyecto.estado_actual.codigo
      }
    });
  }

  eliminarArchivoAdjunto(archivo) {
    let eliminado = false;
    let i = 0;
    this.Modal.confirm(`¿Está seguro de eliminar por completo el archivo?`,
      () => {
        while (!eliminado && this.proyecto.archivo_adjunto[i]) {
          if (this.proyecto.archivo_adjunto[i].datos.titulo == archivo.datos.titulo) {
            archivo.datos.p = 'delete';
            this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/archivo_adjunto/${archivo.datos.id_archivo_adjunto}`, archivo.datos).then(doc => {
              if (doc && doc.finalizado) {
                this.proyecto.archivo_adjunto.splice(i,1);
              }
            });
            eliminado = true;
            break;
          }
          i++;
        }
      }, null, 'Eliminar archivo', 'Sí, estoy seguro/a');
  }

}

export default ArchivoAdjunto;
