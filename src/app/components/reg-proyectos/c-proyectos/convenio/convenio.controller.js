/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class DatosGenerales {
  constructor($scope, Util, Proyecto, Modal, _) {
    'ngInject';
    this.$scope = $scope;
    this.Util = Util;
    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this._ = _;
    this.archivos = null;
  }

  $onInit() {
    this.tipos_plazo_ampliado = [{
      codigo: 'Convenio modificatorio',
      nombre: 'Convenio modificatorio'
    },{
      codigo: 'Adenda al convenio',
      nombre: 'Adenda al convenio'
    }];
    //this.opciones_plazo_administrativo = [{codigo:240,nombre:'240 días (8 meses)'},{codigo:255,nombre:'255 días (8 1/2 meses)'}];
    if (!this.proyecto.convenio) this.proyecto.convenio = {};
    this.proyecto.convenio.plazo_tiempo_administrativo = this.proyecto.financiamiento.plazo_tiempo_administrativo.codigo;
    this.proyecto.convenio.plazo_convenio_ampliado = this.proyecto.convenio.plazo_convenio_ampliado || 0;

    this.$scope.$watch('$ctrl.proyecto.convenio.plazo_tiempo_administrativo', () => {
      if (this.proyecto.convenio.plazo_tiempo_administrativo) {
        this.proyecto.convenio.plazo_convenio = this.proyecto.plazo_ejecucion + this.proyecto.convenio.plazo_tiempo_administrativo;
      }
    });

    this.archivos = {
      urlDocConvenio: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/C` : '',
      urlDocConvenioDescarga: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/C/descargar` : '',
      urlDocConvenioExtendido: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT` : '',
      urlDocConvenioExtendidoDescarga: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT/descargar` : '',
      urlDocConvenioExtendido2: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT2` : '',
      urlDocConvenioExtendidoDescarga2: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT2/descargar` : '',
      urlDocResolucion: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/RM` : '',
      urlDocResolucionDescarga: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/RM/descargar` : ''
    };

    if (this.proyecto.orden_proceder) {
      this.plazo_convenio0 = new Date(this.proyecto.orden_proceder);
      this.plazo_convenio0.setDate(this.plazo_convenio0.getDate() + parseInt(this.proyecto.convenio.plazo_convenio))
      this.plazo_convenio0 = this.plazo_convenio0.toISOString();
    } else {
      this.plazo_convenio0 = null;
    }
    this.mostrar = true;
    this.recalcularPlazoConvenio1();
    this.recalcularPlazoConvenio2();
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.proyecto.monto_fdi', () => {
      if (!this.proyecto) {
        return;
      }
      this.calcularMontoConvenio();
    });
    this.$scope.$watch('$ctrl.proyecto.monto_contraparte', () => {
      if (!this.proyecto) {
        return;
      }
      this.calcularMontoConvenio();
    });
  }

  calcularMontoConvenio() {
    this.proyecto.monto_total_convenio = this.Util.redondear((this.proyecto.monto_fdi ? this.proyecto.monto_fdi : 0) + (this.proyecto.monto_contraparte ? this.proyecto.monto_contraparte : 0));
  }

  recalcularPlazoConvenio1() {
    if (this.plazo_convenio0) {
      this.plazo_convenio1 = new Date(this.plazo_convenio0);
      this.plazo_convenio1.setDate(this.plazo_convenio1.getDate() + parseInt(this.proyecto.convenio.plazo_convenio_ampliado))
      this.plazo_convenio1 = this.plazo_convenio1.toISOString();
    }
  }
  recalcularPlazoConvenio2() {
    if (this.plazo_convenio1 && this.proyecto.convenio.plazo_convenio_ampliado2>0) {
      this.plazo_convenio2 = new Date(this.plazo_convenio1);
      this.plazo_convenio2.setDate(this.plazo_convenio2.getDate() + parseInt(this.proyecto.convenio.plazo_convenio_ampliado2))
      this.plazo_convenio2 = this.plazo_convenio2.toISOString();
    }
  }
}

export default DatosGenerales;
