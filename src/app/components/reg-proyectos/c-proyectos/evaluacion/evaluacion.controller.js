/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

import fixFechaController from './fix-fecha.controller';
import fixFechaTemplate from './fix-fecha.html';

class DatosEvaluacion {
  constructor($scope, $timeout, Proyecto, DataService, Message, Modal, Storage) {
    'ngInject';
    this.$scope = $scope;
    this.$timeout = $timeout;
    this.Proyecto = Proyecto;
    this.DataService = DataService;
    this.Message = Message;
    this.Modal = Modal;
    this.user = Storage.getUser();
    this.esTecEval = [12,13].indexOf(this.user.rol.id_rol)>=0;
  }

  $onInit() {
    this.plazo_convenio = 0;
    this.archivos = {
      urlDocEvExterna: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/EVE` : '',
      urlDocEvExternaDescarga: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/EVE/descargar` : '',
    };

    this.$scope.$watch('$ctrl.proyecto.beneficiarios.tipo', () => {
      if (this.Proyecto.tratarComoProductivo(this.proyecto)) {
        if (!this.proyecto.evaluacion.datos || !this.proyecto.evaluacion.datos2 || !this.proyecto.evaluacion.datos3 || !this.proyecto.evaluacion.datos4 || !this.proyecto.evaluacion.datos5) {
          this.obtenerFormEvaluacion();
        }
      }
    });
    this.$scope.$watch('$ctrl.proyecto.beneficiarios.plazo_ejecucion', () => {
      let plazo_tiempo_administrativo = 0;
      if (this.proyecto.financiamiento && this.proyecto.financiamiento.plazo_tiempo_administrativo) plazo_tiempo_administrativo = this.proyecto.financiamiento.plazo_tiempo_administrativo.codigo;
      this.plazo_convenio = this.proyecto.beneficiarios.plazo_ejecucion + plazo_tiempo_administrativo;
    });
    // this.interval = setInterval(() => {
    //   const accionAutoguardado = {
    //     estado: 'REGISTRO_PROYECTO_FDI',
    //     guardar: true,
    //     label: 'Guardar'
    //   };
    //   this.$scope.$emit('ejecutarGuardado', accionAutoguardado);
    // }, 3000);
    this.esRechazado = this.proyecto.estado_proyecto === 'RECHAZADO_FDI';
  }

  // $onDestroy() {
  //   clearInterval(this.interval);
  // }

  $doCheck() {
    if (this.proyecto) {
      let datos;
      if (this.proyecto.evaluacion) {
        this.proyecto.evaluacion.nombre = this.proyecto.nombre;
        this.proyecto.evaluacion.financiamiento = angular.copy(this.proyecto.financiamiento);
        this.proyecto.evaluacion.beneficiarios = angular.copy(this.proyecto.beneficiarios);
        datos = this.proyecto.autoridad_beneficiaria;
        if (datos) {
          // this.proyecto.evaluacion.responsable_gam_gaioc = `${datos.nombres||''} ${datos.primer_apellido||''} ${datos.segundo_apellido||''} \n${this.proyecto.autoridad_beneficiaria.correo||''} - ${this.proyecto.autoridad_beneficiaria.telefono||''} - ${this.proyecto.fax_autoridad_beneficiaria||''}`;
          let contactoGam = '';
          contactoGam = `${datos.nombres || ''} ${datos.primer_apellido || ''} ${datos.segundo_apellido || ''}<br>`;
          if (this.proyecto.autoridad_beneficiaria.correo) contactoGam += `${this.proyecto.autoridad_beneficiaria.correo}`;
          if (this.proyecto.autoridad_beneficiaria.telefono) contactoGam += ` - ${this.proyecto.autoridad_beneficiaria.telefono}`;
          if (this.proyecto.fax_autoridad_beneficiaria) contactoGam += ` - ${this.proyecto.fax_autoridad_beneficiaria}`;
          this.proyecto.evaluacion.responsable_gam_gaioc = contactoGam;
        }
      }
      if (this.proyecto.contactos) {
        datos = this.proyecto.contactos.datos[0];
        if(datos && this.proyecto.evaluacion) {
          // this.proyecto.evaluacion.responsable_edtp = `${datos.persona.nombres||''} ${datos.persona.primer_apellido||''} ${datos.persona.segundo_apellido||''} \n${datos.correo||''} - ${datos.telefono||''} - ${datos.fax||''}`;
          let contactoEdtp = '';
          contactoEdtp = `${datos.persona.nombres || ''} ${ datos.persona.primer_apellido || ''} ${datos.persona.segundo_apellido || ''}<br>`;
          if (datos.correo) contactoEdtp += `${datos.correo}`;
          if (datos.telefono) contactoEdtp += ` - ${datos.telefono}`;
          if (datos.fax) contactoEdtp += ` - ${datos.fax}`;
          this.proyecto.evaluacion.responsable_edtp = contactoEdtp;
        }
      }
    }
  }

  regenerarFormulario() {
    this.obtenerFormEvaluacion();
  }

  initPlantilla() {
    this.$timeout(() => {
      if (this.proyecto) {
        if (!this.proyecto.evaluacion) {
          this.proyecto.evaluacion = {
            $_plantilla: 101,
            $_nombre: 'evaluacion',
            cod: this.proyecto.id_proyecto,
            nombre: this.proyecto.nombre,
            codigo: this.proyecto.codigo,
            municipio: angular.copy(this.proyecto.dpa.municipio),
            provincia: angular.copy(this.proyecto.dpa.provincia),
            departamento: angular.copy(this.proyecto.dpa.departamento),
            financiamiento: angular.copy(this.proyecto.financiamiento),
            beneficiarios: angular.copy(this.proyecto.beneficiarios),
          };
          if (this.Proyecto.tratarComoProductivo(this.proyecto)) {
            this.obtenerFormEvaluacion();
          }
        }
        this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/evaluaciones`).then((resp) => {
          if (resp && resp.finalizado) {
            this.documentos = resp.datos;
          }
        });
      }

      if (!this.proyecto.evaluacion.revisor) this.proyecto.evaluacion.revisor = {};
      //if (this.proyecto.evaluacion.revisor.nombre == undefined || this.proyecto.evaluacion.revisor.nombre.trim() == "" || this.proyecto.evaluacion.revisor.nombre !== (this.proyecto.revisor.persona.nombres+" "+this.proyecto.revisor.persona.primer_apellido+" "+this.proyecto.revisor.persona.segundo_apellido).trim()) {
      if (this.proyecto.revisor) {
        this.proyecto.evaluacion.revisor.nombre = (this.proyecto.revisor.persona.nombres+" "+this.proyecto.revisor.persona.primer_apellido+" "+this.proyecto.revisor.persona.segundo_apellido).trim();
        this.proyecto.evaluacion.revisor.cargo = this.proyecto.revisor.cargo;
      }
      //}

      if (!this.proyecto.evaluacion.jefe_area) this.proyecto.evaluacion.jefe_area = {};
      //if (this.proyecto.evaluacion.jefe_area.nombre == undefined || this.proyecto.evaluacion.jefe_area.nombre.trim() == "") {
      if (this.proyecto.jefe_area) {
        this.proyecto.evaluacion.jefe_area.nombre = (this.proyecto.jefe_area.persona.nombres+" "+this.proyecto.jefe_area.persona.primer_apellido+" "+this.proyecto.jefe_area.persona.segundo_apellido).trim();
        this.proyecto.evaluacion.jefe_area.cargo = this.proyecto.jefe_area.cargo;
      }
      //}

      if (!this.proyecto.evaluacion.jefe_depto) this.proyecto.evaluacion.jefe_depto = {};
      //if (this.proyecto.evaluacion.jefe_depto.nombre == undefined || this.proyecto.evaluacion.jefe_depto.nombre.trim() == "") {
      if (this.proyecto.jefe_depto) {
        this.proyecto.evaluacion.jefe_depto.nombre = (this.proyecto.jefe_depto.nombre).trim();
        this.proyecto.evaluacion.jefe_depto.cargo = this.proyecto.jefe_depto.cargo;
      }
      //}
    }, 500);
  }

  criteriosElegibilidad() {
    return (this.Proyecto.tratarComoProductivo(this.proyecto));
  }

  visibilidad (item) {
    let resultado = 0;
    for (let i in this.proyecto.evaluacion.datos3) {
      let fila = this.proyecto.evaluacion.datos3[i];
      let fila_requerida = false;

      if (this.Proyecto.tratarComoProductivo(this.proyecto)) fila_requerida = false;

      if (this.proyecto.beneficiarios.productivo) {
        if (this.proyecto.beneficiarios.productivo.hasOwnProperty(fila.key) && this.proyecto.beneficiarios.productivo[fila.key]) fila_requerida = true;
        /*if (fila.key == 'agricola' && this.proyecto.beneficiarios.productivo.agricola) fila_requerida = true;
        if (fila.key == 'pecuario' && this.proyecto.beneficiarios.productivo.pecuario) fila_requerida = true;
        if (fila.key == 'pesca' && this.proyecto.beneficiarios.productivo.pesca) fila_requerida = true;
        if (fila.key == 'piscicultura' && this.proyecto.beneficiarios.productivo.piscicultura) fila_requerida = true;
        if (fila.key == 'aprovecha_biodiversidad' && this.proyecto.beneficiarios.productivo.aprovecha_biodiversidad) fila_requerida = true;
        if (fila.key == 'integral_sustentable_bosque' && this.proyecto.beneficiarios.productivo.integral_sustentable_bosque) fila_requerida = true;
        if (fila.key == 'maquinaria' && this.proyecto.beneficiarios.productivo.maquinaria) fila_requerida = true;
        if (fila.key == 'prod_agroeco_organica' && this.proyecto.beneficiarios.productivo.prod_agroeco_organica) fila_requerida = true;
        if (fila.key == 'sis_agroforestales' && this.proyecto.beneficiarios.productivo.sis_agroforestales) fila_requerida = true;
        if (fila.key == 'infraestructura' && this.proyecto.beneficiarios.productivo.infraestructura) fila_requerida = true;
        if (fila.key == 'transformacion' && this.proyecto.beneficiarios.productivo.transformacion) fila_requerida = true;*/
      }

      for (let j in fila.cols) {
        if (!fila_requerida) {
          if (fila.cols[j].contenido && fila.cols[j].contenido.name == 'CUMPLE') {
            fila.cols[j].contenido.value = false;
          }
          if (fila.cols[j].contenido && fila.cols[j].contenido.name == 'NO_CUMPLE') {
            fila.cols[j].contenido.value = false;
          }
          if (fila.cols[j].contenido && fila.cols[j].contenido.name == 'NO_APLICA') {
            fila.cols[j].contenido.value = true;
          }
        }
      }
    }

    if (this.proyecto.beneficiarios.productivo) {
      if(this.proyecto.beneficiarios.productivo.hasOwnProperty(item) && this.proyecto.beneficiarios.productivo[item])
        resultado = this.proyecto.beneficiarios.productivo[item];
      /*switch (item) {
        case 'pecuario':
          resultado = this.proyecto.beneficiarios.productivo.pecuario;
          break;
        case 'agricola':
          resultado = this.proyecto.beneficiarios.productivo.agricola;
          break;
        case 'pesca':
          resultado = this.proyecto.beneficiarios.productivo.pesca;
          break;
        case 'piscicultura':
          resultado = this.proyecto.beneficiarios.productivo.piscicultura;
          break;
        case 'aprovecha_biodiversidad':
          resultado = this.proyecto.beneficiarios.productivo.aprovecha_biodiversidad;
          break;
        case 'integral_sustentable_bosque':
          resultado = this.proyecto.beneficiarios.productivo.integral_sustentable_bosque;
          break;
        case 'maquinaria':
          resultado = this.proyecto.beneficiarios.productivo.maquinaria;
          break;
        case 'prod_agroeco_organica':
          resultado = this.proyecto.beneficiarios.productivo.prod_agroeco_organica;
          break;
        case 'sis_agroforestales':
          resultado = this.proyecto.beneficiarios.productivo.sis_agroforestales;
          break;
        case 'infraestructura':
          resultado = this.proyecto.beneficiarios.productivo.infraestructura;
          break;
        case 'transformacion':
          resultado = this.proyecto.beneficiarios.productivo.transformacion;
          break;
        default:
        break;
      } */
      return resultado;
    }
  }

  visibilidadAnalisisFactibilidad (item) {
    let resultado = 1;
    if (this.proyecto.beneficiarios && this.proyecto.beneficiarios.tamanio_proyecto == 'OP_TP_MENORES') {
      for (let i in this.proyecto.evaluacion.datos4) {
        let fila = this.proyecto.evaluacion.datos4[i];
        if (fila.key == 'seccion_4_1' && item == fila.key) {
          resultado = 0;
        }
      }
    }
    return resultado;
  }

  radioChangeWithValidation(cols, index) {
    if (!this.modoVista) {
      if (!this.cumpleCriterios() && cols[index].contenido.name === 'APROBADO') {
        this.Message.error('Uno o mas criterios de evaluación están marcados como "no cumple". Corrija estos datos para poder aprobar el formulario');
      } else {
        this.radioChange(cols, index);
      }
    }
  }

  radioChange(cols, index) {
    if (!this.modoVista) {
      for (let i in cols) {
        if (cols[i].contenido && cols[i].contenido.type === 'radio') {
          cols[i].contenido.value = (i == index);
        }
      }
      if (cols[index].contenido.name === 'NO_CUMPLE') {
        this.marcarInformeRechazado();
      }
    }
  }

  radioColor(contenido) {
    if (contenido.value && contenido.name == 'CUMPLE') return 'btn-success';
    if (contenido.value && contenido.name == 'NO_CUMPLE') return 'btn-danger';
    if (contenido.value && contenido.name == 'NO_APLICA') return 'btn-primary';
    if (contenido.value && contenido.name == 'APROBADO') return 'btn-success';
    if (contenido.value && contenido.name == 'AJUSTE') return 'btn-warning';
    if (contenido.value && contenido.name == 'RECHAZO') return 'btn-danger';
    return 'btn-secondary';
  }

  marcarInformeRechazado() {
    for (let i in this.proyecto.evaluacion.datos5) {
      let fila = this.proyecto.evaluacion.datos5[i];
      for (let j in fila.cols) {
        if (fila.cols[j].contenido && fila.cols[j].contenido.type == 'radio') {
          fila.cols[j].contenido.value = fila.cols[j].contenido.name == 'RECHAZO' ? true : false;
        }
      }
    }
  }

  generarEvaluacionPdf() {
    for (let i in this.proyecto.evaluacion.datos3) {
      let fila = this.proyecto.evaluacion.datos3[i];
      let fila_requerida = false;

      if (this.Proyecto.tratarComoProductivo(this.proyecto)) fila_requerida = false;

      if (this.proyecto.beneficiarios.productivo) {
        if (this.proyecto.beneficiarios.productivo.hasOwnProperty(fila.key) && this.proyecto.beneficiarios.productivo[fila.key]) fila_requerida = true;
        /*if (fila.key=='agricola' && this.proyecto.beneficiarios.productivo.agricola) fila_requerida = true;
        if (fila.key=='pecuario' && this.proyecto.beneficiarios.productivo.pecuario) fila_requerida = true;
        if (fila.key=='pesca' && this.proyecto.beneficiarios.productivo.pesca) fila_requerida = true;
        if (fila.key=='piscicultura' && this.proyecto.beneficiarios.productivo.piscicultura) fila_requerida = true;
        if (fila.key=='aprovecha_biodiversidad' && this.proyecto.beneficiarios.productivo.aprovecha_biodiversidad) fila_requerida = true;
        if (fila.key=='integral_sustentable_bosque' && this.proyecto.beneficiarios.productivo.integral_sustentable_bosque) fila_requerida = true;
        if (fila.key=='maquinaria' && this.proyecto.beneficiarios.productivo.maquinaria) fila_requerida = true;
        if (fila.key=='prod_agroeco_organica' && this.proyecto.beneficiarios.productivo.prod_agroeco_organica) fila_requerida = true;
        if (fila.key=='sis_agroforestales' && this.proyecto.beneficiarios.productivo.sis_agroforestales) fila_requerida = true;*/
      }

      for (let j in fila.cols) {
        if (!fila_requerida) {
          if (fila.cols[j].contenido && fila.cols[j].contenido.name == 'CUMPLE') {
            fila.cols[j].contenido.value = false;
          }
          if (fila.cols[j].contenido && fila.cols[j].contenido.name == 'NO_CUMPLE') {
            fila.cols[j].contenido.value = false;
          }
          if (fila.cols[j].contenido && fila.cols[j].contenido.name == 'NO_APLICA') {
            fila.cols[j].contenido.value = true;
          }
        }
      }
    }

    this.Modal.confirm('¿Esta seguro/a de generar el formulario? Este se guardará como parte del historial de formularios de evaluación.',
      () => {
        let _datos = this.proyecto.evaluacion;
        _datos.nro_convenio = this.proyecto.nro_convenio;
        this.DataService.post(`proyectos/${this.proyecto.id_proyecto}/evaluaciones`, _datos).then((resp) => {
          if (resp && resp.finalizado) {
            this.Message.success(resp.mensaje);
            this.documentos.push(resp.datos);
          }
        });
      }, null, null, 'Si estoy seguro/a');
  }

  verEvaluacion(doc) {
    this.verDocumentoEvaluacion(doc);
  }

  verDocumentoEvaluacion(doc) {
    this.Modal.showFormularioBase64(`proyectos/${this.proyecto.id_proyecto}/doc/EVAL?codigo=${doc.codigo_documento}`,`Formulario de evaluación`);
  }

  corregirFecha(doc) {
    this.Modal.show({
      title: 'Modificar fecha!',
      controller: fixFechaController,
      template: fixFechaTemplate,
      data: doc
    }).result.then((resp)=>{
      let datos = {
        codigo_documento: resp.codigo_documento,
        fecha_documento: resp.fecha_documento,
      }
      this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/formulario/evaluacion/${resp.id_formulario}`, datos).then((resp)=>{
        if (resp && resp.finalizado) {
          doc.fecha_documento = resp.datos.fecha_documento;
          this.Message.success(resp.mensaje);
        }
      });
    }).catch(()=>{});
  }
  estado(doc) {
    this.Modal.confirm('¿Esta seguro/a de cambiar el estado?.', () => {
      let datos = {
        codigo_documento: doc.codigo_documento,
        estado: doc.estado==='INACTIVO'? 'ELABORADO': 'INACTIVO',
      }
      this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/formulario/evaluacion/${doc.id_formulario}`, datos).then((resp)=>{
        if (resp && resp.finalizado) {
          doc.estado = resp.datos.estado;
          this.Message.success(resp.mensaje);
        }
      });
    }, null, null, 'Si estoy seguro/a');
  }

  // TODO optimizar estas funciones
  limitarTexto(texto) {
    if (!texto) return '';
    let palabras = this.parsearTexto(texto);
    palabras = palabras.split(' ').slice(0, 5);
    return `${palabras.join(' ')}...`;
  }

  parsearTexto(texto) {
    if (!texto) return '';
    return texto.replace(/<b>|<\/b>|<li>|<ul>|<\/ul>/g,'').replace(/<\/li>/g,' \n');
  }

  cumpleCriterios() {
    let cumple = true;
    let datos = this.proyecto.evaluacion.datos.concat(this.proyecto.evaluacion.datos2, this.proyecto.evaluacion.datos4);
    for (let i in datos) {
      if (!(this.proyecto.beneficiarios && this.proyecto.beneficiarios.tamanio_proyecto == 'OP_TP_MENORES' && datos[i].key == 'seccion_4_1')) {
        if (datos[i].cols.find((col) => { return col.contenido && col.contenido.name == 'NO_CUMPLE' && col.contenido.value; })) {
          cumple = false;
          break;
        }
      }
    }

    if (cumple) {
      for (let i in this.proyecto.evaluacion.datos3) {
        let fila = this.proyecto.evaluacion.datos3[i];
        let fila_requerida = false;

        if (this.Proyecto.tratarComoProductivo(this.proyecto)) fila_requerida = false;

        if (this.proyecto.beneficiarios.productivo) {
          if (this.proyecto.beneficiarios.productivo.hasOwnProperty(fila.key) && this.proyecto.beneficiarios.productivo[fila.key]) fila_requerida = true;
          /*if (fila.key == 'agricola' && this.proyecto.beneficiarios.productivo.agricola) fila_requerida = true;
          if (fila.key == 'pecuario' && this.proyecto.beneficiarios.productivo.pecuario) fila_requerida = true;
          if (fila.key == 'pesca' && this.proyecto.beneficiarios.productivo.pesca) fila_requerida = true;
          if (fila.key == 'piscicultura' && this.proyecto.beneficiarios.productivo.piscicultura) fila_requerida = true;
          if (fila.key == 'aprovecha_biodiversidad' && this.proyecto.beneficiarios.productivo.aprovecha_biodiversidad) fila_requerida = true;
          if (fila.key == 'integral_sustentable_bosque' && this.proyecto.beneficiarios.productivo.integral_sustentable_bosque) fila_requerida = true;
          if (fila.key == 'maquinaria' && this.proyecto.beneficiarios.productivo.maquinaria) fila_requerida = true;
          if (fila.key == 'prod_agroeco_organica' && this.proyecto.beneficiarios.productivo.prod_agroeco_organica) fila_requerida = true;
          if (fila.key == 'sis_agroforestales' && this.proyecto.beneficiarios.productivo.sis_agroforestales) fila_requerida = true;*/
        }

        if (fila_requerida) {
          if (fila.cols.find((col) => { return col.contenido && col.contenido.name == 'NO_CUMPLE' && col.contenido.value; })) {
            cumple = false;
            break;
          }
        }
      }
    }

    // if (cumple) {
    //   for (let i in this.proyecto.evaluacion.datos2) {
    //     if (cumple) {
    //       let fila = this.proyecto.evaluacion.datos2[i];
    //       for (let j in fila.cols) {
    //         if (fila.cols[j].contenido && fila.cols[j].contenido.name == 'NO_CUMPLE' && fila.cols[j].contenido.value) {
    //           cumple = false;
    //         }
    //       }
    //     }
    //   }
    // }
    // if (cumple) {
    //   for (let i in this.proyecto.evaluacion.datos3) {
    //     if (cumple) {
    //       let fila = this.proyecto.evaluacion.datos3[i];
    //       let fila_requerida = false;

    //       if(this.proyecto.beneficiarios.tipo == 'OP_TP_PRODUCTIVO') fila_requerida = false;

    //       if(this.proyecto.beneficiarios.productivo){
    //         if(fila.key=='agricola' && this.proyecto.beneficiarios.productivo.agricola) fila_requerida = true;
    //         if(fila.key=='pecuario' && this.proyecto.beneficiarios.productivo.pecuario) fila_requerida = true;
    //         if(fila.key=='pesca' && this.proyecto.beneficiarios.productivo.pesca) fila_requerida = true;
    //         if(fila.key=='piscicultura' && this.proyecto.beneficiarios.productivo.piscicultura) fila_requerida = true;
    //         if(fila.key=='aprovecha_biodiversidad' && this.proyecto.beneficiarios.productivo.aprovecha_biodiversidad) fila_requerida = true;
    //         if(fila.key=='integral_sustentable_bosque' && this.proyecto.beneficiarios.productivo.integral_sustentable_bosque) fila_requerida = true;
    //         if(fila.key=='maquinaria' && this.proyecto.beneficiarios.productivo.maquinaria) fila_requerida = true;
    //         if(fila.key=='prod_agroeco_organica' && this.proyecto.beneficiarios.productivo.prod_agroeco_organica) fila_requerida = true;
    //         if(fila.key=='sis_agroforestales' && this.proyecto.beneficiarios.productivo.sis_agroforestales) fila_requerida = true;
    //       }

    //       for (let j in fila.cols) {
    //         if (fila_requerida) {
    //           if (fila.cols[j].contenido && fila.cols[j].contenido.name == 'NO_CUMPLE' && fila.cols[j].contenido.value) {
    //             cumple = false;
    //           }
    //         }
    //       }
    //     }
    //   }
    // }
    // if (cumple) {
    //   for (let i in this.proyecto.evaluacion.datos4) {
    //     if (cumple) {
    //       let fila = this.proyecto.evaluacion.datos4[i];
    //       for (let j in fila.cols) {
    //         if (fila.cols[j].contenido && fila.cols[j].contenido.name == 'NO_CUMPLE' && fila.cols[j].contenido.value) {
    //           cumple = false;
    //         }
    //       }
    //     }
    //   }
    // }
    return cumple;
  }

  obtenerFormEvaluacion() {
    this.proyecto.evaluacion.datos = [];
    this.proyecto.evaluacion.datos2 = [];
    this.proyecto.evaluacion.datos3 = [];
    this.proyecto.evaluacion.datos4 = [];
    this.proyecto.evaluacion.datos5 = [];
    this.DataService.get(`contenidos/evaluacion`).then(response => {
      if (response) {
        for (let row in response.datos) {
          let a = response.datos[row];
          if (a.seccion === "1" && a.estructura) {
            this.proyecto.evaluacion.datos = a.estructura;
          } else if (a.seccion === "2" && a.estructura) {
            this.proyecto.evaluacion.datos2 = a.estructura;
          } else if (a.seccion === "3" && a.estructura) {
            this.proyecto.evaluacion.datos3 = a.estructura;
          } else if (a.seccion === "4" && a.estructura) {
            this.proyecto.evaluacion.datos4 = a.estructura;
            // if (this.proyecto.beneficiarios.tamanio_proyecto == 'OP_TP_MENORES') {
            //   this.proyecto.evaluacion.datos4[0].cols[5].contenido.value = false;
            //   this.proyecto.evaluacion.datos4[0].cols[6].contenido.value = true;
            //   this.proyecto.evaluacion.datos4[1].cols[3].contenido.value = false;
            //   this.proyecto.evaluacion.datos4[1].cols[4].contenido.value = true;
            // }
          } else if (a.seccion === "5" && a.estructura) {
            this.proyecto.evaluacion.datos5 = a.estructura;
          }
        }
      }
    });
  }
}

export default DatosEvaluacion;
