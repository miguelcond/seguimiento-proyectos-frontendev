'use strict';

class FixFecha {
  constructor($uibModalInstance, data, eventOk, eventCancel) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.doc = angular.copy(data);
    this.eventOk = eventOk;
    this.eventCancel = eventCancel;
    this.cancel = true;
  }

  $onInit() {
  }

  ok() {
    this.$uibModalInstance.close(this.doc);
    if (typeof this.eventOk == 'function') {
      this.eventOk();
    }
  }

  close() {
    this.$uibModalInstance.dismiss('cancel');
    if (typeof this.eventCancel == 'function') {
      this.eventCancel();
    }
    this.$uibModalInstance.result.then(()=>{},()=>{});
  }
}

export default FixFecha;
