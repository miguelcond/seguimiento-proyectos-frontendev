'use strict';

import controller from './datos-generales-resumen.controller';
import template from './datos-generales-resumen.html';

const DatosGenerales = {
  bindings:{
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default DatosGenerales;
