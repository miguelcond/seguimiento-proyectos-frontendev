/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class DatosGenerales {
  constructor(DataService, Util, Proyecto, Modal, $scope, urm2lat, Usuarios) {
    'ngInject';
    this.DataService = DataService;
    this.Util = Util;
    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this.$scope = $scope;
    this.urm2lat = urm2lat;
    this.Usuarios = Usuarios;
  }

  $onInit() {
    this.lista_comunidades = "";
    this.ver_comunidades = true;
    this.$$iniciarLeaflet();
    this.sectorProductivo = [];
    this.seleccion_unidades = [];
    this.subtipos = 'inicio';

    this.$scope.$watch('$ctrl.proyecto.beneficiarios.tipo', () => {
      // if (this.proyecto.beneficiarios.tipo != 'OP_TP_PRODUCTIVO' && this.proyecto.beneficiarios.tipo != 'OP_TP_MAQUINARIA') {
      if (!this.Proyecto.tratarComoProductivo(this.proyecto)) {
        this.seleccion_unidades = [];
        this.limpiarMatriz();
      }
    });

    if (typeof this.proyecto.beneficiarios.indicadores_impacto === 'string') {
      this.proyecto.beneficiarios.indicadores_impacto = [
        {
          combinado: this.proyecto.beneficiarios.indicadores_impacto,
          numero: this.proyecto.beneficiarios.indicadores_impacto_numero,
          unidades: this.proyecto.beneficiarios.indicadores_impacto_unidades,
          otro: this.proyecto.beneficiarios.indicadores_impacto_otro
        }
      ];
    }

    this.$scope.$watch('$ctrl.proyecto.beneficiarios.subtipo_productivo', () => {
      switch (this.proyecto.beneficiarios.subtipo_productivo) {
        case 'ST_PROD_AGRICOLA':
        case 'ST_PROD_PISCICOLA':
        case 'ST_PROD_APICULTURA':
          this.seleccion_unidades = [{codigo:'t',nombre:'t'}];
          break;
        case 'ST_PROD_MAQUINARIA':
          this.seleccion_unidades = [{codigo:'HA',nombre:'HA'}];//otros
        break;
        case 'ST_PROD_BOVINO':
        case 'ST_PROD_CAMELIDOS':
        case 'ST_PROD_GANADO_MENOR':
          this.seleccion_unidades = [{codigo:'cabezas',nombre:'cabezas'}];
        break;
        case 'ST_PROD_TRANSFORMACION':
          //this.seleccion_unidades = [{codigo:'T',nombre:'T'},{codigo:'l',nombre:'l'},{codigo:'Otro',nombre:'Otro'}];
          this.seleccion_unidades = [{codigo:'t',nombre:'t'},{codigo:'l',nombre:'l'}];//otros
        break;
        case 'ST_PROD_AVICOLA':
          this.seleccion_unidades = [{codigo:'aves',nombre:'aves'}];
        break;
        case 'ST_PROD_INFRAESTRUCTURA':
          this.seleccion_unidades = [{codigo:'unidades',nombre:'unidades'}];
        break;
        case 'ST_PROD_LACTEOS':
          this.seleccion_unidades = [{codigo:'l',nombre:'l'}];
        break;
        default:
        break;
      }
      this.seleccion_unidades.push({codigo:'Otro',nombre:'Otro'});

      // if (this.seleccion_unidades.length === 1 && this.proyecto.beneficiarios.tipo === 'OP_TP_PRODUCTIVO') {
      //   this.proyecto.beneficiarios.indicadores_impacto_unidades = [];
      //   this.proyecto.beneficiarios.indicadores_impacto_unidades.push(this.seleccion_unidades[0].codigo);
      // }
      // if (this.seleccion_unidades.length > 1 && this.proyecto.beneficiarios.tipo === 'OP_TP_PRODUCTIVO') {
      //   this.proyecto.beneficiarios.indicadores_impacto_unidades = [];
      //   this.proyecto.beneficiarios.indicadores_impacto_unidades.push(this.seleccion_unidades[0]);
      // }

      // NUEVO CUADRO SEGUN SUBTIPO
      if (this.proyecto.beneficiarios.subtipo_productivo) {
        this.DataService.get(`contenidos/${this.proyecto.beneficiarios.subtipo_productivo}/1`).then(response => {
          if (response) {
            if (response.datos && response.datos.estructura) {
              if (this.subtipos != 'inicio') {
                this.limpiarMatriz();
              } else {
                this.subtipos = '';
              }

              this.sectorProductivo = response.datos.estructura.reduce((a, b) => {
                let temp = b;
                if (temp.templateOptions) {
                  temp.templateOptions.disabled = !this.editarBeneficiarios(); // Habilitar o deshabilitar los campos del formulario
                }
                a.push(temp);
                return a;
              }, []);

              // Si solo existe una opcion en la matriz, esta es seleccionada por defecto
              if (this.sectorProductivo.length === 1 && this.sectorProductivo[0].hasOwnProperty('key')) {
                this.proyecto.beneficiarios.productivo[this.sectorProductivo[0].key] = true;
              }
            }
          }
        });
      }
    });

    // this.$scope.$watch('$ctrl.proyecto.beneficiarios.indicadores_impacto_unidades', () => {
    //   this.combinarIndicadoresImpacto();
    // });

    /*this.DataService.get(`contenidos/sector_productivo/1`).then(response => {
      if (response) {
        if (response.datos && response.datos.estructura) {
          this.sectorProductivo = response.datos.estructura.reduce((a, b) => {
            let temp = b;
            if (temp.templateOptions) {
              temp.templateOptions.disabled = !this.editarBeneficiarios(); // Habilitar o deshabilitar los campos del formulario
            }
            a.push(temp);
            return a;
          }, []);
        }
      }
    });*/

    // this.sectorProductivo = [
    //   {
    //     key: 'agricola',
    //     type: 'checkbox',
    //     className: 'col-md-6 col-lg-4',
    //     templateOptions: {
    //       label: 'Agrícola',
    //       disabled: !this.editarBeneficiarios()
    //     }
    //   },
    //   {
    //     key: 'prod_agroeco_organica',
    //     type: 'checkbox',
    //     className: 'col-md-6 col-lg-4',
    //     templateOptions: {
    //       label: 'Producción Agroecológica y Orgánica',
    //       disabled: !this.editarBeneficiarios()
    //     }
    //   },
    //   {
    //     key: 'pecuario',
    //     type: 'checkbox',
    //     className: 'col-md-6 col-lg-4',
    //     templateOptions: {
    //       label: 'Pecuario',
    //       disabled: !this.editarBeneficiarios()
    //     }
    //   },
    //   {
    //     key: 'pesca',
    //     type: 'checkbox',
    //     className: 'col-md-6 col-lg-4',
    //     templateOptions: {
    //       label: 'Pesca',
    //       disabled: !this.editarBeneficiarios()
    //     }
    //   },
    //   {
    //     key: 'integral_sustentable_bosque',
    //     type: 'checkbox',
    //     className: 'col-md-6 col-lg-4',
    //     templateOptions: {
    //       label: 'Manejo Integral y Sustentable de Bosques',
    //       disabled: !this.editarBeneficiarios()
    //     }
    //   },
    //   {
    //     key: 'sis_agroforestales',
    //     type: 'checkbox',
    //     className: 'col-md-6 col-lg-4',
    //     templateOptions: {
    //       label: 'Manejo en Sistemas agroforestales',
    //       disabled: !this.editarBeneficiarios()
    //     }
    //   },
    //   {
    //     key: 'maquinaria',
    //     type: 'checkbox',
    //     className: 'col-md-6 col-lg-4',
    //     templateOptions: {
    //       label: 'Entrega Maquinaria',
    //       disabled: !this.editarBeneficiarios()
    //     }
    //   },
    //   {
    //     key: 'piscicultura',
    //     type: 'checkbox',
    //     className: 'col-md-6 col-lg-4',
    //     templateOptions: {
    //       label: 'Piscicultura',
    //       disabled: !this.editarBeneficiarios()
    //     }
    //   },
    //   {
    //     key: 'aprovecha_biodiversidad',
    //     type: 'checkbox',
    //     className: 'col-md-6 col-lg-4',
    //     templateOptions: {
    //       label: 'Aprovechamiento sustentable de la biodiversidad',
    //       disabled: !this.editarBeneficiarios()
    //     }
    //   },
    // ];

    if (this.proyecto.dpa && this.proyecto.dpa.municipio && this.proyecto.dpa.municipio.point && this.proyecto.dpa.municipio.point.coordinates) {
      this.mapa.centro = {
        lat: this.proyecto.dpa.municipio.point.coordinates[1],
        lng: this.proyecto.dpa.municipio.point.coordinates[0],
        zoom: 9,
      }
    } else {
      this.mapa.centro = {
        lat: -16.5286043,
        lng: -68.1798767,
        zoom: 8,
      }
    }
    this.mapa.marcadores = [];
    if (this.proyecto.beneficiarios && this.proyecto.beneficiarios.comunidades && this.proyecto.beneficiarios.comunidades.length > 0) {
      for (let i in this.proyecto.beneficiarios.comunidades) {
        let icon = {
            iconUrl: 'images/marker.png',
            iconSize: [38, 40]
          };
        this.mapa.marcadores.push({
          lat: this.proyecto.beneficiarios.comunidades[i].coordenadas.lat,
          lng: this.proyecto.beneficiarios.comunidades[i].coordenadas.lng,
          focus: false,
          draggable: !this.noEditarCoordenadas(),
          message: '-'+this.proyecto.beneficiarios.comunidades[i].nombre,
          icon: icon
        });
      }
    }

    this.$scope.$on('leafletDirectiveMarker.dragend', (event, args) => {
      this.mapa.marcadores[args.modelName].lat = args.model.lat;
      this.mapa.marcadores[args.modelName].lng = args.model.lng;
      this.proyecto.beneficiarios.comunidades[args.modelName].coordenadas.lat = args.model.lat;
      this.proyecto.beneficiarios.comunidades[args.modelName].coordenadas.lng = args.model.lng;

      this.proyecto.beneficiarios.comunidades[args.modelName].coordenadas.este_x = '';
      this.proyecto.beneficiarios.comunidades[args.modelName].coordenadas.norte_y = '';
      this.proyecto.beneficiarios.comunidades[args.modelName].coordenadas.zona = '';
    });
  }

  agregarComunidades() {
    this.ver_comunidades = !this.ver_comunidades;
  }

  limpiarListaComunidades() {
    this.lista_comunidades = "";
  }

  agregarComunidadesImportar() {
    var filas = (this.lista_comunidades.split(/\n/g));
    if (filas.length > 0) {
      for (let i = 0; i < filas.length; i++) {
        if (filas[i].trim() != "") {
          var columnas = filas[i].split(/\t/g);//"|"
          if (columnas.length >= 3) {
            console.log(columnas);
            if (columnas.length > 3 && columnas[3].trim() != "") {
              var latlon = new Array(2);
              this.urm2lat.UTMXYToLatLon(parseFloat(columnas[2]),parseFloat(columnas[3]),parseInt(columnas[1]),true,latlon);
              if (latlon.length > 0) {
                this.agregarComunidadManual(columnas[0].trim(),parseFloat(latlon[0]),parseFloat(latlon[1]));
              }
            } else {
              this.agregarComunidadManual(columnas[0].trim(),parseFloat(columnas[1]),parseFloat(columnas[2]));
            }
          }
        }
      }
    }
    this.ver_comunidades = !this.ver_comunidades;
  }

  agregarComunidadManual(_nombre,_lat,_lng) {
    if (!this.proyecto.beneficiarios) {
      this.proyecto.beneficiarios = {};
    }
    if (!this.proyecto.beneficiarios.comunidades) {
      this.proyecto.beneficiarios.comunidades = [];
    }
    let comunidad = {
      nro: this.proyecto.beneficiarios.nro_comunidades? this.proyecto.beneficiarios.nro_comunidades + 1: 1,
      nombre: _nombre,
      coordenadas: {
        lat: _lat,
        lng: _lng
      }
    };
    this.proyecto.beneficiarios.comunidades.push(comunidad);
    this.proyecto.beneficiarios.nro_comunidades = this.proyecto.beneficiarios.comunidades.length;

    let icon = {
        iconUrl: 'images/marker.png',
        iconSize: [38, 40],
        iconAnchor: [19, 30]
      };
    this.mapa.marcadores.push({
      lat: _lat,
      lng: _lng,
      focus: false,
      draggable: true,
      message: _nombre,
      icon: icon
    });
  }

  agregarComunidad() {
    if (!this.proyecto.beneficiarios) {
      this.proyecto.beneficiarios = {};
    }
    if (!this.proyecto.beneficiarios.comunidades) {
      this.proyecto.beneficiarios.comunidades = [];
    }
    let comunidad = {
      nro: this.proyecto.beneficiarios.nro_comunidades ? this.proyecto.beneficiarios.nro_comunidades + 1 : 1,
      nombre: '',
      coordenadas: {
        lat: this.mapa.centro.lat,
        lng: this.mapa.centro.lng
      }
    };
    this.proyecto.beneficiarios.comunidades.push(comunidad);
    this.proyecto.beneficiarios.nro_comunidades = this.proyecto.beneficiarios.comunidades.length;

    let icon = {
        iconUrl: 'images/marker.png',
        iconSize: [38, 40],
        iconAnchor: [19, 30]
      };
    this.mapa.marcadores.push({
      lat: this.mapa.centro.lat,
      lng: this.mapa.centro.lng,
      focus: false,
      draggable: true,
      message: '-',
      icon: icon
    });
  }

  quitarComunidad(index) {
    if (this.proyecto.beneficiarios.comunidades && this.proyecto.beneficiarios.comunidades.length > 0) {
      this.proyecto.beneficiarios.comunidades.splice(index, 1);
      this.proyecto.beneficiarios.nro_comunidades = this.proyecto.beneficiarios.comunidades.length;
      this.mapa.marcadores.splice(index, 1);
    }
  }

  renombrarComunidad(index, comunidad) {
    this.mapa.marcadores[index].message = '' + comunidad.nombre;
  }

  coordenadaManual(index, comunidad) {
    if (comunidad.coordenadas.zona && comunidad.coordenadas.zona != '' && comunidad.coordenadas.este_x && comunidad.coordenadas.este_x != 0 && comunidad.coordenadas.norte_y && comunidad.coordenadas.norte_y != 0) {
      var latlon = new Array(2);
      this.urm2lat.UTMXYToLatLon(parseFloat(comunidad.coordenadas.este_x),parseFloat(comunidad.coordenadas.norte_y),parseInt(comunidad.coordenadas.zona),true,latlon);
      if (latlon.length > 0) {
        comunidad.coordenadas.lat = this.mapa.marcadores[index].lat = parseFloat(latlon[0]);
        comunidad.coordenadas.lng = this.mapa.marcadores[index].lng = parseFloat(latlon[1]);
      }
    } else {
      comunidad.coordenadas.lat = this.mapa.marcadores[index].lat = parseFloat(comunidad.coordenadas.lat);
      comunidad.coordenadas.lng = this.mapa.marcadores[index].lng = parseFloat(comunidad.coordenadas.lng);
    }
  }

  plazoEjecucion() {
    this.proyecto.plazo_ejecucion = this.proyecto.beneficiarios.plazo_ejecucion;
  }

  verDocumento(tipo, nombreDocumento) {
    this.Modal.showDocumentBase64(`proyectos/${this.proyecto.id_proyecto}/documentos/${tipo}`, nombreDocumento);
  }

  selTipoFinanciamiento(item) {
    if (item.indexOf('PUBLICA') >= 0 || item.indexOf('PRIVADA') >= 0) {
      this.$$obtenerCuadro(1);
      // this.$$obtenerCuadro1();
      // this.proyecto.financiamiento.cuadro.tipo = 1;
    } else if (item.indexOf('MIXTA') >= 0) {
      this.$$obtenerCuadro(2);
      // this.$$obtenerCuadro2();
      // this.proyecto.financiamiento.cuadro.tipo = 2;
    }
  }

  $$obtenerCuadro(tipo) {
    // this.proyecto.financiamiento.cuadro = {};
    this.DataService.get(`contenidos/cuadro_financiamiento/${tipo}`).then(response => {
      if (response) {
        if (response.datos && response.datos.estructura) {
          this.proyecto.financiamiento.cuadro = response.datos.estructura;
          // console.log('-------------cuadro--------------');
          // console.log(this.proyecto.financiamiento.cuadro);
          // console.log('---------------------------------');
        }
      }
    });
  }

  // $$obtenerCuadro1() {
  //   // this.proyecto.financiamiento.cuadro = {};
  //   this.DataService.get(`contenidos/cuadro_financiamiento/1`).then(response => {
  //     if (response) {
  //       if (response.datos && response.datos.estructura) {
  //         this.proyecto.financiamiento.cuadro = response.datos.estructura;
  //         // console.log('-------------cuadro--------------');
  //         // console.log(this.proyecto.financiamiento.cuadro);
  //         // console.log('---------------------------------');
  //       }
  //     }
  //   });

    // this.proyecto.financiamiento.cuadro = {
    //   datos: [
    //     {
    //       cols: [
    //         {
    //           rowspan: 4,
    //           contenido: `Inversión fija`,
    //         },
    //         { contenido: `Infraestructura productiva` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'costo_total', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'fdi' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'beneficiarios' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'otros' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Bienes y/o equipamiento` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'costo_total', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'fdi' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'beneficiarios' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'otros' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Medidas Físicas (infraestructura) de prevención y/o mitigación de impactos ambientales` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'costo_total', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'fdi' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'beneficiarios' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'otros' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Medidas Físicas (infraestructura) de gestión de riesgos` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'costo_total', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'fdi' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'beneficiarios' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'otros' } },
    //       ]
    //     },

    //     //*******
    //     {
    //       cols: [
    //         {
    //           rowspan: 3,
    //           contenido: `Capital de trabajo`,
    //         },
    //         { contenido: `Mano de obra` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'costo_total', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'fdi' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'beneficiarios' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'otros' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Materia prima e insumos` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'costo_total', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'fdi' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'beneficiarios' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'otros' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Servicios` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'costo_total', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'fdi' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'beneficiarios' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'otros' } },
    //       ]
    //     },

    //     //*****
    //     {
    //       cols: [
    //         {
    //           rowspan: 5,
    //           contenido: `Inversión diferida`,
    //         },
    //         { contenido: `Gerenciamiento del Proyecto (Administración directa o delegada)` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'costo_total', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'fdi' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'beneficiarios' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'otros' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Capacitación y/o Asistencia Técnica` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'costo_total', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'fdi' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'beneficiarios' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'otros' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Supervisión` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'costo_total', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'fdi' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'beneficiarios' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'otros' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Fiscalización` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'costo_total', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'fdi' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'beneficiarios' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'otros' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Auditoria externa` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'costo_total', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'fdi' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'beneficiarios' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'otros' } },
    //       ]
    //     },
    //   ],
    //   totales: {
    //     costo_total: 0,
    //     fdi: 0,
    //     gam: 0,
    //     beneficiarios: 0,
    //     otros: 0
    //   },
    //   tipo: 1
    // };
  // }

  // $$obtenerCuadro2() {
  //   // this.proyecto.financiamiento.cuadro = {};
  //   this.DataService.get(`contenidos/cuadro_financiamiento/2`).then(response => {
  //     if (response) {
  //       if (response.datos && response.datos.estructura) {
  //         this.proyecto.financiamiento.cuadro = response.datos.estructura;
  //         // console.log('-------------cuadro2-------------');
  //         // console.log(this.proyecto.financiamiento.cuadro);
  //         // console.log('---------------------------------');
  //       }
  //     }
  //   });

    // this.proyecto.financiamiento.cuadro = {
    //   datos: [
    //     {
    //       cols: [
    //         {
    //           rowspan: 4,
    //           contenido: `Inversión fija`,
    //         },
    //         { contenido: `Infraestructura productiva` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'tfdi', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'publico' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'privado' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Bienes y/o equipamiento` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'tfdi', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'publico' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'privado' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Medidas Físicas (infraestructura) de prevención y/o mitigación de impactos ambientales` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'tfdi', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'publico' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'privado' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Medidas Físicas (infraestructura) de gestión de riesgos` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'tfdi', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'publico' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'privado' } },
    //       ]
    //     },

    //     //*******
    //     {
    //       cols: [
    //         {
    //           rowspan: 3,
    //           contenido: `Capital de trabajo`,
    //         },
    //         { contenido: `Mano de obra` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'tfdi', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'publico' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'privado' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Materia prima e insumos` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'tfdi', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'publico' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'privado' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Servicios` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'tfdi', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'publico' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'privado' } },
    //       ]
    //     },

    //     //*****
    //     {
    //       cols: [
    //         {
    //           rowspan: 5,
    //           contenido: `Inversión diferida`,
    //         },
    //         { contenido: `Gerenciamiento del Proyecto (Administración directa o delegada)` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'tfdi', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'publico' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'privado' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Capacitación y/o Asistencia Técnica` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'tfdi', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'publico' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'privado' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Supervisión (Hasta el 6%)` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'tfdi', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'publico' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'privado' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Fiscalización` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'tfdi', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'publico' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'privado' } },
    //       ]
    //     },
    //     {
    //       cols: [
    //         { contenido: `Auditoria externa (Hasta el 2% del FDI)` },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'tfdi', disabled: true } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'gam' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'publico' } },
    //         { contenido: { type: 'inputMoney', value: 0, name: 'privado' } },
    //       ]
    //     },
    //   ],
    //   totales: {
    //     fdi: 0,
    //     gam: 0,
    //     publico: 0,
    //     privado: 0,
    //     fdi_pp: 0,
    //     fdi_gam: 0
    //   },
    //   tipo: 2
    // };
  // }

  // establecerIndicadoresImpacto() {
  //   if (this.proyecto.beneficiarios && this.proyecto.beneficiarios.indicadores_impacto) {
  //     let indicadores = this.proyecto.beneficiarios.indicadores_impacto.split(" ");
  //     if (indicadores.length>0) {
  //       this.proyecto.beneficiarios.indicadores_impacto_numero = indicadores[0];
  //       this.proyecto.beneficiarios.indicadores_impacto_unidades = indicadores[1];
  //     }
  //   }
  // }

  setIndicadores() {
    this.proyecto.beneficiarios.subtipo_productivo = null;

    this.proyecto.beneficiarios.indicadores_impacto = [
      {
        combinado: null,
        numero: null,
        unidades: null,
        otro: null
      }
    ];
  }

  cambiarUnidades() {
    var _unidad = null;
    switch (this.proyecto.beneficiarios.tipo) {
      case 'OP_TP_RIEGO':
        _unidad = 'HA';
      break;
      case 'OP_TP_PUENTES':
        _unidad = 'M';
      break;
      default:
      break;
    }
    this.proyecto.beneficiarios.indicadores_impacto[0].unidades = _unidad;
    if (this.proyecto.informe) {
      // this.proyecto.informe.valoracion[5].cols[0].contenido = `Valor ${(this.proyecto.beneficiarios.tipo !== 'OP_TP_PRODUCTIVO' && this.proyecto.beneficiarios.tipo !== 'OP_TP_MAQUINARIA') ? 'VANS': 'VANE'}`;
      // this.proyecto.informe.valoracion[6].cols[0].contenido = `Valor ${(this.proyecto.beneficiarios.tipo !== 'OP_TP_PRODUCTIVO' && this.proyecto.beneficiarios.tipo !== 'OP_TP_MAQUINARIA') ? 'VANP': 'VANF'}`;
      this.proyecto.informe.valoracion[5].cols[0].contenido = `valor ${!this.Proyecto.tratarComoProductivo(this.proyecto) ? 'VANS' : 'VANE'}`;
      this.proyecto.informe.valoracion[6].cols[0].contenido = `valor ${!this.Proyecto.tratarComoProductivo(this.proyecto) ? 'VANP' : 'VANF'}`;
    }
  }

  clearIndicadores() {
    this.proyecto.beneficiarios.indicadores_impacto = [
      {
        combinado: null,
        numero: null,
        unidades: null,
        otro: null
      }
    ];
  }

  combinarIndicadoresImpacto(idx = 0) {
    if (this.proyecto.beneficiarios.indicadores_impacto[idx].numero && this.proyecto.beneficiarios.indicadores_impacto[idx].unidades) {
      if (typeof this.proyecto.beneficiarios.indicadores_impacto[idx].unidades === 'object') {
        if (this.proyecto.beneficiarios.indicadores_impacto[idx].unidades.codigo === 'Otro') {
          if (this.proyecto.beneficiarios.indicadores_impacto[idx].otro) {
            this.proyecto.beneficiarios.indicadores_impacto[idx].combinado = this.proyecto.beneficiarios.indicadores_impacto[idx].numero + ' ' + this.proyecto.beneficiarios.indicadores_impacto[idx].otro;
          } else {
            this.proyecto.beneficiarios.indicadores_impacto[idx].combinado = this.proyecto.beneficiarios.indicadores_impacto[idx].numero;
          }
        } else {
          this.proyecto.beneficiarios.indicadores_impacto[idx].combinado = this.proyecto.beneficiarios.indicadores_impacto[idx].numero + ' ' + this.proyecto.beneficiarios.indicadores_impacto[idx].unidades.nombre;
        }
      } else {
        this.proyecto.beneficiarios.indicadores_impacto[idx].combinado = this.proyecto.beneficiarios.indicadores_impacto[idx].numero + ' ' + this.proyecto.beneficiarios.indicadores_impacto[idx].unidades;
      }
    }

    // if (this.proyecto.beneficiarios.indicadores_impacto_numero && this.proyecto.beneficiarios.indicadores_impacto_unidades) {
    //   if (typeof this.proyecto.beneficiarios.indicadores_impacto_unidades === 'object') {
    //     if (this.proyecto.beneficiarios.indicadores_impacto_unidades.codigo === 'Otro') {
    //       if (this.proyecto.beneficiarios.indicadores_impacto_otro) {
    //         this.proyecto.beneficiarios.indicadores_impacto = this.proyecto.beneficiarios.indicadores_impacto_numero + ' ' + this.proyecto.beneficiarios.indicadores_impacto_otro;
    //       } else {
    //         this.proyecto.beneficiarios.indicadores_impacto = this.proyecto.beneficiarios.indicadores_impacto_numero;
    //       }
    //     } else {
    //       this.proyecto.beneficiarios.indicadores_impacto = this.proyecto.beneficiarios.indicadores_impacto_numero + ' ' + this.proyecto.beneficiarios.indicadores_impacto_unidades.nombre;
    //     }
    //   } else {
    //     this.proyecto.beneficiarios.indicadores_impacto = this.proyecto.beneficiarios.indicadores_impacto_numero + ' ' + this.proyecto.beneficiarios.indicadores_impacto_unidades;
    //   }
    // }

    console.log('----------------combine-------------');
    console.log(this.proyecto.beneficiarios.indicadores_impacto);
    console.log('------------------------------------');
  }

  actualizarIndicadoresImpacto() {
    if (this.proyecto.informe) {
      if (!this.proyecto.informe.indicador) this.proyecto.informe.indicador = {};
      this.proyecto.informe.indicador.proceso = this.proyecto.beneficiarios.indicadores_impacto[0].combinado;
    }
  }

  adicionarIndicador() {
    if (!this.proyecto.beneficiarios.indicadores_impacto) this.proyecto.beneficiarios.indicadores_impacto = [];

    this.proyecto.beneficiarios.indicadores_impacto.push({
      combinado: null,
      numero: null,
      unidades: null,
      otro: null
    });

    console.log('----------------add-----------------');
    console.log(this.proyecto.beneficiarios.indicadores_impacto);
    console.log('------------------------------------');
  }

  eliminarIndicador($index) {
    this.proyecto.beneficiarios.indicadores_impacto.splice($index, 1);
    return;
  }

  limpiarMatriz() {
    this.proyecto.beneficiarios.productivo = {};
  }

  editarBeneficiarios() {
    // if (this.Usuarios.esSupervisor()) return false;
    return this.Proyecto.esEditable('beneficiarios');
  }

  editarIndicadores() {
    // return this.Usuarios.esTecnicoResponsable() ? true : false;
    return this.Proyecto.esEditable('evaluacion');
  }

  noEditarCoordenadas() {
    if (this.Usuarios.esSupervisor()) return false;
    return this.modoVista || !this.editarBeneficiarios();
  }

  $$iniciarLeaflet() {
    this.mapa = {
      defaults: {
        minZoom: 5,
      },
      maxbounds: {
        northEast: {
          lat: -8.433237,
          lng: -71.367187
        },
        southWest: {
          lat: -23.596439,
          lng: -56.118164
        }
      }
    };
    let layers = {
      geobolivia_topo: {
        name: 'GeoBolivia Topográfico',
        url: 'https://geo.gob.bo/raster/service/wmts?',
        type: 'wms',
        visible: true,
        layerParams: {
          layers: 'mapbox_terrain',
          format: "image/png",
          transparent: true,
          //crs: L.CRS.EPSG4326,
          //version: '1.1.1',
        },
        layerOptions: {
          attribution: '<a href="https://geo.gob.bo" target="_blank">GeoBolivia</a>',
          continuousWorld: false
        }
      },
      geobolivia_sat: {
        name: 'GeoBolivia Satelital',
        url: 'https://geo.gob.bo/raster/service/wms?',
        type: 'wms',
        visible: true,
        layerParams: {
          layers: 'mapbox_bolivia',
          format: "image/png",
          transparent: true,
          //crs: L.CRS.EPSG4326,
          //version: '1.1.1',
        },
        layerOptions: {
          attribution: '<a href="https://geo.gob.bo" target="_blank">GeoBolivia</a>',
          continuousWorld: false
        }
      },
      openstreetmap: {
        name: 'OpenStreetMap',
        type: 'xyz',
        url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        layerOptions: {
          subdomains: ['a', 'b', 'c'],
          attribution: '&copy; <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> contributors',
          continuousWorld: false
        }
      },
      arcgis: {
        name: 'Topográfico',
        url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
        type: 'xyz',
        layerOptions: {
          attribution: '<a href="https://www.arcgis.com/home/webmap/viewer.html" target="_blank">ArcGIS</a>',
          continuousWorld: false
        }
      },
      mapbox_api: {
        name: 'Escala de grises',
        url: 'https://api.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png?access_token={apikey}',
        type: 'xyz',
        layerOptions: {
          apikey: 'pk.eyJ1IjoiY2hlbG9hbGVqb2MiLCJhIjoiY2lod2lkZHdrMDJueHUxbTFqMXRodGUzdyJ9.Vs-56Km_o5AXT9N5QrcLsw',
          mapid: 'landplanner.map-dsn5dtki',
          attribution: '<a href="https://www.mapbox.com/about/maps/" target="_blank">Mapbox</a>'
        }
      },
      arcgisimage: {
        name: 'Imágenes',
        url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
        type: 'xyz',
        layerOptions: {
          attribution: '<a href="https://www.arcgis.com/home/webmap/viewer.html" target="_blank">ArcGIS</a>',
          continuousWorld: false
        }
      },
    };

    this.myLayers = {
      baselayers: layers
    };
  }

}

export default DatosGenerales;
