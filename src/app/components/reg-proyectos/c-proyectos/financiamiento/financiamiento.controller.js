/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class DatosFinanciamiento {
  constructor($timeout, Proyecto, Util, Message, Usuarios) {
    'ngInject';
    this.$timeout = $timeout;
    this.Proyecto = Proyecto;
    this.Message = Message;
    this.Util = Util;
    this.Usuarios = Usuarios;
  }

  $onInit() {
    this.opciones_plazo_administrativo = [
      {codigo:240,nombre:'240 días (8 meses)'},
      {codigo:255,nombre:'255 días (8 1/2 meses)'},
      {codigo:270,nombre:'270 días (9 meses)'}
    ];
    if (this.proyecto) {
      if (!this.proyecto.estado_actual || !this.proyecto.estado_actual.areas || !this.proyecto.estado_actual.areas.DATOS_GENERALES_EXTRA) {
        if (this.proyecto.financiamiento) {
          this.proyecto.financiamiento.cuadro = null;
        }
      }

      if (this.proyecto.financiamiento) this.proyecto.financiamiento.plazo_tiempo_administrativo = {codigo:270,nombre:'270 días (9 meses)'};
    }
  }

  initPlantilla() {
    this.$timeout(() => {
      if (this.proyecto) {
        if (!this.proyecto.financiamiento) {
          this.proyecto.financiamiento = {
            $_plantilla: 2,
            $_nombre: 'financiamiento'
          };
        }
        this.monto_previo = this.proyecto.monto_total || 0;
        if (this.proyecto.estado_proyecto === 'REGISTRO_PROYECTO_FDI' && this.Usuarios.esTecnicoResponsable()) {
          this.obtenerTechoPresupuestario();
        }
        if (this.proyecto.financiamiento && this.proyecto.financiamiento.cuadro) {
          // if (!this.proyecto.financiamiento.cuadro.porcentajes) {
          this.calTotales();
          // }
        }
      }
    }, 500);
  }

  calcularFinanciamiento() {
    this.proyecto.financiamiento.monto_total_proyecto = (this.proyecto.financiamiento.monto_fdi || 0)
                                                      + (this.proyecto.financiamiento.monto_contraparte || 0)
                                                      + (this.proyecto.financiamiento.monto_beneficiarios || 0)
                                                      + (this.proyecto.financiamiento.monto_otros || 0);
    this.proyecto.monto_total = this.proyecto.financiamiento.monto_total_proyecto;
    this.proyecto.monto_fdi = this.proyecto.financiamiento.monto_fdi || 0;
    // Verificar monto total con techo presupuestario
    if (this.techoPresupuestario) {
      if (this.proyecto.observacion.mensaje) {
        this.techoPresupuestario.monto_restante = this.techoPresupuestario.monto_disponible - this.proyecto.financiamiento.monto_fdi + this.monto_previo;
      } else {
        this.techoPresupuestario.monto_restante = this.techoPresupuestario.monto_disponible - this.proyecto.financiamiento.monto_fdi;
      }
      this.techoPresupuestario.monto_restante = (this.techoPresupuestario.monto_restante > this.techoPresupuestario.techo_presupuestal) ? this.techoPresupuestario.techo_presupuestal : this.techoPresupuestario.monto_restante;
      if (this.techoPresupuestario.monto_restante < 0) {
        this.Message.warning('El monto asignado al FDI según la tabla de estructura de financiamiento excede el techo presupuestario para el municipio seleccionado');
        // this.proyecto.montoFinanciamientoExcedido = true;
        this.proyecto.montoFinanciamientoExcedido = false;
      // } else {
      //   this.proyecto.montoFinanciamientoExcedido = false;
      }
    }
    if (this.proyecto.estado_proyecto === 'REGISTRO_PROYECTO_FDI' && this.Usuarios.esTecnicoResponsable()) {
      this.calcularPorcentajes(this.proyecto.financiamiento.monto_total_proyecto);
    }
  }

  limpiarCero(columna, disabled) {
    if (columna.contenido.value === 0 && !disabled) {
      columna.contenido.value = '';
    }
  }

  reemplazarVacio(columna) {
    if (columna.contenido.value === null || columna.contenido.value === '') {
      columna.contenido.value = +0;
    }
  }

  calTotales() {
    let cuadro = this.proyecto.financiamiento.cuadro;
    cuadro.totales.costo_total = 0;
    cuadro.totales.fdi = 0;
    cuadro.totales.gam = 0;
    cuadro.totales.beneficiarios = 0;
    cuadro.totales.otros = 0;
    cuadro.totales.publico = 0;
    cuadro.totales.privado = 0;
    // Recorrer filas y columnas para calcular totales
    let campo = (cuadro.tipo === 1) ? ['fdi','gam','beneficiarios','otros'] : (cuadro.tipo === 2) ? ['gam','beneficiarios','otros','publico','privado'] : [];
    let param = {};
    // filas
    for (let i in cuadro.datos) {
      let columna_total = null;
      let campo_sumando = [];
      // columnas
      for (let j in cuadro.datos[i].cols) {
        let contenido = cuadro.datos[i].cols[j].contenido;
        if (cuadro.tipo === 1 && contenido && contenido.name === 'costo_total') { columna_total = contenido; }
        if (cuadro.tipo === 2 && contenido && contenido.name === 'tfdi') { columna_total = contenido; }
        if (contenido && campo.indexOf(contenido.name) >= 0) {
          if (!param[contenido.name]) { param[contenido.name] = []; }
          param[contenido.name].push(contenido.value);
          // Calcular columna costo total
          if (cuadro.tipo === 1) {
            campo_sumando.push(contenido.value);
          }
          // Calcular columna columna fdil
          if (cuadro.tipo === 2 && contenido.name != 'gam' && contenido.name != 'beneficiarios' && contenido.name != 'otros') {
            campo_sumando.push(contenido.value);
          }
        }
      }
      if (columna_total) {
        columna_total.value = this.Util.sumar(campo_sumando);
      }
    }
    // Sumar arrays
    for (let p in campo) {
      // param[campo[p]] = [cuadro.totales[campo[p]]]
      cuadro.totales[campo[p]] = this.Util.sumar(param[campo[p]]);
    }

    // Calcular totales finales
    if (cuadro.tipo === 1 || cuadro.tipo === 2) {
      if (cuadro.tipo === 2) {
        this.proyecto.financiamiento.cuadro.totales.fdi_pp = this.Util.sumar([this.proyecto.financiamiento.cuadro.totales.publico, this.proyecto.financiamiento.cuadro.totales.privado]);
        this.proyecto.financiamiento.cuadro.totales.fdi = this.proyecto.financiamiento.cuadro.totales.fdi_pp;
        this.proyecto.financiamiento.cuadro.totales.fdi_gam = this.Util.sumar([this.proyecto.financiamiento.cuadro.totales.fdi, this.proyecto.financiamiento.cuadro.totales.gam]);
      }

      this.proyecto.financiamiento.cuadro.totales.costo_total = this.Util.sumar([
        this.proyecto.financiamiento.cuadro.totales.fdi,
        this.proyecto.financiamiento.cuadro.totales.gam,
        this.proyecto.financiamiento.cuadro.totales.beneficiarios,
        this.proyecto.financiamiento.cuadro.totales.otros
      ]);
    }

    // Valores resumen
    this.proyecto.financiamiento.monto_fdi = this.proyecto.financiamiento.cuadro.totales.fdi || 0;
    this.proyecto.financiamiento.monto_contraparte = this.proyecto.financiamiento.cuadro.totales.gam || 0;
    this.proyecto.financiamiento.monto_beneficiarios = this.proyecto.financiamiento.cuadro.totales.beneficiarios || 0;
    this.proyecto.financiamiento.monto_otros = this.proyecto.financiamiento.cuadro.totales.otros || 0;

    // this.proyecto.financiamiento.cuadro.totales.fdi_pp
    this.calcularFinanciamiento();
  }

  calcularPorcentajes(referencia) {
    if (this.proyecto.financiamiento.cuadro.tipo === 1 || this.proyecto.financiamiento.cuadro.tipo === 2) {
      this.proyecto.financiamiento.cuadro.porcentajes = {
        total_referencia: referencia > 0 ? 100 : 0,
        porcentaje_fdi: referencia > 0 ? (this.proyecto.financiamiento.cuadro.totales.fdi / referencia)*100 : 0,
        porcentaje_gam: referencia > 0 ? (this.proyecto.financiamiento.cuadro.totales.gam / referencia)*100 : 0,
        porcentaje_beneficiarios: referencia > 0 ? (this.proyecto.financiamiento.cuadro.totales.beneficiarios / referencia)*100 : 0,
        porcentaje_otros: referencia > 0 ? (this.proyecto.financiamiento.cuadro.totales.otros / referencia)*100 : 0
      };

      if (this.proyecto.financiamiento.cuadro.tipo === 2) {
        this.proyecto.financiamiento.cuadro.porcentajes.porcentaje_fdi_publico = referencia > 0 ? (this.proyecto.financiamiento.cuadro.totales.publico / referencia)*100 : 0;
        this.proyecto.financiamiento.cuadro.porcentajes.porcentaje_fdi_privado = referencia > 0 ? (this.proyecto.financiamiento.cuadro.totales.privado / referencia)*100 : 0;
      }
    }
  }

  obtenerTechoPresupuestario() {
    const codMunicipio = (this.proyecto && this.proyecto.dpa && this.proyecto.dpa.municipio && this.proyecto.dpa.municipio.codigo) ? this.proyecto.dpa.municipio.codigo : 0;
    if (codMunicipio !== 0) {
      this.Proyecto.DataService.get(`techo_presupuestario/${codMunicipio}?gestion=${this.proyecto.nro_convenio.substr(-4)}`).then(response => {
        if (response) {
          this.techoPresupuestario = response.datos;
          // this.techoPresupuestario.monto_restante = response.datos.monto_disponible - this.proyecto.monto_total;
          if (this.proyecto.observacion.mensaje) {
            this.techoPresupuestario.monto_restante = response.datos.monto_disponible - this.proyecto.financiamiento.monto_fdi + this.monto_previo;
          } else {
            this.techoPresupuestario.monto_restante = response.datos.monto_disponible - this.proyecto.financiamiento.monto_fdi;
          }
          this.techoPresupuestario.monto_restante = (this.techoPresupuestario.monto_restante > this.techoPresupuestario.techo_presupuestal) ? this.techoPresupuestario.techo_presupuestal : this.techoPresupuestario.monto_restante;
        }
      });
    }
  }
}

export default DatosFinanciamiento;
