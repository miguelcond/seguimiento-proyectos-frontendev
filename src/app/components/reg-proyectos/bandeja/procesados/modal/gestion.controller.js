'use strict';

class GestionController {
    constructor(data, $uibModalInstance) {
        'ngInject';

        this.$uibModalInstance = $uibModalInstance;
        this.data = data;
    }

    $onInit() {
      let gestion = new Date();
      this.gestiones = [];
      for (let nombre=2018; nombre<=1900+gestion.getYear(); nombre++) {
        this.gestiones.unshift({nombre});
        this.gestion = this.gestiones[0];
      }
    }

    cerrar() {
      // this.$uibModalInstance.dismiss('cancel');
      this.$uibModalInstance.close(this.gestion.nombre);
    }

    aceptar() {
      this.$uibModalInstance.close(this.gestion.nombre);
    }
}

export default GestionController;
