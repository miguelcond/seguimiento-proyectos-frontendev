'use strict';

import modalGestionTemplate from './modal/gestion.html';
import modalGestionController from './modal/gestion.controller.js';

class ConvenioModalController {
  constructor($state, $uibModalInstance, DataService, Storage, data, Util, $filter, $timeout, Message, Proyecto, Loading, Modal) {
    'ngInject';
    this.$state = $state;
    this.$uibModalInstance = $uibModalInstance;
    this.DataService = DataService;
    this.Util = Util;
    this.$filter = $filter;
    this.data = data;
    this.$timeout = $timeout;
    this.Message = Message;
    this.Proyecto = Proyecto;
    this.Loading = Loading;
    this.Modal = Modal;
    this.conEstados = false;
    this.user = Storage.getUser();
  }

  $onInit(){
    this.opciones = {};
    this.modoVistaConvenio = false;
    this.getDatosEstadoInicial();
  }

  getDatosEstadoInicial() {
    if(this.data.id_proyecto_referencia){
      this.DataService.get(`estados/PROYECTO-FDI/`+this.data.id_proyecto_referencia).then(response => {
        if (response) {
          this.Proyecto.setEstadoActual(response.datos);
          this.conEstados = true;
          if(!this.proyecto) {
            this.proyecto = {
              fecha_suscripcion_convenio: response.datos.fecha_actual,
              nro_convenio: response.datos.codigo_generado,
              id_proyecto_referencia: this.data.id_proyecto_referencia,
              nro_convenio_referencia: this.data.nro_convenio_referencia
            }
          }
        }
      });
    }else{
      const modalGestion =  this.Modal.show({
        template: modalGestionTemplate,
        controller: modalGestionController,
        data: {},
        size: 'md'
      });
      modalGestion.result.then( (gestion) => {
        this.DataService.get(`estados/PROYECTO-FDI?gestion=${gestion}`).then(response => {
          if (response) {
            this.Proyecto.setEstadoActual(response.datos);
            this.conEstados = true;
            if(!this.proyecto) {
              this.proyecto = {
                fecha_suscripcion_convenio: response.datos.fecha_actual,
                nro_convenio: response.datos.codigo_generado
              }
            }
          }
        });
      }, () => { });
    }
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

  guardarDatos() {
    this.Loading.show('Registrando proyecto', true);
    this.DataService.post(`proyectos/reg`, this.formatoDatosConvenio()).then(response => {
      if (response) {
        this.Message.success("Proyecto creado correctamente.");
        this.cerrar();
        this.Loading.hide();
        if (response.datos && response.datos.nro_convenio) {
          this.Modal.alert(`El código de la solicitud es<br><strong>${response.datos.nro_convenio}</strong>.`, ()=>{
            this.$state.reload();
          }, 'Solicitud enviada');
        } else {
          this.$state.reload();
        }
      }
    });
  }

  formatoDatosConvenio() {
    const convenio = angular.copy(this.proyecto);
    convenio.fcod_municipio = convenio.dpa.municipio.codigo;
    convenio.rol = this.user.rol.id_rol;
    // Convenio corregido
    convenio.nro_convenio = `${this.proyecto.cite_codigo}${this.proyecto.cite_nro}`;
    // Para proyectos FDI
    if (!convenio.tipo) {
      convenio.tipo = 'TP_CIFE';
    }
    delete convenio.dpa;

    return convenio;
  }

}

export default ConvenioModalController;
