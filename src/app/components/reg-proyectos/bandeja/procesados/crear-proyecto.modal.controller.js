'use strict';

class ConvenioModalController {
  constructor($state, $uibModalInstance, DataService, Storage, data, Util, $filter, $timeout, Message, Proyecto, Loading) {
    'ngInject';
    this.$state = $state;
    this.$uibModalInstance = $uibModalInstance;
    this.DataService = DataService;
    this.Util = Util;
    this.$filter = $filter;
    this.data = data;
    this.$timeout = $timeout;
    this.Message = Message;
    this.Proyecto = Proyecto;
    this.Loading = Loading;
    this.conEstados = false;
    this.user = Storage.getUser();
  }

  $onInit(){    
    this.opciones = {};
    this.modoVistaConvenio = false;
    this.getDatosEstadoInicial();
  }

  getDatosEstadoInicial() {
    this.DataService.get(`estados/PROYECTO`).then(response => {
      if (response) {
        this.Proyecto.setEstadoActual(response.datos);
        this.conEstados = true;
      }
    });
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

  guardarDatos() {
    this.Loading.show('Registrando proyecto', true);
    this.DataService.post(`proyectos`, this.formatoDatosConvenio()).then(response => {
      if (response) {
        this.Message.success("Proyecto creado correctamente.");
        this.cerrar();
        this.Loading.hide();
        this.$state.reload();
      }
    });
  }

  formatoDatosConvenio() {
    const convenio = angular.copy(this.proyecto);
    convenio.fcod_municipio = convenio.dpa.municipio.codigo;
    convenio.rol = this.user.rol.id_rol;
    delete convenio.dpa;

    return convenio;
  }

}

export default ConvenioModalController;
