/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
import modalAdjudicacionesController from '../supervisiones/adjudicaciones.modal.controller';
import modalAdjudicacionesTemplate from '../supervisiones/adjudicaciones.modal.html';
import modalModificacionesController from '../supervisiones/modificaciones.modal.controller';
import modalModificacionesTemplate from '../supervisiones/modificaciones.modal.html';

import modalModificacionesAdjudicacionController from '../modificaciones/modal.controller';
import modalModificacionesAdjudicacionTemplate from '../modificaciones/modal.html';

'use strict';

class BandejaPendientesController {
  constructor(Storage, DataService, DataTable, $state, _, Modal, Usuarios, apiUrl) {
    'ngInject';
    this.Storage = Storage;
    this.DataService = DataService;
    this.DataTable = DataTable;
    this.$state = $state;
    this._ = _;
    this.Modal = Modal;
    this.Usuarios = Usuarios;
    this.apiUrl = apiUrl;
  }

  $onInit() {
    this.usuario = angular.fromJson(this.Storage.getUser());
    this.config = this.getConfiguracion();
    this.configSupervisiones = this.getConfiguracionSupervisiones();
    const QUERY = {
      rol: this.Storage.get('rol').id_rol
    }; // this.esEmpresa() ? {matricula: this.usuario.matricula} : {rol:this.Storage.get('rol').id_rol};
    this.tableParams = this.DataTable.getParams(`proyectos`, QUERY);
    this.verModificacion = this.puedeVerModificacion();
  }

  getConfiguracion() {
    if (!this.usuario.rol) {
      return;
    }
    let configuracion = {
      acciones: [{icon: 'edit', tooltip: 'Procesar proyecto', md: 'primary', metodo: this.procesarProyecto.bind(this)}]
    };
    if (this.Usuarios.esFiscal()) {
      configuracion = {
        acciones: [{icon: 'eye', tooltip: 'Ver', md: 'primary', metodo: this.procesarProyecto.bind(this)}]
      };
    }
    return configuracion;
  }

  getConfiguracionSupervisiones() {
    var configuracion = {};
    if (!this.usuario.rol) {
      return;
    }
    switch (this.usuario.rol.nombre) {
      default:
        configuracion = {
          acciones: [
            {
              icon: 'edit',
              tooltip: 'Procesar planilla',
              md: 'tertiary',
              metodo: this.procesarPlanilla.bind(this)
            }
          ]
        }
    }
    return configuracion;
  }

  urlPlanillaConsolidada(proyecto) {
    return this.urlPlanillaConsolidadaDescarga = `${this.apiUrl}proyectos/${proyecto.id_proyecto}/doc/PAC_${proyecto.id_proyecto}/descargar`;
  }

  puedeProcesarProyecto(proyecto) {
    if (proyecto.estado_proyecto == 'APROBADO_FDI' && this.Usuarios.esResponsableGAM())
      return true;
    if (proyecto.estado_proyecto == 'APROBADO_FDI' && this.Usuarios.esSupervisor())
      return true;
    // if(proyecto.estado_proyecto == 'EN_EJECUCION_FDI' && this.Usuarios.esFiscal()) return false;
    return proyecto.estado_proyecto !== 'APROBADO_FDI' || (proyecto.estado_proyecto === 'APROBADO_FDI' && this.Usuarios.esSupervisor());
  }

  procesarProyecto(proyecto) {
    // if (this.esEmpresa() && proyecto.estado_proyecto === 'REGISTRO_EMPRESA') {
    //   return this.documentarProyecto(proyecto);
    // }
    if ((this.Usuarios.esSupervisor() && proyecto.estado_proyecto === 'MODIFICADO_FDI') ||
        ((this.Usuarios.esTecnicoResponsable() || this.Usuarios.esJefeRevision()) && proyecto.estado_proyecto === 'REVISION_MODIFICADO_FDI')) {
      return this.procesarModificacion(proyecto);
    }
    this.$state.go('reg-proyecto', {codigo: proyecto.id_proyecto});
  }

  // documentarProyecto(proyecto) {
  //   this.$state.go('documentacion-proyecto', {codigo: proyecto.id_proyecto});
  // }

  procesarPlanilla(proyecto) {
    this.$state.go('planilla-proyecto', {idSupervision: proyecto.supervisiones[0].id_supervision});
  }

  procesarModificacion(proyecto) {
    this.DataService.get(`proyectos/${proyecto.id_proyecto}/modificaciones/historial`).then((result) => {
      if (result && result.finalizado) {
        let modificacion = result.datos[0];
        let tipoModificacion = modificacion.tipo_modificacion === 'TM_ORDEN_TRABAJO' ? 1 : modificacion.tipo_modificacion === 'TM_ORDEN_CAMBIO' ? 2 : modificacion.tipo_modificacion === 'TM_CONTRATO_MODIFICATORIO' ? 3 : 0;
        if (modificacion && ['MODIFICADO_FDI','REVISION_MODIFICADO_FDI'].indexOf(modificacion.estado_proyecto) >= 0) {
          this.$state.go('modificacion', {
            proyecto: proyecto.id_proyecto,
            version: modificacion.version,
            tipoModificacion,
            edicion: this.Usuarios.esSupervisor()
          });
        }
      }
    });
  }

  // esEmpresa() {
  //   return !angular.isUndefined(this.usuario.nit);
  // }

  puedeVerConsolidado(proyecto) {
    return ['RESP_GAM_GAIOC','FINANCIERO_FDI','TECNICO_FDI','FISCAL_FDI'].indexOf(this.usuario.rol.nombre) !== -1 && ['APROBADO_FDI','EN_EJECUCION_FDI'].indexOf(proyecto.estado_proyecto) !== -1;
  }

  puedeVerSupervision(proyecto) {
    if (this.Usuarios.esFinanciero())
      return false;
    if (proyecto) {
      return ['APROBADO_FDI','EN_EJECUCION_FDI','MODIFICADO_FDI'].indexOf(proyecto.estado_proyecto) >= 0 && proyecto.supervisiones && proyecto.supervisiones.length > 0;
    }
    return false;
  }

  puedeVerModificacion(proyecto) {
    if (proyecto) {
      // TODO Se deberia obtener desde una configuracion del proyecto Base de Datos
      return ['SUPERVISOR_FDI','TECNICO_FDI','JEFE_REVISION_FDI'].indexOf(this.usuario.rol.nombre) >= 0 && ['REVISION_MODIFICADO_FDI','MODIFICADO_FDI','EN_EJECUCION_FDI'].indexOf(proyecto.estado_proyecto) >= 0;
    }
    return false;
  }

  puedeCrearPlanilla(proyecto) {
    if (!proyecto.orden_proceder)
      return false;
    if (['EN_EJECUCION_FDI'].indexOf(proyecto.estado_proyecto) < 0)
      return false;
    //revisar adjudicaciones
    for (let i = 0; i < proyecto.adjudicaciones.length; i++) {
      if (proyecto.adjudicaciones[i].supervision.length <= 0 && proyecto.adjudicaciones[i].avance && !proyecto.adjudicaciones[i].avance.completo && proyecto.adjudicaciones[i].esSupervisor) return true;
    }
    return false;
  }

  puedeVerModificaciones(proyecto) {
    if (!proyecto.orden_proceder)
      return false;
    if ((this.Usuarios.esSupervisor() || this.Usuarios.esFiscal() || this.Usuarios.esResponsableGAM() || this.Usuarios.esTecnicoResponsable()) && ['EN_EJECUCION_FDI'].indexOf(proyecto.estado_proyecto) !== -1)
      return true;
    return false;
  }

  irAModificaciones(proyecto) {
    if (proyecto.adjudicaciones.length > 1) {
      if (proyecto.id_proyecto > 0) {
        this.Modal.show({
          template: modalModificacionesTemplate,
          controller: modalModificacionesController,
          data: proyecto,
          size: 'lg'
        });
      }
    } else {
      proyecto.adjudicacion = proyecto.adjudicaciones[0];
      this.Modal.show({
        template: modalModificacionesAdjudicacionTemplate,
        controller: modalModificacionesAdjudicacionController,
        data: proyecto,
        size: 'lg'
      });
    }
  }

  nuevaPlanilla(proyecto) {
    if (proyecto.adjudicaciones.length > 1) {
      if (proyecto.id_proyecto > 0) {
        this.Modal.show({
          template: modalAdjudicacionesTemplate,
          controller: modalAdjudicacionesController,
          data: proyecto,
          size: 'lg'
        });
      }
    } else {
      const data = {
        fid_proyecto: proyecto.id_proyecto,
        computo_metrico: []
      };
      this.DataService.post(`supervisiones/adjudicacion/${proyecto.adjudicaciones[0].id_adjudicacion}`, data).then(response => {
        if (response) {
          this.irPlanilla(response.datos.id_supervision);
        }
      });
    }
  }

  irPlanilla(idSupervision) {
    this.$state.go('planilla-proyecto', { idSupervision });
  }
}

export default BandejaPendientesController;
