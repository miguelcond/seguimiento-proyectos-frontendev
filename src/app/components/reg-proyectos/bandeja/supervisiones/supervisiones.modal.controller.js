/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class NuevaAdjudicacionModalController {
  constructor($uibModalInstance, Storage, data, apiUrl, $state, Usuarios) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.Storage = Storage;
    this.apiUrl = apiUrl;
    this.$state = $state;
    this.proyecto = data;
    this.Usuarios = Usuarios;
  }

  $onInit() {
    this.usuario = this.Storage.getUser();
    this.asignarDatosDescarga();
  }

  asignarDatosDescarga() {
    this.proyecto.supervisiones.map(supervision => {
      supervision.urlPlanillaDescargaAvance = `${this.apiUrl}proyectos/${this.proyecto.id_proyecto}/doc/SUP_${supervision.id_supervision}/descargar`;
      supervision.urlPlanillaDescarga = `${this.apiUrl}proyectos/${this.proyecto.id_proyecto}/doc/PA_${supervision.fid_adjudicacion}-${supervision.nro_supervision}/descargar`;
      supervision.nombrePlanillaDescarga = `Planilla de avance nro ${supervision.nro_supervision}`;
      supervision.urlComputosDescarga = `${this.apiUrl}proyectos/${this.proyecto.id_proyecto}/doc/CMR_${supervision.nro_supervision}/descargar`;
      supervision.nombreComputosDescarga = `Resumen de computos metricos planilla_${supervision.nro_supervision}`;
    });
  }

  irAPlanilla(supervision) {
    this.$state.go('planilla-proyecto', { idSupervision: supervision.id_supervision });
    this.$uibModalInstance.close(false);
  }

  puedeVerPlanilla(idAdjudicacion,supervision) {
    if ((this.Usuarios.esFiscal() && ['EN_FISCAL_FDI','PENDIENTE_FDI'].indexOf(supervision.estado_supervision) == -1) || this.Usuarios.esResponsableProyecto())
      return true;
    for (let i = 0; i < this.proyecto.adjudicaciones.length; i++) {
      if (this.proyecto.adjudicaciones[i].id_adjudicacion == idAdjudicacion) {
        if (this.puedeProcesarPlanilla(idAdjudicacion,supervision))
          return false;
        else
          return true;
      }
    }
    return false;
  }

  puedeProcesarPlanilla(idAdjudicacion,supervision) {
    if (this.Usuarios.esFiscal() && ['EN_FISCAL_FDI','PENDIENTE_FDI'].indexOf(supervision.estado_supervision) != -1)
      return true;
    for (let i = 0; i < this.proyecto.adjudicaciones.length; i++) {
      if (this.proyecto.adjudicaciones[i].id_adjudicacion == idAdjudicacion && ['EN_ARCHIVO_FDI','PENDIENTE_FDI','EN_FISCAL_FDI'].indexOf(supervision.estado_supervision) === -1 && this.Usuarios.esSupervisor() && this.proyecto.adjudicaciones[i].esSupervisor) {
        return true;
      }
    }
    return false;
  }

  puedeRevisarPlanilla(idAdjudicacion) {
    if (this.Usuarios.esFiscal() || this.Usuarios.esResponsableProyecto())
      return true;
    for (let i = 0; i < this.proyecto.adjudicaciones.length; i++) {
      if (this.proyecto.adjudicaciones[i].id_adjudicacion == idAdjudicacion && this.proyecto.adjudicaciones[i].esSupervisor) {
        return true;
      }
    }
    return false;
  }

  verComponente(idAdjudicacion) {
    let nombre = '';
    let adjudicacion = this.verAdjudicacion(idAdjudicacion);
    if (adjudicacion.referencia) nombre = adjudicacion.referencia;
    return nombre;
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

  planillaEnArchivo(supervision) {
    return supervision.estado_supervision === 'EN_ARCHIVO_FDI';
  }

  planillaEnAvance(supervision) {
    return ['EN_SUPERVISION_FDI', 'PENDIENTE_FDI', 'EN_FISCAL_FDI'].indexOf(supervision.estado_supervision) !== -1 || (this.Usuarios.esTecnicoResponsable() && supervision.estado_supervision !== 'EN_ARCHIVO_FDI');
  }
}

export default NuevaAdjudicacionModalController;
