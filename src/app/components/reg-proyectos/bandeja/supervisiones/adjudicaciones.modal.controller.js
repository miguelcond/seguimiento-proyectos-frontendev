/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class NuevaAdjudicacionModalController {
  constructor($uibModalInstance,DataService, Storage, data, apiUrl, $state,Usuarios) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.Storage = Storage;
    this.apiUrl = apiUrl;
    this.$state = $state;
    this.proyecto = data;
    this.Usuarios = Usuarios;
    this.DataService = DataService;
  }

  $onInit() {
    this.usuario = this.Storage.getUser();
  }

  puedeCrearPlanilla(adjudicacion) {
    if (adjudicacion.supervision.length > 0) {
      return false;
    }
    return (adjudicacion.avance && !adjudicacion.avance.completo && adjudicacion.esSupervisor);
  } 
  
  nuevaPlanilla(proyecto,idAjudicacion) {
    const data = {
      fid_proyecto: proyecto.id_proyecto,
      computo_metrico: []
    };
    this.DataService.post(`supervisiones/adjudicacion/${idAjudicacion}`, data).then(response => {
      if (response) {
        this.$uibModalInstance.close(false);
        this.irPlanilla(response.datos.id_supervision);
      }
    });
  }

  irPlanilla(idSupervision) {
      this.$state.go('planilla-proyecto', { idSupervision });
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }
}

export default NuevaAdjudicacionModalController;
