'use strict';

class ParalizarModalController {
  constructor($uibModalInstance, $state, data, Modal, DataService, Storage, _) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.$state = $state;
    this.proyecto = data;
    this.DataService = DataService;
    this._ = _;
    this.user = Storage.getUser();
  }

  $onInit() {
    this.existeSupervisionesEnCurso = this.tieneSupervisionesEnCurso();
  }

  tieneSupervisionesEnCurso() {
    return this._.some(this.proyecto.supervisiones, supervision => {
      return supervision.estado_supervision !== 'EN_ARCHIVO_FDI' && supervision.estado_supervision !== 'REGISTRO_PLANILLA_FDI';
    });
  }

  paralizarProyecto() {
    this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/empresas/paralizar`).then((resp) => {
      if (resp && resp.finalizado) {
        //this.$state.go('mis-proyectos', {});
        this.$uibModalInstance.close(true);
        this.$state.reload();
      }
    });
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }
}

export default ParalizarModalController;
