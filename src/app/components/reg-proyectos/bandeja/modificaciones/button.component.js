'use strict';

import controller from './button.controller';
import template from './button.html';

const ListaModificacionesComponent = {
  bindings:{
    proyecto: '<'
  },
  controller,
  template
};

export default ListaModificacionesComponent;
