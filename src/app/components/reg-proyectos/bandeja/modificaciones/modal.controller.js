'use strict';

class NuevaModificacionModalController {
  constructor($uibModalInstance, $state, data, Modal, DataService, Storage, _, Usuarios) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.$state = $state;
    this.proyecto = data;
    this.Modal = Modal;
    this.DataService = DataService;
    this._ = _;
    this.modificando = true;
    this.user = Storage.getUser();
    this.Usuarios = Usuarios;
  }

  $onInit() {
    this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/modificaciones/${this.proyecto.adjudicacion.id_adjudicacion}/historial`).then((result) => {
      if (result && result.finalizado) {
        this.modificaciones = result.datos;
        this.existeSupervisionesEnCurso = this.tieneSupervisionesEnCurso();
        this.puedeModificar = !(this.proyecto.estado_proyecto != 'EN_EJECUCION_FDI') &&
          this.Usuarios.esSupervisor() && !this.existeSupervisionesEnCurso && this.proyecto.estado_proyecto !== 'CERRADO_FDI' && !result.en_modificacion;
        this.modificando = ['SUPERVISOR_FDI','FISCAL_FDI','TECNICO_FDI','JEFE_REVISION_FDI','RESP_GAM_GAIOC'].indexOf(this.user.rol.nombre) >= 0 ? true : false;
        let cant = this.modificaciones.length - 1;
        if (cant >= 0 && this.modificaciones[cant] && !this.modificaciones[cant].nombre) {
          this.modificaciones[cant].nombre = 'Contrato Original';
        }
      }
    });
  }

  tieneSupervisionesEnCurso() {
    return this._.some(this.proyecto.supervisiones, supervision => {
      return supervision.estado_supervision !== 'EN_ARCHIVO_FDI' && supervision.estado_supervision !== 'REGISTRO_PLANILLA_FDI' && supervision.fid_adjudicacion === this.proyecto.adjudicacion.id_adjudicacion;
    });
  }

  abrirModificacion(tipoModificacion) {
    let html = `
      1. Para realizar una modificación deberá contar con el respectivo documento de respaldo debidamente aprobado por las instancias pertinentes (Libro de Ordenes e Informes Técnicos para la Orden de Trabajo y Orden de Cambio y el respectivo contrato para el Contrato Modificatorio).
      <br/><br/>
      2. Una vez que este registrando modificaciones la empresa no podrá procesar una planilla de avance hasta que se finalice y apruebe la solicitud de modificación.
    `;
    this.Modal.confirm(html, () => {
      this.$uibModalInstance.close(false);
      /*const data = {
        id_adjudicacion: this.proyecto.adjudicacion.id_adjudicacion,
        fid_proyecto: this.proyecto.id_proyecto,
        tipo_modificacion: tipoModificacion
      };*/
      /*this.DataService.post(`proyectos/adjudicaciones/modificacion/${this.proyecto.adjudicacion.id_adjudicacion}`, data).then(response => {
        if (response) {
          //this.irAModificacion(response.datos.id_adjudicacion);
        }
      });*/
      this.$state.go('modificacion', {
        proyecto: this.proyecto.id_proyecto,
        componente: 'test',
        adjudicacion: this.proyecto.adjudicacion.id_adjudicacion,
        tipoModificacion,
        edicion: true,
        crear: true
      });
    }, null, 'Confirmar inicio de modificación', 'Confirmar');
  }

  irAModificacion(modificacion, edicion) {
    if (this.Usuarios.esFiscal()||this.Usuarios.esTecnicoResponsable()) edicion = false;
    let tipoModificacion = 0;
    switch (modificacion.tipo_modificacion) {
      case 'TM_ORDEN_TRABAJO':
        tipoModificacion = 1;
        break;
      case 'TM_ORDEN_CAMBIO':
        tipoModificacion = 2;
        break;
      case 'TM_CONTRATO_MODIFICATORIO':
        tipoModificacion = 3;
        break;
      default:
        tipoModificacion = 0;
        break;
    }
    // let tipoModificacion = modificacion.tipo_modificacion === 'TM_ORDEN_TRABAJO' ? 1 : modificacion.tipo_modificacion === 'TM_ORDEN_CAMBIO' ? 2 : modificacion.tipo_modificacion === 'TM_CONTRATO_MODIFICATORIO' ? 3 : 0;
    this.$uibModalInstance.close(false);

    this.$state.go('modificacion', {
      proyecto: this.proyecto.id_proyecto,
      adjudicacion: this.proyecto.adjudicacion.id_adjudicacion,
      version: modificacion.version,
      tipoModificacion,
      edicion,
      crear: false
    });
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

  verModificar(modificacion) {
    return (modificacion.estado_adjudicacion == 'MODIFICADO_FDI' && this.Usuarios.esSupervisor())
      || (modificacion.estado_adjudicacion == 'REVISION_MODIFICADO_FDI' && this.Usuarios.esFiscal())
      || (modificacion.estado_adjudicacion == 'NO_OBJECION_FDI' && this.Usuarios.esTecnicoResponsable());
  }

  verUltimaModificacion(modificacion) {
    return ['NO_OBJECION_FDI', 'REVISION_MODIFICADO_FDI', 'MODIFICADO_FDI', 'CERRADO_FDI'].indexOf(modificacion.estado_adjudicacion) >= 0 &&
           ['SUPERVISOR_FDI','TECNICO_FDI','JEFE_REVISION_FDI','RESP_GAM_GAIOC','FISCAL_FDI'].indexOf(this.user.rol.nombre) >= 0 &&
           !this.verModificar(modificacion);
  }
}

export default NuevaModificacionModalController;
