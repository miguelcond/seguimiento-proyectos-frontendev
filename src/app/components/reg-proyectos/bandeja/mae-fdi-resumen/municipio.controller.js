'use strict';
import resumenProyectoController from '../procesados/resumen-proyecto.modal.controller';
import resumenProyectoTemplate from '../procesados/resumen-proyecto.modal.html';

// private
let local = {
  calcularTotales: ($ctrl, datos)=>{
    $ctrl.totales = {
      total: 0,
      fdi: 0,
      gam: 0,
      beneficiarios: 0,
      ejecucion: 0,
      avance_fisico: 0,
    };
    for (let i in datos) {
      $ctrl.totales.total = local.Util.sumar([$ctrl.totales.total, datos[i].financiamiento.monto_total_proyecto]);
      $ctrl.totales.fdi = local.Util.sumar([$ctrl.totales.fdi, datos[i].financiamiento.monto_fdi]);
      $ctrl.totales.gam = local.Util.sumar([$ctrl.totales.gam, datos[i].financiamiento.monto_contraparte]);
      $ctrl.totales.beneficiarios = local.Util.sumar([$ctrl.totales.beneficiarios, datos[i].financiamiento.monto_beneficiarios]);
    }
  },
  calcularTotalesEval: ($ctrl, datos)=>{
    $ctrl.totalesEval = {
      total: 0,
      fdi: 0,
      gam: 0,
      beneficiarios: 0,
      ejecucion: 0,
      avance_fisico: 0,
    };
    for (let i in datos) {
      $ctrl.totalesEval.total = local.Util.sumar([$ctrl.totalesEval.total, datos[i].financiamiento.monto_total_proyecto]);
      $ctrl.totalesEval.fdi = local.Util.sumar([$ctrl.totalesEval.fdi, datos[i].financiamiento.monto_fdi]);
      $ctrl.totalesEval.gam = local.Util.sumar([$ctrl.totalesEval.gam, datos[i].financiamiento.monto_contraparte]);
      $ctrl.totalesEval.beneficiarios = local.Util.sumar([$ctrl.totalesEval.beneficiarios, datos[i].financiamiento.monto_beneficiarios]);
    }
  },
  obtenerResumen: ($ctrl, dpa)=>{
    if(dpa && dpa.departamento) {
      let _ruta = dpa.departamento.codigo;
      if(dpa.municipio){
        _ruta += `/${dpa.municipio.codigo}`;
      }
      local.DataService.get(`proyectos/municipios/${_ruta}?gestion=${$ctrl.gestion.nombre}`).then(resp=>{
        local.$log.log('resp', resp);
        if (resp && resp.finalizado) {
          $ctrl.datos = resp.datos;
          local.calcularTotales($ctrl, resp.datos);
          $ctrl.titulo = resp.titulo;
          $ctrl.datosEval = resp.datosEval;
          $ctrl.datosRechazados = resp.datosRechazados;
          local.calcularTotalesEval($ctrl, resp.datosEval);
        }
      });
    } else {
      $ctrl.datos = [];
      $ctrl.datosEval = [];
      $ctrl.datosRechazados = [];
    }
  }
};

// public
class ListadoController {
  constructor($log, DataService, $state, Util, Modal, Loading) {
    'ngInject';

    local.$log = $log;
    local.DataService = DataService;
    local.Util = Util;
    local.titulo = '';
    local.Loading = Loading;
    this.$state = $state;
    this.Modal = Modal;
  }
  $onInit() {
    let gestion = new Date();
    this.gestiones = [];
    for (let nombre=2018; nombre<=1900+gestion.getYear(); nombre++) {
      this.gestiones.unshift({nombre});
      this.gestion = this.gestiones[0];
    }
  }

  $doCheck() {
    let d1 = this.dpa && this.dpa.departamento;
    let d2 = local.dpa && local.dpa.departamento;

    let m1 = this.dpa && this.dpa.municipio;
    let m2 = local.dpa && local.dpa.municipio;

    if ( ((d1 && !d2) || (!d1 && d2) || (d1 && d2 && d1.codigo!=d2.codigo) ) || ((m1 && !m2) || (!m1 && m2) || (m1 && m2 && m1.codigo!=m2.codigo) )) {
      local.dpa = angular.copy(this.dpa);
      local.obtenerResumen(this, local.dpa);
    }
  }

  cambiarGestion() {
    local.obtenerResumen(this, local.dpa);
  }

  verProyecto(proyecto) {
    //this.$state.go('reg-proyecto', {codigo: proyecto.id_proyecto, urlProcesado: 'procesados'});
    local.DataService.get(`proyectos/${proyecto.id_proyecto}/procesados`).then(resp=>{
      this.Modal.show({
        template: resumenProyectoTemplate,
        controller: resumenProyectoController,
        data: resp.datos,
        size: 'xlg'
      });
    });
  }

  fichaProyecto(proyecto) {
    local.Loading.show(`Se estan obteniendo los datos, espere por favor`, true);
    local.DataService.get(`reportes/ficha_tecnica/${proyecto.id_proyecto}`).then((resp) => {
      if (resp && resp.finalizado) {
        local.Loading.hide();
        const base64 = `data:application/pdf;base64,${resp.datos.base64}`;
        const aLink = document.createElement("a");
        const fileName = `Ficha_Tecnica_${proyecto.nro_convenio.replace(/\//g,'.')}.pdf`;

        aLink.href = base64;
        aLink.download = fileName;
        aLink.click();
      } else {
        local.Loading.hide();
      }
    });
  }

}

export default ListadoController;
