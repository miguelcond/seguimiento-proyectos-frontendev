'use strict';

import ListaProyectosComponent from './listado.component';

const Proyectos = angular
    .module('app.revisor.proyectos', [])
    .component('listaProyectos', ListaProyectosComponent)
    .config(($stateProvider) => {
      $stateProvider
          .state('lista-proyectos', {
            url: '/lista-proyectos',
            component: 'listaProyectos'
          });
    })
    .name;

export default Proyectos;
