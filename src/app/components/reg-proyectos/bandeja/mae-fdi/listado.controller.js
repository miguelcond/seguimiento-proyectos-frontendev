'use strict';

class ListadoController {
  constructor(apiUrl, Storage, DataService, Modal, DataTable, $state, $location, $log) {
    'ngInject';
    this.Storage = Storage;
    this.DataService = DataService;
    this.apiUrl = apiUrl;
    this.$location = $location;
    this.$log = $log;
    this.Modal = Modal;
    this.$state = $state;
    this.DataTable = DataTable;
  }
  $onInit() {
    this.datos= {};
    this.usuario = angular.fromJson(this.Storage.getUser());
    this.config = this.obtenerConfiguracion();
    this.esEmpresa = this.esUsuarioEmpresa();
    this.verModificacion = this.puedeVerModificacion();
    const QUERY = this.esUsuarioEmpresa()? {matricula: this.usuario.matricula} : {rol:this.Storage.get('rol').id_rol};
    this.tableParams = this.DataTable.getParams(`proyectos/procesados`, QUERY);
  }

  obtenerConfiguracion() {
    var configuracion = {};
    if (this.usuario.rol) {
      switch (this.usuario.rol.nombre) {
        // case 'EMPRESA':
        //   configuracion = {
        //     opciones: [{icon: 'plus', tooltip: 'Adjudicar', md: 'success', metodo: this.nuevaAdjudicacion.bind(this)}],
        //   };
        //   break;
        default:
      }
      configuracion.acciones = [{icon: 'eye', tooltip: 'Ver', md: 'primary', metodo: this.observarProy.bind(this)}];
    }
    return configuracion;
  }

  eventoOpcion(event, item, data) {
    if(typeof item.metodo === 'function') {
      item.metodo(event, data);
    }
  }

  // nuevaAdjudicacion() {
  // }

  observarProy(event, proyecto) {
    // if (this.esEmpresa() && proyecto.estado_proyecto === 'REGISTRO_EMPRESA') {
    //   return this.documentarProyecto(proyecto);
    // }
    this.$log.log('Proyecto', event, proyecto);
    this.$state.go('reg-proyecto', {codigo: proyecto.id_proyecto, urlProcesado: 'procesados'});
  }

  documentar() {

  }

  nuevaPlanilla(proyecto) {
    const data = {
      fid_proyecto: proyecto.id_proyecto,
      computo_metrico: []
    };
    this.DataService.post(`supervisiones`, data).then( response => {
      if (response) {
        this.irPlanilla(response.datos.id_supervision);
      }
    });
  }

  irPlanilla(idSupervision) {
      this.$state.go('planilla-proyecto',{ idSupervision });
  }

  esUsuarioEmpresa() {
    return !angular.isUndefined(this.usuario.nit);
  }

  puedeCrearPlanilla(proyecto) {
    if (proyecto.supervisiones.length > 0) {
      return proyecto.supervisiones[0].estado_supervision === 'EN_ARCHIVO';
    }
    return true;
  }

  puedeVerSupervision(proyecto) {
    if(proyecto) {
      return ['APROBADO','CERRADO','MODIFICADO','PARALIZADO'].indexOf(proyecto.estado_proyecto)>=0 && proyecto.supervisiones && proyecto.supervisiones.length > 0;
    }
    return false;
  }

  puedeVerModificacion(proyecto) {
    let enSupervision = false;
    if(proyecto) {
      if(proyecto.supervisiones) {
        enSupervision = proyecto.supervisiones.find((sup)=>{ return sup.estado_supervision=='EN_FISCAL'; });
      }
      return ['SUPERVISOR','TECNICO_UPRE','RESP_DEPARTAMENTAL_UPRE','ENCAR_REGIONAL_UPRE','EMPRESA'].indexOf(this.usuario.rol.nombre)>=0 && !enSupervision
              && ['APROBADO','REVISION_MODIFICACION','MODIFICADO','CERRADO','PARALIZADO'].indexOf(proyecto.estado_actual.codigo)>=0;
    }
    return false;
  }

  puedeParalizar(proyecto) {
    if(proyecto) {
      return ['TECNICO_UPRE','RESP_DEPARTAMENTAL_UPRE'].indexOf(this.usuario.rol.nombre)>=0
              && ['APROBADO'].indexOf(proyecto.estado_actual.codigo)>=0;
    }
    return false;
  }
}

export default ListadoController;
