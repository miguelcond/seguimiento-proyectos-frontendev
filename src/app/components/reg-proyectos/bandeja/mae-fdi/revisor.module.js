'use strict';

import Proyectos from './proyectos/proyectos.module';

const Revisor = angular
    .module('app.revisor', [
        Proyectos,
    ])
    .name;

export default Revisor;
