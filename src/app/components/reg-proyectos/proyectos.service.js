/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class ProyectoService {
  constructor(Storage, DataService, $q, Datetime, Util, $window, _) {
    'ngInject';
    this.Storage = Storage;
    this.DataService = DataService;
    this.Datetime = Datetime;
    this.estadoActual = null;
    this.Util = Util;
    this.$window = $window;
    this.$q = $q;
    this.proyectoOrigen = {};
    this._ = _;
  }

  setCopia(proyecto) {
    this.proyectoOrigen = angular.copy(proyecto);
  }

  setEstadoActual(estado) {
    this.estadoActual = estado;
    this.prepararDatosEstado(this.estadoActual);
  }

  getEstadoActual() {
    return this.estadoActual;
  }

  get(idProyecto, procesado) {
    if (!procesado) procesado = '';
    let url = `proyectos/${idProyecto}/${procesado}`;
    url += `?rol=${this.Storage.get('rol').id_rol}`;
    let defer = this.$q.defer();
    this.DataService.get(url).then( resp => {
      if (resp) {
        this.setCopia(resp.datos);
      }
      defer.resolve(resp);
    });
    return defer.promise;
  }

  /**
   * prepararDatosEstado - Convierte el array a objetos (de atributos, requeridos y áreas) para hacer más simple la búsqueda por atributos o campos
   *
   * @param  {type} estado Estado actual del proyecto
   */
  prepararDatosEstado(estado) {
    if (this._.isEmpty(estado)) {
      return;
    }
    if (estado.atributos) estado.atributos = this.Util.arrayToObject(estado.atributos);
    if (estado.requeridos) estado.requeridos = this.Util.arrayToObject(estado.requeridos);
    if (estado.areas) estado.areas = this.Util.arrayToObject(estado.areas);
  }

  /**
   * esEditable - Verifica si el campo es editable de acuerdo a los datos del estado actual del proyecto
   *
   * @param  {string} nombreCampo nombre del campo a verificar
   * @return {boolean}             true si el campo se encuentra en la lista permitida de acuerdo a la etapa actual
   */
   esEditable(nombreCampo) {
     if (this.Util.isEmpty(this.estadoActual) || this.Util.isEmpty(this.estadoActual.atributos)) {
       return;
     }
     return this.estadoActual.atributos[nombreCampo] ? true : false;
   }

  /**
   * esRequerido - Verifica si el campo es requerido de acuerdo a los datos del estado actual del proyecto
   *
   * @param  {string} nombreCampo nombre del campo a verificar
   * @return {boolean}             true si el campo se encuentra en la lista permitida de acuerdo a la etapa actual
   */
  esRequerido(nombreCampo) {
    if (this.Util.isEmpty(this.estadoActual) || this.Util.isEmpty(this.estadoActual.requeridos)) {
      return;
    }
    return this.estadoActual.requeridos[nombreCampo] ? true : false;
  }

  /**
   * getDatosSegunEstado - Preparando datos para enviar, de acuerdo a los atributos que se registran
   * según el estado del proyecto
   *
   * @param  {object} proyecto objeto con datos del proyecto, entre ellos el estado_actual
   * @return {object}          objeto con los atributosa enviar según el estado actual del proyecto
   */
  getDatosSegunEstado(proyecto) {
    let nProyecto = {};
    const ATRIBUTOS = Array.isArray(proyecto.estado_actual.atributos) ? proyecto.estado_actual.atributos : this._.keys(proyecto.estado_actual.atributos);
    ATRIBUTOS.map(atributo => {
      if (atributo === 'fcod_municipio' && proyecto.dpa && proyecto[atributo] !== proyecto.dpa.municipio.codigo) {
        nProyecto[atributo] = proyecto.dpa.municipio.codigo;
      }
      if (!this.esAtributoModificado(atributo, proyecto)) {
        return;
      }
      if (!this.Util.isEmpty(proyecto[atributo])) {
        nProyecto[atributo] = proyecto[atributo];
      }
    });
    return nProyecto;
  }

  /**
   * esAtributoModificado - Verificando si el atributo fue modificado, la comparación
   * se realiza entre los datos del proyecto en edición y los datos antes de su edición
   *
   * @param  {string} atributo nombre del atributo a verificar
   * @param  {object} proyecto objeto con datos del proyecto en edición
   * @return {boolean}          true si el atributo fue modificado y false en caso contrario
   */
  esAtributoModificado(atributo, proyecto) {
    return !angular.equals(this.proyectoOrigen[atributo], proyecto[atributo]);
  }

  /**
   * esAreaPermitida - Verifica si un área determinada es permitida para su visualización
   * de acuerdo al estado del proyecto (datos obtenidos en el servicio)
   *
   * @param  {String} area Nombre del área a verificar
   * @return {Boolean}      true si el area se encuentra en la lista de áreas habilitadas
   * y false en caso contrario
   */
   esAreaPermitida(area) {
     if (this.Util.isEmpty(this.estadoActual) || this.Util.isEmpty(this.estadoActual.areas)) {
       return;
     }
     return this.estadoActual.areas[area] ? true : false;
   }

  tratarComoProductivo(proyecto) {
    if (proyecto) {
      return proyecto.beneficiarios.tipo === 'OP_TP_PRODUCTIVO' || proyecto.beneficiarios.tipo === 'OP_TP_MAQUINARIA';
    }
    return false;
  }
}

export default ProyectoService;
