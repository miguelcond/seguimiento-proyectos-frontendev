'use strict';

import modalArchivoController from '../../../proyectos/c-proyectos/items/archivo.modal.controller';
import modalArchivoTemplate from '../../../proyectos/c-proyectos/items/archivo.modal.html';

class ModuloController {
  constructor(DataService, Util,  Modal, NgTableParams, Message, $log) {
    'ngInject';
    this.DataService = DataService;
    this.Util = Util;
    this.Modal = Modal;
    this.NgTableParams = NgTableParams;
    this.Message = Message;
    this.$log = $log;

    if(!this.options) { // Configuracion por defecto
      this.options = {
        guardar: true,
        adicionar: true,
        editar: true,
        eliminar: true
      };
    }
  }

  $onInit() {
    this.getTipoUnidades();
  }

  getTipoUnidades() {
    this.DataService.get(`parametricas/UNIDAD`).then(response => {
      if (response) {
        this.unidades = response.datos.rows;
      }
    });
  }

  /*
  * MODULO
  */
  guardarDatosModulo() {
  }

  adicionarItem(modulo) {
    this.$log.log('adicionarItem', modulo);
    if (!modulo.items) {
      modulo.items = [];
    }
    modulo.items.push({edicion: true});
  }

  editarModulo() {
  }

  eliminarModulo() {
    if(this.modulo) {
      this.Modal.confirm(`¿Está seguro/a de eliminar el nuevo módulo y los items adicionados?`,
        ()=>{
          this.DataService.delete(`modulos/${this.modulo.id_modulo}`).then( (result) => {
            if (result && result.finalizado) {
              this.Message.success(result.mensaje);
              this.modulo = null;
            }
          });
        },null,'Eliminar el nuevo módulo','Sí, estoy seguro/a');
    }
  }


  /*
  * ITEMS
  */
  guardarItem(item) {
    this.$log.log('guardarItem',item);
    item.fid_modulo = this.modulo.id_modulo;
    this.DataService.post(`items`, item).then( (result) => {
      if (result && result.finalizado) {
        this.Message.success(result.mensaje);
        item.id_item = result.datos.id_item;
        item.edicion = false;
        this.callback({data:item});
      }
    });
  }

  actualizarItem(item) {
    this.$log.log('actualizarItem',item);
    this.DataService.put(`items/${item.id_item}`, item).then( (result) => {
      if (result && result.finalizado) {
        this.Message.success(result.mensaje);
        item.edicion = false;
        this.callback({data:item});
      }
    });
  }

  verPdfItem(item, codigo) {
    if(codigo && (codigo=='ET'||codigo=='APU')) {
      let datos = {
        codigo: `${codigo}_${item.id_item}`,
        titulo: codigo==='ET'? `Especificaciones Técnicas - "${item.nombre}"`: `Analisis de Precios Unitarios - "${item.nombre}"`
      };
      this.Modal.showDocumentBase64(`proyectos/${this.options.idProyecto}/doc/${datos.codigo}`, datos.titulo);
    }
  }

  cargarPdfItem(item, codigo) {
    if(codigo && (codigo=='ET'||codigo=='APU')) {
      let titulo = (codigo==='ET')? `Especificaciones Técnicas - "${item.nombre}"`: `Analisis de Precios Unitarios - "${item.nombre}"`;
      const confArchivo = {
        formatos: 'pdf',
        tipos: '.pdf'
      };
      this.mostrarModal(titulo, 'fa-upload', confArchivo, modalArchivoTemplate, modalArchivoController, 'Cargar documento')
      .result.then( docEspecificaciones => {
        if (angular.isUndefined(docEspecificaciones)) {
          return;
        }
        let datos = {};
        if(codigo==='ET') datos.especificaciones = docEspecificaciones;
        if(codigo==='APU') datos.analisis_precios = docEspecificaciones;
        this.DataService.put(`items/${item.id_item}`, datos).then(result => {
          if (result && result.finalizado) {
            this.Message.success(`Se registró el documento del item "${item.nombre}" correctamente.`);
            // item.especificaciones = docEspecificaciones;
            if(codigo==='ET') item.especificaciones = docEspecificaciones;
            if(codigo==='APU') item.analisis_precios = docEspecificaciones;
          }
        });
      });
    }
  }

  eliminarItem(item, $index) {
    if (!item.id_item) {
      this.modulo.items.splice($index, 1);
      this.callback({data:item});
      return;
    }
    this.Modal.confirm(`¿Está seguro/a de eliminar el item ${item.nombre}?`,
      ()=>{
        this.DataService.delete(`items/${item.id_item}`).then(result => {
          if (result && result.finalizado) {
            this.Message.success(result.mensaje);
            this.modulo.items.splice($index, 1);
            this.callback({data:item});
          }
        });
      },null,'Eliminar item','Sí, estoy seguro/a');
  }

  // Modal
  mostrarModal(titulo, icon, data, template, controller, labelOk, size) {
    return  this.Modal.show({
      template: template,
      controller: controller,
      data: data || {},
      title: titulo,
      icon: icon,
      size: size?size:'md',
      labelOk: labelOk
    });
  }
}

export default ModuloController;
