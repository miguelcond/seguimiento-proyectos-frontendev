'use strict';

import avanceProyectoComponent from './avance-proyecto.component';

const avanceProyecto = angular
    .module('app.avanceProyecto', [])
    .component('avanceProyecto', avanceProyectoComponent)
    .config(($stateProvider) => {
      $stateProvider
          .state('avance-proyecto', {
            url: '/avance-proyecto/:matricula/:idProyecto',
            component: 'avanceProyecto'
          });
    })
    .name;

export default avanceProyecto;
