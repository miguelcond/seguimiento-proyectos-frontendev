'use strict';

import modalConfirmacionController from './confirmacion.modal.controller.js';
import modalConfirmacionTemplate from './confirmacion.modal.html';

class DocumentacionProyectoController {
  constructor($scope, $stateParams, Storage, DataService, Message, WizardHandler, Modal, _, $q, $state, Proyecto, Loading) {
    'ngInject';

    this.$scope = $scope;
    this.$stateParams = $stateParams;
    this.DataService = DataService;
    this.Message = Message;
    this.WizardHandler = WizardHandler;
    this.Modal = Modal;
    this._ = _;
    this.$q = $q;
    this.$state = $state;
    this.Proyecto = Proyecto;
    this.Loading = Loading;
    this.idProyecto = this.$stateParams.codigo;
    this.urlProyectos = `proyectos/${this.idProyecto}`;
    this.usuario = Storage.getUser();
    this.tieneDocumentacion = false;
    this.estaCompleto = false;
  }

  $onInit() {
    this.getDatosProyectoAdjudicado();
    this.$scope.$on('refrescar-documentacion-proyecto', () => {
      this.$state.reload();
      this.WizardHandler.wizard().goTo(1);
    });
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.tieneDocumentacion', () => {
      if (!this.tieneDocumentacion && this.WizardHandler.wizard()) {
        this.WizardHandler.wizard().reset();
      }
    });
  }

  getDatosProyectoAdjudicado() {
    this.DataService.get(`proyectos/empresas/${this.usuario.matricula}/${this.idProyecto}`)
    .then( response => {
      if (response) {
        this.proyecto = response.datos;
        this.esEmpresa = (this.proyecto.documentacion_exp&&this.proyecto.documentacion_exp.datos_asociacion&&this.proyecto.documentacion_exp.datos_asociacion.id_asociacion_accidental)?false:true;
        this.Proyecto.setEstadoActual(this.proyecto.estado_actual);
        this.esModoEdicion = this.Proyecto.esEditable('plazo_ejecucion');
        if (this.existeFormulario()) {
          this.codigoDocumentoProy = angular.copy(this.proyecto.cod_documentacion_exp);
        }
      }
    });
  }

  ejecutarAccionPaso() {
    const paso = this.WizardHandler.wizard().currentStepNumber();
    switch (paso) {
      case 1:
        this.verificarCodigoYContrato();
        break;
      default:
      this.siguiente();
    }
  }

  verificarCodigoYContrato() {
    if (!this.proyecto.cod_documentacion_exp) {
      this.Message.warning(`Debe registrar el código y el número de invitación.`);
      return;
    }
    if (!this.tieneDocumentacion) {
      return this.relacionarFormDocumentacionEmpresaAProyecto();
    }
    this.siguiente();
  }

  relacionarFormDocumentacionEmpresaAProyecto() {
    if (this.existeFormulario() && this.codigoDocumentoProy === this.proyecto.cod_documentacion_exp) {
      this.tieneDocumentacion = true;
      this.siguiente();
      return;
    }
    const data = {
      cod_documentacion_exp: this.proyecto.cod_documentacion_exp,
      nro_invitacion: this.proyecto.nro_invitacion
    };
    const ITEMS = this.proyecto.items ? this.proyecto.items : [];
    const FORMULARIOS = this.proyecto.formularios ? this.proyecto.formularios : [];
    const MONTO_TOTAL = this.proyecto.monto_total ? this.proyecto.monto_total : 0;
    this.Loading.show('Obteniendo datos del formulario.', true);
    this.DataService.put(this.urlProyectos, data).then( result => {
      if (result) {
        this.proyecto = result.datos;
        this.proyecto.items = ITEMS;
        this.proyecto.formularios = FORMULARIOS;
        this.proyecto.monto_total = MONTO_TOTAL;
        this.Message.success(`Se obtuvo datos de la documentación de la empresa correctamente.`);
        this.tieneDocumentacion = true;
        this.codigoDocumentoProy = angular.copy(this.proyecto.cod_documentacion_exp);
        this.esEmpresa = (this.proyecto.documentacion_exp&&this.proyecto.documentacion_exp.datos_asociacion&&this.proyecto.documentacion_exp.datos_asociacion.id_asociacion_accidental)?false:true;
        this.siguiente();
        this.Loading.hide();
      }
    });
  }

  validarPresupuesto(contextObj) {
    if(contextObj.step1Valid){
       return true;
   } else {
       return false;
   }
  }

  confirmacionGeneracion() {
    let defer = this.$q.defer();
    if (!this.puedeGenerarFormularios()) {
      defer.reject();
    } else {
      const modalConfirmacion =  this.Modal.show({
        template: modalConfirmacionTemplate,
        controller: modalConfirmacionController,
        data: this.proyecto,
        size: 'lg'
      });
      modalConfirmacion.result.then( (proyecto) => {
        this.proyecto = proyecto;
        defer.resolve();
      }, () => { });
    }
    return defer.promise;
  }

  puedeGenerarFormularios() {
    if (this.existeFormulario() && this.tienePresupuestoCompleto() && this.proyecto.monto_total && this.tieneCronogramas()) {
      return true;
    } else {
      this.Modal.alert('Debe completar los precios de los items en Presupuesto y cargar los archivos en Cronogramas.');
    }
    return false;
  }

  esDocumentacionCompleta() {
    if (this.existeFormulario() && this.tienePresupuestoCompleto() && this.proyecto.monto_total && this.tieneCronogramas()) {
      return true;
    }
    return false;
  }

  tienePresupuestoCompleto() {
    const PRECIOS_ITEMS = this._.map(this.proyecto.items, 'precio_unitario');
    return this._.indexOf(PRECIOS_ITEMS, undefined) === -1;
  }

  tieneCronogramas() {
    return this.proyecto.formularios &&
           this.existeDocumento('A8') &&
           this.existeDocumento('B5') &&
           this.existeDocumento('B2');
  }

  existeDocumento(codigo) {
    return !this._.isEmpty(this._.find(this.proyecto.formularios, {codigo_documento: codigo}));
  }

  verificarSiTieneForm() {
    if (this.WizardHandler.wizard() && this.existeFormulario()) {
        this.WizardHandler.wizard().setEditMode();
    }
  }

  siguiente() {
    this.WizardHandler.wizard().next();
  }

  anterior() {
    this.WizardHandler.wizard().previous();
  }

  existeFormulario() {
    if (!this.proyecto) {
      return false;
    }
    if (this.proyecto.cod_documentacion_exp && this.proyecto.documentacion_exp) {
      return true;
    }
    return false;
  }

  getRolUsuario() {
    let id_rol = null;
    if (this.usuario.rol) {
      id_rol = this.usuario.rol.id_rol;
    } else {
      id_rol = this.usuario.matricula? 8 : 0;
    }
    return id_rol;
  }

  // esModoEdicion() {
  //   if (!this.proyecto) {
  //     return;
  //   }
  //   return this.Proyecto.esEditable('plazo_ejecucion');
  // }

}

export default DocumentacionProyectoController;
