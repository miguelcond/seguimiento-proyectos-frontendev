'use strict';

class SupervisionService {
  constructor(DataService, $q, Datetime, Util, $window, _) {
    'ngInject';
    this.DataService = DataService;
    this.Datetime = Datetime;
    this.estadoActual = null;
    this.Util = Util;
    this.$window = $window;
    this.$q = $q;
    this.proyectoOrigen = {};
    this._ = _;

  }

  setSupervision(pSupervision) {
    this.supervision = angular.copy(pSupervision);
    this.supervision.estado_actual.areas = this._.zipObject(this.supervision.estado_actual.areas, Array.from(new Array(this.supervision.estado_actual.areas.length), () => true));
  }

  getSupervision() {
    return this.supervision || null;
  }

  isSetSupervision() {
    return angular.isDefined(this.supervision);
  }

  setUsuario(pUsuario){
    this.usuario = angular.copy(pUsuario);
    this.id_rol = null;
    if (this.usuario.rol) {
      this.id_rol = this.usuario.rol.id_rol;
    } else {
      this.id_rol = this.usuario.matricula? 8 : 0;
    }
  }

  esRolPermitido() {
    return this.supervision.estado_actual.roles.indexOf(this.id_rol)!==-1;
  }

  esRolEstadoSupervisor() {
    return this.supervision.estado_supervision === 'EN_SUPERVISOR' && this.id_rol == 9;
  }

  /**
   * getItems - Prepara y envía una lista de items que tienen el atributo show = true (que se va asignando de acuerdo a los filtros de la planilla)
   *
   * @return {array}  Lista de items filtrados por el atributo show = true
   */
  getItems() {
    if (!this.supervision) {
      return null;
    }
    let items = [];
    this.supervision.modulos.map(modulo => {
      if (modulo.show) {
        items = this._.unionWith(items, this._.reject(modulo.items, { 'show': false }));
      }
    });
    return this._.orderBy(items, 'nro_item', 'asc');
  }

  getDatosSegunEstado(supervision) {
    let nSupervision = {};
    const atributos = Array.isArray(supervision.estado_actual.atributos) ? supervision.estado_actual.atributos : this._.keys(supervision.estado_actual.atributos);
    atributos.map(atributo => {
      if (!this.Util.isEmpty(supervision[atributo])) {
        nSupervision[atributo] = supervision[atributo];
      }
    });
    return nSupervision;
  }

  esAreaPermitida(area) {
    if (this.Util.isEmpty(this.supervision)) {
      return;
    }
    return this.supervision.estado_actual.areas[area] ? true : false;
  }

  esEditable(atributo) {
    return this.supervision.estado_actual.atributos[atributo] ? true : false;
  }

  calcularDescuento(tipo, montoEjecutado, deudaAnticipo, porcentajeAnticipo) {
    const DESCUENTO = {};
    switch (tipo) {
      case 'TOTAL':
        DESCUENTO.descuento = deudaAnticipo;
        DESCUENTO.total = this.Util.restar(montoEjecutado, deudaAnticipo);
        break;
      case 'PROGRESIVO':
        DESCUENTO.descuento = this.Util.getPorcentaje(montoEjecutado, porcentajeAnticipo);
        DESCUENTO.total = this.Util.restar(montoEjecutado, DESCUENTO.descuento);
        break;
    }
    return DESCUENTO;
  }
}

export default SupervisionService;
