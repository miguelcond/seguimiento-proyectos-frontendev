'use strict';

import computoProyectoComponent from './computo-proyecto.component';
import resumenComputosComponent from './resumen/resumen.component';
import fotosComputosComponent from './fotos/fotos.component';
import computosMetricosComponent from './computos-metricos/computos-metricos.component';
import ComputoService from './computo.service';

const computoProyecto = angular
    .module('app.computoProyecto', [])
    .component('computoProyecto', computoProyectoComponent)
    .component('resumenComputos', resumenComputosComponent)
    .component('fotosComputos', fotosComputosComponent)
    .component('computosMetricos', computosMetricosComponent)
    .service('Computo', ComputoService)
    .name;

export default computoProyecto;
