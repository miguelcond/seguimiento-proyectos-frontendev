'use strict';

class ComputoProyectoController {
  constructor($scope, DataService, Supervision, Computo, _, Util, Loading) {
    'ngInject';
    this.$scope = $scope;
    this.DataService = DataService;
    this.Supervision = Supervision;
    this.Computo = Computo;
    this._ = _;
    this.Util = Util;
    this.Loading = Loading;
    this.items = [];
    this.esEditable = false;
    this.supervisionHistorica = {};
    this.computoFinalizado = true;
  }

  $onInit() {
    this.modificados = [];
    this.idSupervision = this.supervision.id_supervision;

    this.resumen = {
      cant_ejecutada: 0,
      cant_faltante: 0,
      cant_anterior: 0 // cantidad total de las planillas anteriores
    };

    this.inicializarDatos();
    this.items = this.Supervision.getItems();
  }

  inicializarDatos() {
    this.getDatosItem();
    this.supervisiones = [];
    this.supervisionHistorica = {};
    this.computoFinalizado = true;
  }

  getDatosItem() {
    this.Loading.show(`Cargando datos del item.`, true);
    this.DataService.get(`items/${this.item.id_item}/resumen`).then(response => {
      this.item = response.datos;
      this.getSupervisionesItem();
      this.getComputosItem(this.supervision.id_supervision);
      this.$scope.$broadcast('recargarFotos');
    });
  }

  /**
   * getSupervisionesItem - Obteniendo las supervisiones en las que se han registrado cómputos métricos de un item
   */
  getSupervisionesItem() {
    this.DataService.get(`items/${this.item.id_item}/supervisiones`).then(response => {
      this.prepararSupervisiones(response.datos);
    });
  }

  getComputosItem(idSupervision) {
    this.DataService.get(`computos_metricos?item=${this.item.id_item}&supervision=${idSupervision}`)
    .then(response => {
      this.resumen.cant_anterior = response.datos.cantidad_anterior;
      this.computos = this.Computo.getListaOrdenadaComputos(response.datos.computos, this.item);
      this.copiaComputos = angular.copy(this.computos);
      this.procesarFotos(response.datos.fotos);
      this.$scope.$broadcast('recargarDatosComputos');
      this.actualizarResumen();
      this.establecerEdicion();
      this.Loading.hide();
    });
  }

  procesarFotos(fotos) {
    fotos.forEach( foto => {
      foto.name = foto.nombre.substring(0, foto.nombre.indexOf('_'));
      foto.nombre = foto.nombre.substring(foto.nombre.indexOf('_')+1);
      foto.show = true;
      this.fotos[foto.name] = foto;
      //guardamos backup para recuperar el id_foto_supervision
      this.fotos.backups[foto.name] = angular.copy(this.fotos[foto.name]);
    });
  }

  actualizarResumen() {
    this.resumen = this.Computo.getResumen(this.item, this.computos, this.resumen);
  }

  establecerEdicion() {
    this.esEditable = this.Supervision.esEditable('computos') && this.item.cantidad > 0 &&
    this.Supervision.getSupervision().id_supervision === this.supervisionHistorica.id_supervision;
  }

  irItemPosicion(opcion) {
    if (!opcion) {
      return;
    }
    if (this.existeCambios) {
      this.$scope.$broadcast('guardarComputosMetricos', () => {
        this.siguienteAnterior(opcion);
      });
    } else {
      this.siguienteAnterior(opcion);
    }
  }

  siguienteAnterior(opcion) {
    const POSICION = this._.findIndex(this.items, { id_item: parseInt(this.item.id_item) });
    if ((opcion === 'siguiente' && POSICION === this.items.length - 1) || (opcion === 'anterior' && POSICION === 0)) {
      return;
    } else {
      const POSICION_SIGUIENTE = opcion === 'siguiente'? POSICION + 1 : opcion === 'anterior'? POSICION -1 : POSICION;
      this.item = this.items[POSICION_SIGUIENTE];
      this.inicializarDatos();
    }
  }

  esPrimerItem() {
    return this._.findIndex(this.items, {id_item: this.item.id_item}) === 0;
  }

  esUltimoItem() {
    return this._.findIndex(this.items, {id_item: this.item.id_item}) === this.items.length -1;
  }

  prepararSupervisiones(supervisiones) {
    if (this.supervision.estado_supervision === 'REGISTRO_PLANILLA') {
    const SUPERVISION_ACTUAL = {id_supervision: this.supervision.id_supervision, estado_supervision: this.supervision.estado_supervision};
    if (supervisiones.length === 0) {
      supervisiones = [SUPERVISION_ACTUAL];
    } else {
      const FINALIZADO = this._.find(supervisiones, {estado_supervision: 'EN_ARCHIVO'});
      if (FINALIZADO && !this.item.completado) {
        supervisiones.push(SUPERVISION_ACTUAL)
      }
    }
    }

    supervisiones.map(supervision => {
      if (supervision.estado_supervision === 'REGISTRO_PLANILLA') {
        Object.assign(supervision, {nombre: `En proceso`});
        this.asignarValorSupervHistorica(supervision);
      } else {
        if (supervision.nro_supervision) {
          Object.assign(supervision, {nombre: `Planilla ${supervision.nro_supervision}`});
        }
      }
    });
    this.supervisiones = supervisiones;
    if (!this.esEditable) {
      const SUPERVISION_ACTUAL = this._.find(this.supervisiones, {id_supervision: this.supervision.id_supervision});
      if (SUPERVISION_ACTUAL) {
        this.asignarValorSupervHistorica(SUPERVISION_ACTUAL);
      }
    }
  }

  asignarValorSupervHistorica(supervision) {
    this.supervisionHistorica = supervision;
    this.verificarComputoFinalizado();
  }

  mostrarDatosSupervision() {
    this.verificarComputoFinalizado();
    this.getComputosItem(this.supervisionHistorica.id_supervision);
    this.$scope.$broadcast('recargarFotos');
  }

  verificarComputoFinalizado() {
    this.computoFinalizado = this.supervisionHistorica.estado_supervision === 'EN_ARCHIVO';
  }

  puedeVerComputos() {
    return (this.supervision.estado_supervision === 'REGISTRO_PLANILLA' ? true : this.resumen.total_actual > 0 && this.item.estado !== 'CERRADO')
    || (this.supervisionHistorica.id_supervision && this.supervisionHistorica.id_supervision !== this.supervision.id_supervision);
  }

}

export default ComputoProyectoController;
