'use strict';

class ComputosService {
  constructor(DataService, $q, Datetime, Util, $window, _, Big, decimales) {
    'ngInject';
    this.DataService = DataService;
    this.Datetime = Datetime;
    this.estadoActual = null;
    this.Util = Util;
    this.$window = $window;
    this.$q = $q;
    this.proyectoOrigen = {};
    this._ = _;
    this.Big = Big;
    this.decimales = decimales;
  }

  setCantParcial(unidad, computo) {
    unidad = unidad || '';
    unidad = unidad.toUpperCase();
    let cantidadParcial = 0;
    let cc = this.Big(computo.cantidad?computo.cantidad:0);
    let cff = this.Big(computo.factor_forma?computo.factor_forma:0);
    let clargo = this.Big(computo.largo?computo.largo:0);
    let calto = this.Big(computo.alto?computo.alto:0);
    let cancho = this.Big(computo.ancho?computo.ancho:0);
    switch (unidad) {
      case 'UN_GLOBAL':
      case 'UN_PIEZA':
      case 'UN_PUNTO':
        cantidadParcial = computo.cantidad;
      break;
      case 'UN_METRO':
        cantidadParcial = cc.mul(clargo).toString();
      break;
      case 'UN_METRO_CUAD':
      // con redondeos intermedios
        // computo.area = cff.mul(clargo.mul(calto).toFixed(this.decimales||2)).toFixed(this.decimales||2).toString();
        // cantidadParcial = cc.mul(computo.area).toString();
      // sin redondeos intermedios
        computo.area = cff.mul(clargo.mul(calto)).toFixed(this.decimales||2).toString();
        cantidadParcial = cc.mul(cff).mul(clargo).mul(calto).toFixed(this.decimales||2).toString();
      break;
      case 'UN_METRO_CUB':
      // con redondeos intermedios
        // computo.area = cff.mul(clargo.mul(cancho).mul(calto).toFixed(this.decimales||2)).toFixed(this.decimales||2).toString();
        // cantidadParcial = cc.mul(computo.area).toString();
      // sin redondeos intermedios
        computo.area = cff.mul(clargo).mul(cancho).mul(calto).toFixed(this.decimales||2).toString();
        cantidadParcial = cc.mul(cff).mul(clargo).mul(cancho).mul(calto).toFixed(this.decimales||2).toString();
      break;
    }
    computo.cantidad_parcial = cantidadParcial;
  }

  getConf(unidad) {
    unidad = unidad || '';
    unidad = unidad.toUpperCase();
    var val = {}; // valor a retornar
    switch (unidad) {
      case 'UN_GLOBAL':
        val = {
          descripcion: { required:true, disabled:false },
          cantidad: { required:true, disabled:false },
          factor_forma: { required:false, disabled:true },
          largo: { required:false, disabled:true },
          ancho: { required:false, disabled:true },
          alto: { required:false, disabled:true },
          area: { required:false, disabled:true },
          cantidad_parcial: { required:true, disabled:true, class:'bg-white' }
        };
      break;
      case 'UN_PIEZA':
      case 'UN_PUNTO':
        val = {
          descripcion: { required:true, disabled:false },
          cantidad: { required:true, disabled:false },
          factor_forma: { required:false, disabled:true },
          largo: { required:false, disabled:true },
          ancho: { required:false, disabled:true },
          alto: { required:false, disabled:true },
          area: { required:false, disabled:true },
          cantidad_parcial: { required:true, disabled:true, class:'bg-white' }
        };
      break;
      case 'UN_METRO':
        val = {
          descripcion: { required:true, disabled:false },
          cantidad: { required:true, disabled:false },
          factor_forma: { required:false, disabled:true },
          largo: { required:true, disabled:false },
          ancho: { required:false, disabled:true },
          alto: { required:false, disabled:true },
          area: { required:false, disabled:true },
          cantidad_parcial: { required:true, disabled:true, class:'bg-white' }
        };
      break;
      case 'UN_METRO_CUAD':
        val = {
          descripcion: { required:true, disabled:false },
          cantidad: { required:true, disabled:false },
          factor_forma: { required:true, disabled:false },
          largo: { required:true, disabled:false },
          ancho: { required:false, disabled:true },
          alto: { required:true, disabled:false },
          area: { required:true, disabled:true, class:'bg-white' },
          cantidad_parcial: { required:true, disabled:true, class:'bg-white' }
        };
      break;
      case 'UN_METRO_CUB':
        val = {
          descripcion: { required:true, disabled:false },
          cantidad: { required:true, disabled:false },
          factor_forma: { required:true, disabled:false },
          largo: { required:true, disabled:false },
          ancho: { required:true, disabled:false },
          alto: { required:true, disabled:false },
          area: { required:true, disabled:true, class:'bg-white' },
          cantidad_parcial: { required:true, disabled:true, class:'bg-white' }
        };
      break;
    }
    return val;
  }

  getListaOrdenadaComputos(computos, item) {
    if (computos.length==0) {
      computos.push({});
    } else {
      // vamos a ordenar por subtitulo
      var indexs_subtitulos = {}; // buscaremos todos los subtitulos y luego los añadiremos;
      var computo_auxiliar = computos[0];
      indexs_subtitulos[0] = computo_auxiliar.subtitulo;
      computos.forEach( (computo, i) => {
        if (computo.subtitulo != computo_auxiliar.subtitulo) {
          indexs_subtitulos[i] = computo.subtitulo;
          computo_auxiliar = computo;
        }
        if (item.unidad_medida === 'UN_METRO_CUB') {
          computo.area = computo.volumen;
        }
      })
      var i = 0;
      for (var k in indexs_subtitulos) {
        if (indexs_subtitulos[k]) {
          computos.splice(parseInt(k)+i, 0, { isSubtitulo:true, descripcion:indexs_subtitulos[k] });
          i++;
        }
      }
    }
    return computos;
  }

  getResumen(item, computos, resumen) {
    resumen.total_actual = 0;
    computos.forEach( computo => {
      if (!computo.isSubtitulo) {
        resumen.total_actual = this.Util.sumar([resumen.total_actual, parseFloat(computo.cantidad_parcial) || 0])
      }
    });
    resumen.cant_ejecutada = this.Util.sumar([resumen.total_actual, resumen.cant_anterior]);
    resumen.cant_faltante = item.cantidad > 0 ? this.Util.restar(item.cantidad, resumen.cant_ejecutada) : 0;
    return resumen;
  }

}

export default ComputosService;
