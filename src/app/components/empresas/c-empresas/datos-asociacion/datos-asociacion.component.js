'use strict';

import controller from './datos-asociacion.controller';
import template from './datos-asociacion.html';

const DatosAsociacionComponent = {
  bindings: {
    empresa: '=',
    asociacion: '='
  },
  controller,
  template
};

export default DatosAsociacionComponent;
