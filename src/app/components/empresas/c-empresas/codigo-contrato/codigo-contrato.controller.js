'use strict';

class CodigoContratoController {
  constructor(Modal) {
    'ngInject';

    this.Modal = Modal;
  }

  $onInit() {
    if (this.proyecto.cod_documentacion_exp) {
      this.tieneDocumentacion = true;
    }
  }

  cambiarDocumentacion() {
    this.Modal.confirm('¿Está seguro/a de modificar la información registrada de la empresa?', () => {
      this.tieneDocumentacion = false;
    });
  }

  cancelarCambio() {
    this.proyecto.cod_documentacion_exp = this.codigoDocumentacion;
    this.tieneDocumentacion = true;
  }

}

export default CodigoContratoController;
