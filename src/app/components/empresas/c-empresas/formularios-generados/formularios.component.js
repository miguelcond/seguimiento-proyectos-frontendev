'use strict';

import controller from './formularios.controller';
import template from './formularios.html';
import './formularios.scss';

const FormulariosGeneradosComponent = {
  bindings: {
    proyecto: '='
  },
  controller,
  template
};

export default FormulariosGeneradosComponent;
