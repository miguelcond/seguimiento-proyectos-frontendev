'use strict';

import controller from './datos-empresa.controller';
import template from './datos-empresa.html';

const DatosEmpresaComponent = {
  bindings: {
    empresa: '='
  },
  controller,
  template
};

export default DatosEmpresaComponent;
