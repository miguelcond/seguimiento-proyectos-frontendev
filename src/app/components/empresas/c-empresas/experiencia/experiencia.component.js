'use strict';

import controller from './experiencia.controller';
import template from './experiencia.html';

const ExperienciaEmpresaComponent = {
  bindings: {
    experiencias: '='
  },
  controller,
  template
};

export default ExperienciaEmpresaComponent;
