'use strict';

class ExperienciaEmpresaController {
  constructor(DataTable, _, Util, $scope) {
    'ngInject';

    this.DataTable = DataTable;
    this._ = _;
    this.Util = Util;
    this.$scope = $scope;
    this.totalDolares = 0;
    this.totalBolivianos = 0;
  }

  $onInit() {
    this.camposExperiencia = this.definirColumnasExperiencias();
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.experiencias', () => {
      this.calcularSumasDeMontos();
    }, true);
  }

  calcularSumasDeMontos() {
    this.totalesExpGeneral = this.calcularSumas(this.experiencias.generales);
    this.totalesExpEspecifica = this.calcularSumas(this.experiencias.especificas);
  }

  calcularSumas(experiencias) {
    let total = {
      dolares: null,
      bolivianos: null
    };
    if (this.tieneMontosEnDolaresCompletos(experiencias)) {
      total.dolares = this.calcularTotales(experiencias, 'monto_final_contrato_sus');
    }
    total.bolivianos = this.calcularTotales(experiencias, 'monto_final_contrato_bs');
    return total;
  }

  tieneMontosEnDolaresCompletos(experiencias) {
    return this._.isEmpty(this._.find(experiencias, {
      monto_final_contrato_sus: null
    }));
  }

  calcularTotales(experiencias, tipo) {
    return this.Util.redondear(this._.sum(this._.map(experiencias, tipo)));
  }

  definirColumnasExperiencias() {
    return [
      { title: 'Nombre del Contratante', field: 'nombre_contratante', filter: { nombre_contratante: "text" }, visible: true, sortable: "nombre_contratante" },
      { title: 'Dirección de Contacto', field: 'direccion_contacto', filter: { direccion_contacto: "text" }, visible: true, sortable: "direccion_contacto" },
      { title: 'Objeto de Contrato', field: 'objeto_contratacion', filter: { objeto_contratacion: "text" }, visible: true, sortable: "objeto_contratacion" },
      { title: 'Ubicación', field: 'ubicacion', filter: { ubicacion: "text" }, visible: true, sortable: "ubicacion" },
      { title: 'Monto Final de Contrato Bs', field: 'monto_final_contrato_bs', filter: { monto_final_contrato_bs: "number" }, visible: true, sortable: "monto_final_contrato_bs" },
      { title: 'Monto Final de Contrato $us', field: 'monto_final_contrato_sus', filter: { monto_final_contrato_sus: "number" }, visible: true, sortable: "monto_final_contrato_sus" },
      { title: 'Inicio', field: 'fecha_inicio_ejecucion', filter: { fecha_inicio_ejecucion: "text" }, visible: true, sortable: "fecha_inicio_ejecucion" },
      { title: 'Finalización', field: 'fecha_fin_ejecucion', filter: { fecha_fin_ejecucion: "text" }, visible: true, sortable: "fecha_fin_ejecucion" },
      { title: '% de Participación', field: 'participacion', filter: { participacion: "text" }, visible: true, sortable: "participacion" },
      { title: 'Nombre del Socio', field: 'socio', filter: { socio: "text" }, visible: true, sortable: "socio" },
      { title: 'Profesional Responsable', field: 'profesional_responsable', filter: { profesional_responsable: "text" }, visible: true, sortable: "profesional_responsable" }
    ];
  }

}

export default ExperienciaEmpresaController;
