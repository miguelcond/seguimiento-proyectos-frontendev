'use strict';

import controllerModal from './experiencia-empleado.modal.controller.js';
import templateModal from './experiencia-empleado.modal.html';

class EmpleadosController {
  constructor(DataTable, Modal, $scope) {
    'ngInject';
    this.DataTable = DataTable;
    this.Modal = Modal;
    this.$scope = $scope;
  }

  $onInit() {

  }

  $onChanges() {
    this.$scope.$watch('$ctrl.empleados', () => {
      this.iniciarDatos();
    }, true);
  }

  iniciarDatos() {
    this.tableParams = this.DataTable.getParamsList(this.getFormatoLista());
  }

  getFormatoLista() {
    let lista = [];
    this.empleados.map( empleado => {
      lista.push(this.formatearEstructuraDatos(empleado));
    });
    return lista;
  }

  formatearEstructuraDatos(datos) {
    return {
      nombre: datos.nombre,
      documento_identidad: datos.persona.documento_identidad,
      nacionalidad: datos.persona.nacionalidad,
      profesion: datos.profesion,
      nro_registro_profesional: datos.nro_registro_profesional,
      cargo_actual: datos.cargo_actual,
      experiencias: datos.experiencias
    }
  }

  verExperiencia(empleado) {
    this.Modal.show({
      template: templateModal,
      controller: controllerModal,
      data: {
        empleado: empleado
      },
      size: 'xlg'
    });
  }

}

export default EmpleadosController;
