'use strict';

class EmpresaAnteriorController {
  constructor($state, DataService, Storage) {
    'ngInject';
    this.$state = $state;
    this.DataService = DataService;
    this.Storage = Storage;
    this.user = Storage.getUser();
    this.historialEmpresas = false;
  }

  $onInit() {
    let idProyecto = this.supervision.fid_proyecto;
    this.DataService.get(`proyectos/${idProyecto}/empresas/avance`).then((resp)=>{
      if (resp && resp.finalizado) {
        this.historico = resp.datos;
        if(resp.datos&&resp.datos.proyectos.length>0) {
          this.historialEmpresas = true;
        }
      }
    });
  }

  verSupervision(avance) {
    this.$state.go('planilla-proyecto', {idSupervision:avance.supervisiones[0].id_supervision});
  }

}

export default EmpresaAnteriorController;
