'usr strict';

import controller from './planilla-proyecto.controller';
import template from './planilla-proyecto.html';
import './planilla-proyecto.scss';

const PlanillaProyectoComponent = {
  bindings: {},
  controller,
  template
};

export default PlanillaProyectoComponent;
