'use strict';

import controller from './fotos-supervision.controller';
import template from './fotos-supervision.html';
import './fotos-supervision.scss';

const FotosSupervisionComponent = {
  bindings: {
    supervision: '=',
    fotos: '<'
  },
  controller,
  template
};

export default FotosSupervisionComponent;
