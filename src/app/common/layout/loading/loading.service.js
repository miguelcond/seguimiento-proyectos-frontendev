'use strict';

class LoadingService {
    constructor(cfpLoadingBar) {
        'ngInject';

        this.cfpLoadingBar = cfpLoadingBar;
    }

    show(message, locked) {
        // this.cfpLoadingBar.start();
        // document.querySelector('#loading-bar-spinner .loading-text').innerHTML = message || 'Cargando';

        // if (locked) {
        //     document.querySelector('#loading-bar-spinner').classList.add('screen-locked');
        // } else {
        //     document.querySelector('#loading-bar-spinner').classList.remove('screen-locked');
        // }
        this.cfpLoadingBar.start();
        let loading1 = document.querySelector('#loading-bar-spinner .loading-text');
        if (loading1) loading1.innerHTML = message || 'Cargando';

        let loading2 = document.querySelector('#loading-bar-spinner');
        if (locked) {
            if (loading2) loading2.classList.add('screen-locked');
        } else {
            if (loading2) loading2.classList.remove('screen-locked');
        }
    }

    hide() {
        // this.cfpLoadingBar.complete();
        // if (document.querySelector('#loading-bar-spinner .loading-text')) {
        //   document.querySelector('#loading-bar-spinner .loading-text').innerHTML = 'Cargando';
        //   document.querySelector('#loading-bar-spinner').classList.remove('screen-locked');
        // }
        this.cfpLoadingBar.complete();
        let loading1 = document.querySelector('#loading-bar-spinner .loading-text');
        if (loading1) loading1.innerHTML = 'Cargando';
        let loading2 = document.querySelector('#loading-bar-spinner');
        if (loading2) loading2.classList.remove('screen-locked');
    }
}

export default LoadingService;
