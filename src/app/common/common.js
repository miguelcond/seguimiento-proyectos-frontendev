'use strict';

// Importando css de las librerías del sistema
import '../../../node_modules/bootstrap/dist/css/bootstrap.css';
import '../../../node_modules/font-awesome/css/font-awesome.css';
import '../../../node_modules/angular-loading-bar/build/loading-bar.css';
import '../../../node_modules/angular-toastr/dist/angular-toastr.min.css';
import '../../../node_modules/ui-select/dist/select.min.css';
import '../../../node_modules/leaflet/dist/leaflet.css';
import '../../../node_modules/angular-wizard/dist/angular-wizard.min.css';
import '../../../node_modules/angular-timeline/dist/angular-timeline.css';
// import '../../../node_modules/jsoneditor/dist/jsoneditor.min.css';
import './core/vendor/ng-table/ng-table.css';
import './styles/sass/index.scss';
import './styles/postcss/index.css';

// Importando dependencias
import uiRouter from '@uirouter/angularjs';
import satellizer from 'satellizer';
import loadingBar from 'angular-loading-bar';
import ngSanitize from 'angular-sanitize';
import ngMessages from 'angular-messages';
import ngResource from 'angular-resource';
import uiSelect from 'ui-select';
import toastr from 'angular-toastr';
import uiValidate from 'angular-ui-validate';
import 'lodash';
// import jsonEditor from 'jsoneditor';

import Big from '../../../node_modules/big.js';
// import 'tether'; // Tether (required for Bootstrap 4.x)
import '../../../node_modules/angular-formly/dist/formly';
import '../../../node_modules/angular-formly-templates-bootstrap/dist/angular-formly-templates-bootstrap';
import '../../../node_modules/angular-file-upload/dist/angular-file-upload';
//import '../../../node_modules/leaflet/dist/leaflet'; //<-- agregado mediante webpack.config
//import '../../../node_modules/angular-leaflet-directive/dist/angular-leaflet-directive';
//import '../../../node_modules/leaflet-fullscreen/dist/Leaflet.fullscreen';
import './core/vendor/ng-table/ng-table';
//import '../../../node_modules/pdfjs-dist/build/pdf.combined'; //<-- agregado mediante webpack.config
import '../../../node_modules/angular-wizard/dist/angular-wizard';
import '../../../node_modules/angular-timeline/dist/angular-timeline';
import '../../../node_modules/ng-csv/build/ng-csv.min';
import '../../../node_modules/angular-jsoneditor/dist/angular-jsoneditor';

import Config from './common.config';
import Run from './common.run';

import Lang from './lang/lang.module';
import Core from './core/core.module';
import Layout from './layout/layout.module';

const Common = angular
  .module('app.common', [
    loadingBar,
    ngSanitize,
    ngMessages,
    ngResource,
    satellizer,
    'angularFileUpload',
    'formlyBootstrap',
    toastr,
    'ngTable',
    uiRouter,
    uiSelect,
    Lang,
    Core,
    Layout,
    uiValidate,
    'cfp.loadingBar',
    'mgo-angular-wizard',
    'angular-timeline',
    'ngCsv',
    'angular-jsoneditor'
  ])
  .config(Config)
  .run(Run)
  .constant('statePrevs', {}) // Controlar state(url) previos para cada state, si no se define en un state trabajara normal
  .constant('_', window._)
  .constant('Big', Big)
  .name;

export default Common;
