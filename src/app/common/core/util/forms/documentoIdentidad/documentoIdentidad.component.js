'use strict';

import controller from './documentoIdentidad.controller';
import template from './documentoIdentidad.html';
import './documentoIdentidad.scss';

const documentoIdentidadComponent = {
    bindings: {
        ngModel: '=',
        onlyCi: '<?',
        ngId: '@?',
        conf: '<',
        url: '<',
        callbackSearch:'&',
        edadMaxima: '=?',
        edadMinima: '=?',
        modoVista: '<?'
    },
    controller,
    template
};

export default documentoIdentidadComponent;
