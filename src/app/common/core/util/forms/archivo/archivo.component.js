'use strict';

import controller from './archivo.controller';
import template from './archivo.html';
import './archivo.scss';

const ArchivoComponent = {
    bindings: {
      name: '@',
      label: '@',
      ngModel: '=',
      types: '@',
      mimeTypes: '@',
      maxsize: '@',
      modoVista: '<?',
      url: '@',
      urlDescarga: '@',
      apiUrl: '@',
      ver: '<',
      descargar: '<'
    },
    controller,
    template
};

export default ArchivoComponent;
