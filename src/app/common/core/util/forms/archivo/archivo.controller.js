'use strict';

class ArchivoController {
  constructor(Modal, apiUrl, $scope){
    'ngInject';

    this.Modal = Modal;
    this.api = apiUrl;
    this.$scope = $scope;
  }
  $onInit(){
    this.ver = angular.isUndefined(this.ver) ? true : this.ver;
    this.descargar = angular.isUndefined(this.descargar) ? true : this.descargar;
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.urlDescarga', () => {
      if (this.urlDescarga) {
        this.urlDescargaDocumento = angular.isUndefined(this.apiUrl)? `${this.api}${this.urlDescarga}` : `${this.apiUrl}${this.urlDescarga}`;
      }
    });
  }

  mostrarArchivo() {
    return !this.modoVista && typeof this.ngModel !== 'object' && !angular.isUndefined(this.ngModel);
  }

  vistaPrevia() {
    this.Modal.showDocumentBase64(this.url, this.label);
  }

}

export default ArchivoController;
