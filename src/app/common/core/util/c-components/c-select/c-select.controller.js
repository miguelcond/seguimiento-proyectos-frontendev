'use strict';

class CSelectController {
    constructor(Util) {
      'ngInject';
      this.Util = Util;
    }
    $onInit() {
        this.field = angular.isUndefined(this.field)? 'nombre' : this.field;
        this.placeholder = angular.isUndefined(this.placeholder)? 'Seleccione ...' : this.placeholder;
        this.establecerDatosPorDefecto();
    }

    /**
     * establecerDatosPorDefecto - Si en la lista de opciones solo hay un elemento,éste es asignado
     * a la variable que corresponde como dato por defecto. Además se asigna sólo si ngModel es indefinido.
     */
    establecerDatosPorDefecto() {
      if (this.options && this.options.length === 1 && angular.isUndefined(this.ngModel)) {
        if (this.keyValue) {
          this.ngModel = this.options[0][this.keyValue];
        } else {
          this.ngModel = this.options[0];
        }
      }
    }

}
export default CSelectController;
