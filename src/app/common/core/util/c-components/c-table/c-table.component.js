'use strict';

import controller from './c-table.controller';
import template from './c-table.html';

const CTableComponent = {
  bindings: {
          columns: '<',
          dataset: '<'
  },
  template,
  controller
};

export default CTableComponent;
