'use strict';

class ValidarFormularioAlerta {
  constructor() {
    'ngInject';
    this.restrict = 'A';
    this.controller = ValidarFormularioAlertaController;
    this.scope = {
      form: '=validarFormularioAlerta',
      nombreForm: '@nombreFormulario',
      method: '&ngClick',
      methodValidation: '&validacionParalela'
    }
  }

  link($scope, $elem, $attr, $ctrl) {
    $elem.bind('click', (event) => {
      if (!$scope.form && $scope.nombreForm) {
        $scope.form = $scope.nombreForm;
      }
      if ($ctrl.Util.isEmpty($scope.form)) {
        return;
      }
      event.stopImmediatePropagation();
      if (typeof $scope.form === 'object') {
        this.validarFormularioAlerta($scope, $ctrl, $scope.form, event);
      } else if (typeof $scope.form === 'string') {
        const FORM = this.buscarFormulario($scope);
        if (!angular.isUndefined(FORM)) {
          this.validarFormularioAlerta($scope, $ctrl, FORM[$scope.form]);
        }
      }
      $scope.$apply();
    });
  }

  validarFormularioAlerta($scope, $ctrl, form) {
    if ($ctrl.Util.isValidFormFieldsAlerta(form)) {
      $scope.method();
    } else {
      $scope.methodValidation();
    }
  }

  buscarFormulario($scope) {
    return angular.element(document.querySelector('[name=' + $scope.form + ']')).scope();
  }

}

class ValidarFormularioAlertaController {
  constructor(Util) {
    'ngInject';
    this.Util = Util;
  }
}

export default ValidarFormularioAlerta;
