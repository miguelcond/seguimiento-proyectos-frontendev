//require('../../../../../../node_modules/leaflet/dist/leaflet');
//require('../../../../../../node_modules/leaflet/dist/leaflet.css');
//require('../../../../../../node_modules/leaflet-fullscreen/dist/Leaflet.fullscreen.min');
//require('../../../../../../node_modules/leaflet-fullscreen/dist/leaflet.fullscreen.css');
require('./leaflet-fullscreen.css');
require('./angular-leaflet');

var MODULE_NAME = 'angular-leaflet';

angular.module(MODULE_NAME, ['leaflet-directive']);

module.exports = MODULE_NAME;
