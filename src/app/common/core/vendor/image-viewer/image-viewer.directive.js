/**
 * Marcelo Alejo Calle
 */
import template from './image-viewer.html';
import './image-viewer.scss';

export const ImageViewer = ($window, $document, $timeout, $compile) => {
  'ngInject';

  /**
   * b64toBlob - Convert base64 string to a Blob object
   *
   * @param  {string}  b64Data     Base64 string
   * @param  {strig}   contentType Type of base64 file
   * @param  {integer} sliceSize   Slice for performance
   * @return {Blob}                Blob object
   * @description https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
   */
  let b64toBlob = (b64Data, contentType, sliceSize)=> {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    let byteCharacters;
    if (b64Data.split(',')[0].indexOf('base64') >= 0) {
      byteCharacters = atob(b64Data.split(',')[1]);
    } else {
      byteCharacters = atob(b64Data);
    }
    let byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new window.Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }

  let imageSize = (img)=>{
    let image = {}
    if(img && img.src) {
      image.size = Math.round((img.src.length - 22)*3/4);
    } else {
      image.size = img;
    }
    image.unit = 'B';
    if(image.size>=1000) {
      image.size = image.size/1000;
      image.unit = 'KB';
    }
    if(image.size>=1000) {
      image.size = image.size/1000;
      image.unit = 'MB';
    }
    if(image.size.toFixed) {
      image.size = image.size.toFixed(1);
    }
    return image;
  };

  return {
    restrict: 'E',
    template: template,
    scope: {
      base64: '=',
      vm: '=?',
    },

    link($scope, $element, $attrs) {
      let image = $element.find('img')[0];
      let canvas = $element.find('canvas')[0];
      let ctx = canvas.getContext('2d');
      let scale = 100;
      let rotate = 0;
      let objImage = new Image();

      if($scope.vm) {
        $scope.vm = $scope;
      }
      if(!$scope.tipo) {
        $scope.tipo = 'image/jpeg';
      }
      // Objeto para restaurar los cambios
      // if($scope.base64.indexOf('base64')>0) {
      //   image.src = $scope.base64;
      // } else {
      //   image.src = 'data:'+$scope.tipo+';base64,'+$scope.base64;
      // }
      if($scope.base64.indexOf('base64')>0) {
        objImage.src = $scope.base64;
      } else {
        objImage.src = 'data:'+$scope.tipo+';base64,'+$scope.base64;
      }
      objImage.id = 'image-viewer-print';
      objImage.onload = ()=>{
        canvas.width = objImage.width;
        canvas.height = objImage.height;
        ctx.drawImage(objImage,0,0);
        $scope.imageDet = imageSize(objImage);
        $scope.imageWidth = objImage.width;
        $scope.imageHeight = objImage.height;
      };

      // funciones
      $scope.imageZoom = (zoom)=>{
        if(zoom>0) {
          if(scale+zoom<300) {
            scale += zoom;
            canvas.style.width = scale + '%';
          }
        } else if(zoom<0) {
          if(scale+zoom>=10) {
            scale += zoom;
            canvas.style.width = scale + '%';
          }
        }
      };

      $scope.imageInvert = ()=>{
        let imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        let data = imageData.data;
        for (var i = 0; i < data.length; i += 4) {
          data[i]     = 255 - data[i];     // red
          data[i + 1] = 255 - data[i + 1]; // green
          data[i + 2] = 255 - data[i + 2]; // blue
        }
        ctx.putImageData(imageData, 0, 0);
        $scope.modificado = true;
      };

      $scope.imageGrayscale = ()=>{
        let imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        let data = imageData.data;
        for (var i = 0; i < data.length; i += 4) {
          var avg = (data[i] + data[i + 1] + data[i + 2]) / 3;
          data[i]     = avg; // red
          data[i + 1] = avg; // green
          data[i + 2] = avg; // blue
        }
        ctx.putImageData(imageData, 0, 0);
        $scope.modificado = true;
      };

      $scope.imageRotate = ()=>{
        rotate += 90;
        if(rotate=>360) {
          rotate = rotate%360;
        }
        if(rotate%180==0) {
          canvas.width = objImage.width;
          canvas.height = objImage.height;
        } else {
          canvas.width = objImage.height;
          canvas.height = objImage.width;
        }
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.save();
        ctx.translate(canvas.width/2,canvas.height/2);
        ctx.rotate(rotate*Math.PI/180);
        ctx.drawImage(objImage,-objImage.width/2,-objImage.height/2);
        ctx.restore();
        // image.src = canvas.toDataURL('image/png');
        $scope.imageDet = imageSize(Math.round((canvas.toDataURL('image/jpeg',1).length - 23)*3/4));
        $scope.modificado = true;
      };

      $scope.imageDownload1 = ()=>{
        $window.location.href = canvas.toDataURL('image/jpeg',1).replace('image/jpeg','image/octet-stream');
      }

      $scope.imageDownload = ()=>{
        if(!$scope.descargando) {
          $scope.descargando = true;
          let image64 = canvas.toDataURL('image/jpeg',1);
          let file,a = document.createElement('a');
          a.innerHTML = '.';
          document.body.appendChild(a);
          // $compile(a)(scope);
          file = b64toBlob(image64, 'image/jpeg');
          $scope.imageDet = imageSize(file.size);
          if (window.navigator.msSaveBlob) {
            window.navigator.msSaveOrOpenBlob(file, $scope.nombre||'documento.jpg');
            $scope.descargando = false;
          } else {
            a.href = window.URL.createObjectURL(file);
            a.download = $scope.nombre||'documento.jpg';
            //a.target = '_blank';
            $timeout(()=>{
              a.click();
              $scope.descargando = false;
              angular.element(a).remove();
            },250);
          }
        }
      }

      $scope.imagePrint = ()=>{
        if(!$scope.imprimiendo) {
          $scope.imprimiendo = true;
          let imagePrint = new Image();
          imagePrint.src = canvas.toDataURL('image/jpeg',1);
          imagePrint.onload = ()=>{
            if(imagePrint.height/imagePrint.width < 1.294118) { // Relacion de aspecto tamaño carta y/x => 11/8.5
              imagePrint.style = "width:100%;"
            } else {
              imagePrint.style = "height:100%;"
            }
            document.body.appendChild(imagePrint);
            $window.print();
            imagePrint.parentNode.removeChild(imagePrint);
            $scope.imprimiendo = false;
          };
        }
      };

      $scope.imageSave = ()=>{
        if($scope.modificado) {
          $scope.modificado = false;
          $scope.base64 = canvas.toDataURL('image/jpeg',1);
          $scope.imageDet = imageSize(Math.round((canvas.toDataURL('image/jpeg',1).length - 23)*3/4));
        }
      };
    }
  }
}
