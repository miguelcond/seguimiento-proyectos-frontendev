'use strict';

class DownloadFile {
  constructor() {
    this.restrict = 'A';
    this.controller = DownloadFileController;
  }

  link($scope, $element, $attrs, $ctrl) {
    $element.bind('click', () => {
      $ctrl.Loading.show('Preparando documento para su descarga', true);
      let url = $attrs.downloadFile;
      let dType = 'application/pdf';
      if ($attrs.downloadType === 'csv') {
        dType = 'text/csv';
      }
      $ctrl.$http.get(url, {
          responseType: 'arraybuffer',
          headers: {'Authorization': 'Bearer ' + $ctrl.Storage.get('token')}
      }).then(function (response) {
        var blob = new Blob([response.data], {
          type: dType
        });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob);
        } else {
          var url = URL.createObjectURL(blob);
          var a = document.createElement('a');
          var date = new Date();
          a.href = url;
          a.download = `${date.getTime()}_${$attrs.fileName}${$attrs.downloadType ? '.' + $attrs.downloadType : ''}`;
          a.target = '_self';
          document.body.appendChild(a);
          a.click();
        }
        $ctrl.Loading.hide();
      });
    });
  }

}

class DownloadFileController {
  constructor($http, Storage, Loading) {
    'ngInject';
    this.$http = $http;
    this.Storage = Storage;
    this.Loading = Loading;
  }
}

export default DownloadFile;
