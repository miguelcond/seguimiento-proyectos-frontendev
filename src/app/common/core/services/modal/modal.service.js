'use strict';

import Controller from './modal.controller';
import Template from './modal.html';
import PdfController from './pdf/pdfview.modal.controller';
import PdfTemplate from './pdf/pdfview.modal.html';
import DocumentBase64Template from './pdf/document_view_base64.modal.html';

import ImageBase64Template from './image/imageviewer.html';
import ImageController from './image/imageviewer.controller';

class ModalService {
    constructor($uibModal) {
        "ngInject";

        this.$uibModal = $uibModal;
    }

    show(config) {
        let setting = {
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: config.template || Template,
            controller: config.controller || Controller,
            controllerAs: '$ctrl',
            size: config.size,
            keyboard: typeof config.keyboard == 'undefined' ? true : config.keyboard,
            backdrop: 'static',
            resolve: {}
        };

        setData(config, setting, 'data');
        setData(config, setting, 'title');
        setData(config, setting, 'icon');
        setData(config, setting, 'message');
        setData(config, setting, 'labelOk');
        setData(config, setting, 'labelCancel');
        setData(config, setting, 'cancel');
        setData(config, setting, 'eventCancel');
        setData(config, setting, 'eventOk');
        setData(config, setting, 'btnClose');

        return this.$uibModal.open(setting);
    }

    alert(message, callbackOk, title, icon, size) {
        this.show({
            title: title || 'Alerta',
            icon: icon || 'bell',
            message,
            size: size || 'sm',
            cancel: false,
            eventOk: callbackOk,
            keyboard: false,
            btnClose: false
        });
    }

    confirm(message, callbackOk, callbackCancel, title, labelOk, labelCancel, icon, size) {
        let config = {
            title: title || 'Confirmar',
            icon: icon || 'warning',
            message,
            eventOk: callbackOk,
            eventCancel: callbackCancel,
            labelOk,
            labelCancel,
            btnClose: false
        };
        if (typeof size == 'string') {
            config.size = size;
        }
        this.show(config);
        // TODO Modal. Otra forma de quitar el mensaje: Possibly unhandled rejection: cancel
        //this.show(config).result.then(()=>{},()=>{});
    }

    pdf(urlPdfFile, title) {
        this.show({
            template: PdfTemplate,
            controller: PdfController,
            data: {
                urlPdf:urlPdfFile,
                title:title
            },
            size: 'lg'
        });
    }

    showDocumentBase64(urlPdfFile, title, type) {
        this.show({
            template: DocumentBase64Template,
            controller: PdfController,
            data: {
                urlPdf: urlPdfFile,
                title: title,
                type: type?type:'base64'
            },
            size: 'lg'
        })
        .result.then(()=>{},()=>{});
    }

    showFormularioBase64(urlPdfFile, title, type) {
        this.show({
            template: DocumentBase64Template,
            controller: PdfController,
            data: {
                reload: true,
                urlPdf: urlPdfFile,
                title: title,
                type: type?type:'base64'
            },
            size: 'lg'
        })
        .result.then(()=>{},()=>{});
    }

    showImageBase64(urlImgFile, title, type) {
        this.show({
            template: ImageBase64Template,
            controller: ImageController,
            data: {
                image: urlImgFile,
                title: title,
                type: type?type:'base64'
            },
            size: 'lg'
        })
        .result.then(()=>{},()=>{});
    }
}

function setData(config, setting, data) {
    setting.resolve[data] = () => {
        return typeof config[data] == 'undefined' ? '' : config[data];
    };
}

export default ModalService;
