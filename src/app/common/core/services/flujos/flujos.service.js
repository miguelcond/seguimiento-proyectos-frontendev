'use strict';

class FlujosService {
  constructor(Storage, _) {
    "ngInject";

    this._ = _;
    this.Storage = Storage;
  }

  $onInit() {
  }

  getAtributosPermitidos(entidad, estado = 'estado_actual') {
    this.usuario = this.Storage.getUser();
    let atributos = [];
    if (this.usuario) {
      atributos = entidad[estado].atributos;
      if (!Array.isArray(entidad[estado].atributos)) {
        atributos = atributos[this.usuario.rol.id_rol];
        if (!atributos) {
          atributos = [];
        }
      }
    }
    entidad[estado].atributos = this._.zipObject(atributos, Array.from(new Array(atributos.length), () => true));
  }
}

export default FlujosService;
