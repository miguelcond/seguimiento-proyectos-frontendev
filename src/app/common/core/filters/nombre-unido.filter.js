'use strict';

const NombreUnidoFilter = (_, Util) => {
    'ngInject';

    return (objPersona) => {

        if (!_.isEmpty(objPersona)) {
          return Util.getNombreUnido(objPersona);
        }

        return objPersona;
    };
};

export default NombreUnidoFilter;
