'use strict';

const NumberFilter = {
    numberToLiteral: (numToWord) => {
      'ngInject';
      return (valor) => {
        if (!valor || typeof valor !== 'number') {
          return;
        }
        return numToWord.transformar(valor);
      }
    },
    formatDecimal: ($locale, Util, decimales) => {
      'ngInject';
      return (valor, nDecimales) => {
        if (!valor) {
          return;
        }
        if (typeof valor !== 'number') {
          return valor;
        }
        nDecimales = nDecimales || decimales || 2;
        const separadorDecimal = $locale.NUMBER_FORMATS.DECIMAL_SEP;
        const separadordeMiles = $locale.NUMBER_FORMATS.GROUP_SEP;

        let valorFormateado = Util.redondear(parseFloat(valor), nDecimales).toFixed(nDecimales);
        valorFormateado = valorFormateado.replace('.', separadorDecimal);

        let partes = ('' + valorFormateado).split(separadorDecimal);
        partes[0] = partes[0].replace(/\B(?=(\d{3})+(?!\d))/g, separadordeMiles);

        return partes.join(separadorDecimal);
      }
    }
};

export default NumberFilter;
