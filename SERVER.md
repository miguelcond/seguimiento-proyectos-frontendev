# Instalación General en el Servidor (FRONTEND)

Se realizaron distintas instalaciones en el servidor de TEST Debian, a continuación las configuraciones realizadas en dicho servidor.

## Otros

En la plantilla de la máquina virtual de producción se tuvieron problemas con los certificados, para resolverlo se instaló lo siguiente:

```sh
$ sudo  apt-get install ca-certificates
```

## Build Essentials
Instalar build essentials
```sh
sudo apt-get install build-essential
```

## GIT
Para la instalación de git se siguieron algunas instrucciones de la siguiente página:
> https://www.digitalocean.com/community/tutorials/how-to-install-git-on-debian-8

```sh
$ sudo apt-get update
$ sudo apt-get install git-core
```

Es posible configurar los nombres de usuarios:
```sh
$ git config --global user.name "usuario"
$ git config --global user.email usuario@agetic.gob.bo
$ git config --list
```

El último comando es para verificar que se haya guardado la configuración realizada.

Para poder clonar el proyecto posteriormente se puede seguir una de las siguientes opciones:

### Opción 1: Generar SSH key

Si aún no se cuenta con una llave SSH para la conexión a GIT, seguir los siguientes pasos:
Para la generación de la llava SSH se siguieron los pasos del siguiente enlace:
> https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/

```sh
$ ssh-keygen -t rsa -b 4096 -C "usuario@agetic.gob.bo"
```

Para verificar la creación de la llave SSH navegar al siguiente directorio:
```sh
$ cd /home/nombre_usuario/.ssh/
```

La llave se encontrará en el archivo `id_rsa.pub`;

Copiar el contenido del archivo en la respectiva cuenta del GITLAB para la autenticación.

> **Profile Settings** >> **SSH Keys**

### Opción 2: Descargar por HTTPS:

Si no se desea generar una llave SSH, descargar por HTTPS, según los pasos indicados posteriormente en el achivo INSTALL.md.

## Curl y Wget

Si no se tiene instalado el curl y el wget, instalarlos.

```sh
$ sudo  apt-get install curl
$ sudo  apt-get install wget
```

## Instalación de nvm (node version manager)

Instalación de [nvm](https://github.com/creationix/nvm)

```sh
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
```

Actualizar variables de entorno.
```
. /home/nombre_usuario/.bashrc
```

Instalación de node.js LTS versión 6.9.5 utilizando `nvm`.

```sh
nvm install 6.9.5
```

Verificamos con:

```sh
node -v

## Instalación del Proyecto

Para continuar con la instalación del PROYECTO, seguir los pasos del siguiente archivo:

> [INSTALL.md](INSTALL.md)


## Instalación de Apache2

```
$ sudo apt install apache2

$ cd /etc/apache2/sites-available
$ sudo cp 000-default.conf default.conf
$ cd ../sites-enabled/
$ sudo a2dissite 000-default.conf
$ sudo a2ensite default.conf
```

```
$ sudo nano default
```
```apache2
  Alias /fdi /home/usuario/seguimiento-proyectos-frontend/produccion
  <Directory /home/usuario/seguimiento-proyectos-frontend/produccion>
    Options None
    Require all granted
    AllowOverride All
    Order allow,deny
    Allow from all
  </Directory>
```

```
$ sudo a2enmod proxy proxy_http
$ sudo a2enmod proxy_balancer lbmethod_byrequests
$ sudo systemctl restart apache2
```

```
<Proxy balancer://backendApi>
  BalancerMember http://127.0.0.1:4000
  BalancerMember http://127.0.0.1:4004
  ProxySet lbmethod=byrequests
</Proxy>

  <Location /balancer-manager>
    SetHandler balancer-manager
  </Location>

  ProxyPass /balancer-manager !
  ProxyPass        /fdi/api/ balancer://backendApi/
  ProxyPassReverse /fdi/api/ balancer://backendApi/
```


```
$ sudo a2enmod headers

  Header set Content-Security-Policy "default-src https: data: 'unsafe-inline' 'unsafe-eval'; media-src *.bo data: blob:"
  Header set X-Frame-Options: "DENY"
  Header set X-Content-Type-Options: nosniff
  Header set Referrer-Policy: same-origin
```
