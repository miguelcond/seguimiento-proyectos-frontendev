# Instalación de la Aplicación (FRONTEND)


## Configuración del Servidor

Para una correcta instalación, el servidor debe tener las siguientes configuraciones obligatoriamente:

> [SERVER.md](SERVER.md)


Después recién llevar a cabo los siguientes pasos, que son necesarios para instalar la aplicación.


## Instalación del proyecto para Desarrollo

### Clonación

Clonarlo:

**Opción 1:** Si se generó llave SSH: (En los pasos del archivo SERVER.md)
```sh
$ git clone -b FDI-master git@gitlab.geo.gob.bo:agetic/seguimiento-proyecto-frontend.git  seguimiento-proyectos-frontend
```
**Opción 2:** Si se quiere clonar el proyecto por HTTPS:
```sh
$ git clone -b FDI-master https://gitlab.geo.gob.bo/agetic/seguimiento-proyecto-frontend.git  seguimiento-proyectos-frontend
```
Es posible que al descargar el proyecto con HTTPs, nos lance el siguiente error:
```sh
Cloning into 'nombre-del-proyecto'...
fatal: unable to access 'https://url-del-proyecto.git/': server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
```
```sh
$ git config --global http.sslverify false
$ git clone -b FDI-master https://gitlab.geo.gob.bo/agetic/seguimiento-proyecto-frontend.git  seguimiento-proyectos-frontend
```

**Después de clonar con cualquiera de las opciones anteriores:**

Ingresar a la carpeta:
```sh
$ cd seguimiento-proyectos-frontend
```
Podemos verificar que estamos en el branch `FDI-master` ó en el último tag correspondiente por ejemplo FDI-v1.0.0
```sh
$ git status
```
Nos devolverá:
```sh
- On branch master
```

`(Opcional)` Si necesitamos trabajar un branch específico (en este ejemplo, el nombre del branch es nombre_del_branch) ejecutamos:
```sh
$ git checkout nombre_del_branch
```

Al volver a verificar con git status:
```sh
$ git status
```
Se obtiene como respuesta que el proyecto se sitúa en el branch elegido:
```sh
- On branch nombre_del_branch
```
Para instalar la aplicación, se tienen las siguientes opciones:

#### Instalando dependencias npm
```sh
$ npm install
```

#### Configurar los datos de conexión a los servicios REST del backend

Toda la configuración para los archivos del frontend se encuentra en el archivo **`config.json.sample`**, su servidor de producción y su servidor de desarrollo con webpack, el mismo se encuentra en la raíz del proyecto.

Copiar dicho archivo y renombrarlo bajo el nombre **`config.json`** en la raiz del proyecto

A continuación se describe la configuración:

`**¡NO OLVIDE REVISAR EL CONTENIDO DEL ARCHIVO, EL SIGUIENTE CONTENIDO ESTÁ PARA MOTIVOS DE EJEMPLO!**`

```js
{
  "server": "http://dominio.gob.bo/seguimiento-api",
  "timeSessionExpired": 30,
  "debug": false,
  "onbeforeunload": true,
  "port": 3100,
  "subDomain": "/seguimiento/",
  "portDev": 8880
};
```
- **server**: Servidor del backend donde apunta el frontend
  - Ejemplos
    - "server": "http://localhost:4000"
    - "server": "http://192.168.15.15:4000"
    - "server": "http://dominio.gob.bo/proyecto-api"
- **timeSessionExpired**: Tiempo en minutos para que la sesión se cierre automáticamente por inactividad
- **debug**: Habilita los console.log (this.$log.log) para su visualización para producción es necesario deshabilitarlo con: false
- **onbeforeunload**: abre un alerta de confirmación cuando se intente cerrar o actualizar la pestaña del navegador
- **port**: Puerto para el servidor de producción
- **portDev**: Puerto para el servidor de desarrollo con webpack
- **subDomain**: Sub dominio donde iniciará el frontend, por defecto inicia en la raíz de la carpeta dist
  - Ejemplos:
    - "subDomain": "/"
    - "subDomain": "/proyecto/"

---

## Instalación del proyecto para Producción

Para asegurarse de que se instalen todas las librerías necesarias hay que configurar el ambiente en modo desarrollo.

#### Instalando dependencias npm
```sh
$ npm install
```

### Configurar entorno de producción

#### Crear los archivos minificados
```sh
$ npm run build
```

Verificar si se ha creado la carpeta `dist` y renombrarlo a `produccion`

```sh
$ mv dist produccion
```

#### Instalación de Nginx (Opcion 1)

```sh
$ sudo apt-get update
$ sudo apt-get install nginx
```

Crear un enlace simbólico hacia la carpeta `produccion` en la carpeta raiz del servidor nginx

```sh
sudo ln -s /home/usuario/seguimiento-proyecto-frontend/produccion /var/www/html/proyecto
```

Verificar el enlace

```sh
$ ls -la /var/www/html/
```

Editar el archivo de configuración de sitio por defecto

```
$ sudo nano /etc/nginx/sites-enabled/default
```

Ejemplo de cofiguración para reverse proxy hacia el backend `seguimiento-api` en el mismo servidor

```sh
# Default server configuration
#
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root /var/www/html;
    index index.html index.htm index.nginx-debian.html;
    server_name _;

    location /seguimiento-api/ {
      proxy_pass http://localhost:4000/;
      #proxy_redirect    off;
      #proxy_set_header Host $host;
      #proxy_set_header X-Real-IP $remote_addr;
      #proxy_set_header X-Forwarded-Proto https;
    }
}
```

> Considerar que el nombre del usuario del equipo puede variar.

> Considerar que la ruta real en la que se encuentra la aplicación puede variar.


#### Instalación de Apache2 (Opcion 2)

```sh
$ sudo apt-get update
$ sudo apt-get install apache2
```

Crear un enlace simbólico hacia la carpeta `produccion` en la carpeta raiz del servidor apache2

```sh
sudo ln -s /home/usuario/seguimiento-proyecto-frontend/produccion /var/www/html/seguimiento
```

Verificar el enlace

```sh
$ ls -la /var/www/html/
```

Editar el archivo de configuración de sitio por defecto

```
$ sudo nano /etc/apache2/sites-enabled/default
```

Habilitar los modulos para reverse prosy hacia el backend `seguimiento-api`

```
$ sudo a2enmod proxy
$ sudo a2enmod proxy_http
```

Ejemplo de cofiguración para reverse proxy hacia el backend `seguimiento-api` en el mismo servidor

```sh
# Default server configuration
#
  SSLProxyEngine On
  SSLProxyCheckPeerCN Off
  SSLProxyCheckPeerName Off
  SSLProxyCheckPeerExpire Off

  ProxyRequests Off
  ProxyPreserveHost On

  ProxyPass "/seguimiento-api/" "http://localhost:4000/"
  ProxyPassReverse "/seguimiento-api/" "http://localhost:4000/"
```

```
$ sudo apache2ctl configtest
```

> Considerar que el nombre del usuario del equipo puede variar.

> Considerar que la ruta real en la que se encuentra la aplicación puede variar.


##### Reconfigurar TimeZone

```
$ sudo dpkg-reconfigure tzdata
```

Seleccionar America/La_Paz

#### Verificar frontend

Para verificar el funcionamiento se debe abrir en el navegador web una de las siguientes URLs

- http://[ip_servidor]/seguimiento/
- http://[dominio.gob.bo]/seguimiento/

Debe mostrar la página de inicio de sesión del sistema
